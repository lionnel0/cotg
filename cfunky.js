(function FunkyScript() {

    // popup message for players when they open the game.
    $(document).ready(function() {
    var popwin="<div id='HelloWorld' style='width:400px;height:400px;background-color: #E2CBAC;-moz-border-radius: 10px;-webkit-border-radius: 10px;border-radius: 10px;border: 4px ridge #DAA520;position:absolute;right:40%;top:100px; z-index:1000000;'><div class=\"popUpBar\"> <span class=\"ppspan\">Welcome!</span><button id=\"cfunkyX\" onclick=\"$('#HelloWorld').remove();\" class=\"xbutton greenb\"><div id=\"xbuttondiv\"><div><div id=\"centxbuttondiv\"></div></div></div></button></div><div id='hellobody' class=\"popUpWindow\"><span style='margin-left: 5%;'> <h3 style='text-align:center;'>Welcome to Crown Of The Gods!</h3></span><br><br><span style='margin-left: 5%;'> <h4 style='text-align:center;'> CFunky script V1.2.2</h4></span><br><span style='margin-left: 5%;'> <h4 style='text-align:center;'>Updated 25.1.2017</h4></span><br><br><span style='margin-left: 5%;'><h4>changes:</h4> <ul style='margin-left: 6%;'><li>Fixed quick demolish button (26-2-17)</li></ul></span></div></div>";
        $("body").append(popwin);

        setTimeout(function() {
                            var options = {};
                            $('#HelloWorld').hide( 'drop', options, 2000);
                        }, 5000);


    });

    var loot=[0,400,1000,4500,15000,33000,60000,120000,201000,300000,446000]; //cavern loot per lvl
    var bossdef=[625,3750,25000,50000,125000,187500,250000,375000,562500,750000]; //bosses defense value
    var bossdefw=[425,2500,17000,33000,83000,125000,170000,250000,375000,500000]; // bosses defense value for weakness type
    var bossmts=[6,20,100,500,2000,3500,5000,8000,12000,15000]; //minimum TS to send to a boss
    var numbs=[0,0,0];
    var ttloot=[0,0,10,20,10,10,5,0,15,20,15,10,0,0,1000,1500,3000]; //troops loot capacity
    var ttattack=[10,50,30,10,25,50,70,10,40,60,90,120,50,150,3000,1200,12000]; //troops attack value
    var ttspeed=[0,30,20,20,20,20,20,8,10,10,10,10,30,30,5,5,5,40];
    var iscav=[0,0,0,0,0,0,0,1,1,1,1,0,0,0,0,0,0]; //which troop number is cav
    var isinf=[1,0,1,1,1,1,0,0,0,0,0,0,0,0,0,0,0]; //which troop number is inf
    var ismgc=[0,0,0,0,0,0,1,0,0,0,0,1,0,0,0,0,0]; //which troop number is magic
    var isart=[0,1,0,0,0,0,0,0,0,0,0,0,1,1,1,1,1]; //which troop number is artillery
    var ttres=[1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1];
    var ttspeedres=[1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1];
    var resbonus=[0,0.01,0.03,0.06,0.1,0.15,0.2,0.25,0.3,0.35,0.4,0.45,0.5]; // res bonus to attack power per res rank
    var ttts=[1,10,1,1,1,1,1,2,2,2,2,2,10,10,100,100,400,1]; //ts per unit
    var ttname=["Guards","Ballista","Ranger","Triari","Priestess","Vanquisher","Sorcerer","Scout","Arbalist","Praetor","Horseman","Druid","Ram","Scorpion","Galley","Stinger","Warship","Senator"];
    var layoutsl=[""];
    var layoutsw=[""];
    var today= new Date();
    var hidespf=false;
    var tpicdiv=["guard32 trooptdcm","bally32 trooptdcm","ranger32 trooptdcm","triari32 trooptdcm","priest32 trooptdcm","vanq32 trooptdcm","sorc32 trooptdcm","scout32 trooptdcm","arbal32 trooptdcm","praet32 trooptdcm","horsem32 trooptdcm",
                 "druid32 trooptdcm","ram32 trooptdcm","scorp32 trooptdcm","galley32 trooptdcm","sting32 trooptdcm","wship32 trooptdcm","senat32 trooptdcm"];
    var tpicdiv20=["guard20 trooptdcm","bally20 trooptdcm","ranger20 trooptdcm","triari20 trooptdcm","priest20 trooptdcm","vanq20 trooptdcm","sorc20 trooptdcm","scout20 trooptdcm","arbal20 trooptdcm","praet20 trooptdcm","horsem20 trooptdcm",
                 "druid20 trooptdcm","ram20 trooptdcm","scorp20 trooptdcm","galley20 trooptdcm","sting20 trooptdcm","wship20 trooptdcm","senat20 trooptdcm"];
    var buildings={name: ["forester","cottage","storehouse","quarry","hideaway","farmhouse","cityguardhouse","barracks","mine","trainingground","marketplace","townhouse","sawmill","stable","stonemason","mage_tower","windmill","temple","smelter","blacksmith",
                       "castle","port","port","port","shipyard","shipyard","shipyard","townhall","castle"],
               bid: [448,446,464,461,479,447,504,445,465,483,449,481,460,466,462,500,463,482,477,502,"467",488,489,490,491,496,498,455,467]};
    var ibriafaith=0,ylannafaith=0,naerafaith=0,cyndrosfaith=0,domdisfaith=0,vexifaith=0,meriusfaith=0,evarafaith=0; //alliance faiths
    var cdata; //city data return
    var wdata; //world data
    var plata; //players list on server
    var pdata={}; //player data

    var poll2; //city update info
    var clc={};// city lists
    var oga; //city outgoing attacks info
    var city={cid:0,x:0,y:0,th:[0],cont:0}; //current city data
    var bosses={name:["Cyclops","Andar's Colosseum Challenge","Dragon","Romulus and Remus","Gorgon","GM Gordy","Triton"],
                pic:["cyclops32 mauto bostooltip tooltipstered","andar32 mauto bostooltip tooltipstered","dragon32 mauto bostooltip tooltipstered","romrem32 mauto bostooltip tooltipstered","gorgon32 mauto bostooltip tooltipstered","gmgordy32 mauto bostooltip tooltipstered","triton32 mauto bostooltip tooltipstered"]};
    var bossinfo={x:[],y:[],lvl:[],data:[],name:[],cont:[],distance:[]};
    var key="_`abcdefgh";
    var remarksl=[""];
    var remarksw=[""];
    var troopcounw=[[]];
    var troopcounl=[[]];
    var resw=[[]];
    var resl=[[]];
    var notesl=[""];
    var notesw=[""];
    var emptyspots=",.;:#-T";
    var beentoworld=false;
    var splayers={name:[],ally:[],cities:[]};
    var shrinec=[[]];

        //getting city lists
    $(document).ready(function() {
        setTimeout(function() {
            var a=$("#organiser > option");
            var l=a.length;
            //console.log(a,l);
            for  (var i=0; i<l;i++) {
                var temp=String($(a[i]).attr("value"));
                $("#organiser").val(temp).change();
                poll2.player.clc[temp]=[];
                var tempcl=$("#cityDropdownMenu > option");
                var ll=tempcl.length;
                if (poll2.city.cg.indexOf(temp)>-1) {
                    poll2.player.clc[temp].push($(tempcl[0]).attr("value"));
                }
                if (ll>1) {
                    for (var j=1;j<ll;j++) {
                        poll2.player.clc[temp].push($(tempcl[j]).attr("value"));
                    }
                }
                //console.log(i,temp,a[i]);
            }
            //console.log(poll2.player.clc);
            $("#organiser").val("all").change();
        },4000);
    });

    (function(open) {
        XMLHttpRequest.prototype.open = function() {
            this.addEventListener("readystatechange", function() {
                if(this.readyState==4) {
                    var url=this.responseURL;
                    if (url.indexOf('gC.php')!=-1) {
                        poll2.city=JSON.parse(this.response);
                        //console.log(poll2.city);
                        city.x=Number(poll2.city.cid % 65536);
                        city.y=Number((poll2.city.cid-city.x)/65536);
                        city.cont=Number(Math.floor(city.x/100)+10*Math.floor(city.y/100));
                        //console.log(poll2.city.mo);
                        makebuildcount();
                        updateattack();
                        updatedef();
                    }
                    if (url.indexOf('gWrd.php')!=-1) {
                        wdata=JSON.parse(this.response);
                        //console.log("not cfunky");
                        beentoworld=true;
                        wdata=decwdata(wdata.a);
                        getbossinfo();
                    }
                    if (url.indexOf('gPlA.php')!=-1) {
                        pldata=JSON.parse(this.response);
                        //console.log(pldata);
                    }
                    if (url.indexOf('poll2.php')!=-1) {
                        if(poll2) {     
                        var saveclc=poll2.player.clc;
                        var saveoga=poll2.OGA;
                        }
                        poll2=JSON.parse(this.response);
                        //console.log(poll2);
                        //poll2.city=poll2.city;
                        //updateattack();
                        city.x=Number(poll2.city.cid % 65536);
                        city.y=Number((poll2.city.cid-city.x)/65536);
                        city.cont=Number(Math.floor(city.x/100)+10*Math.floor(city.y/100));
                        if ('OGA' in poll2) {

                        } else {
                            poll2.OGA=saveoga;
                        }
                        if ('bd' in poll2.city) {
                            makebuildcount();
                        }
                        if ('clc' in poll2.player) {

                        } else {
                            poll2.player.clc=saveclc;
                        }
                        if ($( "#warcouncTabs" ).tabs( "option", "active" )==2) {
                            var idle="<table id='idleunits' class='beigetablescrollp'><tbody><tr><td style='text-align: center;'><span>Idle troops:</span></td>";
                            for (var i in poll2.city.th) {
                                var notallow=[0,1,7,12,13];
                                if (notallow.indexOf(i)==-1) {
                                    if (poll2.city.th[i]>0) {
                                        idle+="<td><div class='"+tpicdiv[i]+"' style='text-align: right;'></div></td><td style='text-align: left;'><span id='thbr"+i+"' style='text-align: left;'>"+poll2.city.th[i]+"</span></td>";
                                    }
                                }
                            }
                            idle+="</tbody></table>";
                            $("#idletroops").html(idle);
                        }
                        //pdata=poll2.player;
                        //pdata.planame=poll2.player['planame'];
                    }
                }
            }, false);
            open.apply(this, arguments);
        };
    })(XMLHttpRequest.prototype.open);

//make MM/DD/yyyy from date variable
    function getFormattedDate(date) {
        var year = date.getFullYear();
        var month = (1 + date.getMonth()).toString();
        month = month.length > 1 ? month : '0' + month;
        var day = date.getDate().toString();
        day = day.length > 1 ? day : '0' + day;
        return month + '/' + day + '/' + year;
    }
    //rounds nubers to second digit after decimal
    function roundToTwo(num) {
        return +(Math.round(num + "e+2")  + "e-2");
    }
    function errormsgBR(a, b) {
        $(a).show();
        $(b).animate({ opacity: 1, bottom: "+10px" }, 'slow');
        errormsgBRhide(a, b);
    }

    function errormsgBRhide(a, b) {
        setTimeout(function(){ 
            $(b).animate({ opacity: 0, bottom: "-10px" }, 'slow');
            $(a).fadeOut("slow");
        }, 5000);
        setTimeout(function(){ 
            $(a).remove();
        }, 6000);
    }

    var errmBR=0;
    var message="Error, you need at least ";
    //console.log(message);

    function errorgo(j) {
        var errormsgs;
        errmBR = errmBR+1;
        var b = 'errBR' +errmBR;
        var c = '#' +b;
        var d = '#' +b+ ' div';
        errormsgs = '<tr ID = "' +b+ '"><td><div class = "errBR">' +j+ '<div></td></tr>';
        $("#errorBRpopup").append(errormsgs);
        errormsgBR(c, d);
    }


    String.prototype.replaceAt=function(index, char) {
        var a = this.split("");
        a[index] = char;
        return a.join("");
    }
    String.prototype.decrypt=function() {
    var a=this;
    for (var i in a) {
        for (var j in key) {
            if (a.charAt(i)==key.charAt(j)) {
                a=a.replaceAt(i,j);
            }
        }
    }
    return a;
    };

    function getbossinfo() {
        var temp;
        bossinfo={x:[],y:[],lvl:[],data:[],name:[],cont:[],distance:[],cid:[]};
        //console.log("newworld",wdata.bosses);
        for (var i in wdata.bosses) {
            var templvl=Number(wdata.bosses[i].substr(1,2))-10;
            var tempy=Number(wdata.bosses[i].substr(4,3))-100;
            var tempx=Number(wdata.bosses[i].substr(7,3))-100;
            var cid=tempy*65536+tempx;
            bossinfo.x.push(tempx);
            bossinfo.y.push(tempy);
            bossinfo.lvl.push(templvl);
            bossinfo.cont.push(Number(Math.floor(tempx/100)+10*Math.floor(tempy/100)));
            //bossinfo.distance.push(distance);
            bossinfo.data.push(wdata.bosses[i]);
            bossinfo.cid.push(cid);
        }
    }
    $(document).ready(function() {
        var outsumbut="<button style='margin-left:10%;font-size: 12px;width: 150px;' class='greenb regButton' id='outsumGo'>Outgoing Summary</button>";
        var incsumbut="<button style='margin-left:10%;font-size: 12px;width: 150px;' class='greenb regButton' id='incsumGo'>Incomings Summary</button>";
        $("#contselectorAIPog").after(outsumbut);
        $("#contselectorAIPic").after(incsumbut);
        $("#outsumGo").click(function() {
            makeoutsum();
        });
        $("#incsumGo").click(function() {
            makeincsum();
        });
    });
    function makeoutsum() {
        var tg={id:[],se:[],oth:[],ts:[],x:[],y:[],cn:[],pn:[],an:[]}
        var i=0;
        $("#outgoingAttacksTable tr").each(function() {
            if ($($(this).children()[0]).is( "td" )) {
                var tid=$($($($(this).children()[6]).children()).children()).attr("data");
                if (tg.id.indexOf(tid)<0) {
                    tg.id.push(tid);
                    tg.se.push(0);
                    tg.oth.push(0);
                    tg.ts.push(0);
                    tg.cn.push($($($(this).children()[5]).children()).html());
                    tg.an.push($($(this).children()[4]).html());
                    tg.pn.push($($(this).children()[3]).html());
                    var tempx=Number(tid % 65536);
                    var tempy=Number((tid-tempx)/65536);
                    tg.x.push(tempx);
                    tg.y.push(tempy);
                }
                var ti=tg.id.indexOf(tid);
                if ($($(this).children()[2]).html()=="Siege" || $($(this).children()[2]).html()=="Sieging") {
                    tg.se[ti]++;
                } else {
                    tg.oth[ti]++;
                }
                tg.ts[ti]+=Number($($(this).children()[10]).html());
                //console.log(++i);
            }
        });
        var outsumwin="<div id='outsumWin' style='width:50%;height:40%;left:30%;z-index:4000;' class='popUpBox'><div class='popUpBar'><span class='ppspan'>Outgoing Attacks Summary</span><button class='greenb regButton' style='font-size: 14px;margin-left: 10px;margin-top: 1px;height: 90%;width: 60px;'><div class='button'><a href='#' id ='outsumexp' role='button' style='color:white;'>Export</a></div></button><button id='sumX' onclick=\"$('#outsumWin').remove();\" class='xbutton greenb'><div id='xbuttondiv'><div><div id='centxbuttondiv'></div></div></div></button></div><div id=outsumbody' class='popUpWindow'><div class='beigemenutable scroll-pane' style='height:53%;width: 98%;margin-left: 1%;'><table id='outsumTab'><thead><tr><th>Player</th><th>Alliance</th><th>Target</th><th>Coords</th><th>Number of sieges</th><th>Number of non siege</th><th>Total TS</th></tr></thead><tbody>";
        for (var i in tg.id) {
            outsumwin+="<tr><td class='playerblink'>"+tg.pn[i]+"</td><td class='allyblink'>"+tg.an[i]+"</td><td class='cityblink'>"+tg.cn[i]+"</td><td class='coordblink shcitt' data='"+tg.id[i]+"'>"+tg.x[i]+":"+tg.y[i]+"</td><td>"+tg.se[i]+"</td><td>"+tg.oth[i]+"</td><td>"+tg.ts[i].toLocaleString()+"</td></tr>";
        }
        outsumwin+="</tbody></table></div></div></div>";
        $("body").append(outsumwin);
        $("#outsumWin").draggable({ handle: ".popUpBar" , containment: "window", scroll: false});
        $("#outsumWin").resizable();
        var newTableObject = document.getElementById('outsumTab');
        sorttable.makeSortable(newTableObject);
        $("#outsumTab td").css("text-align","center");
        $("#outsumexp").click(function(event) {
            //var outputFile = window.prompt("What do you want to name your output file (Note: This won't have any effect on Safari)") || 'export';
            var outputFile = 'OutgoingSum'+today.getDate()+Number(today.getMonth()+1)+today.getFullYear()+'.csv';

            // CSV
            exportTableToCSV.apply(this, [$('#outsumTab'), outputFile]);
        });
        setTimeout(function() {
            $("#outsumWin").css("z-index","4000");
        },300);
    }
    function makeincsum() {
        var tg={id:[],hos:[],fr:[],x:[],y:[],cn:[],pn:[]}
        var i=0;
        $("#incomingsAttacksTable tr").each(function() {
            if ($($(this).children()[0]).is( "td" )) {
                var tid=$($($($(this).children()[4]).children()).children()).attr("data");
                if (tg.id.indexOf(tid)<0) {
                    tg.id.push(tid);
                    tg.hos.push(0);
                    tg.fr.push(0);
                    tg.cn.push($($($(this).children()[3]).children()).html());
                    tg.pn.push($($(this).children()[2]).html());
                    var tempx=Number(tid % 65536);
                    var tempy=Number((tid-tempx)/65536);
                    tg.x.push(tempx);
                    tg.y.push(tempy);
                }
                var ti=tg.id.indexOf(tid);
                if (cotg.player.name()==$($(this).children()[7]).html()) {
                    tg.fr[ti]++;
                } else {
                    tg.hos[ti]++;
                }
                //console.log(++i);
            }
        });
        var outsumwin="<div id='outsumWin' style='width:50%;height:40%;left:30%;z-index:4000;' class='popUpBox'><div class='popUpBar'><span class='ppspan'>Incoming Attacks Summary</span><button class='greenb regButton' style='font-size: 14px;margin-left: 10px;margin-top: 1px;height: 90%;width: 60px;'><div class='button'><a href='#' id ='outsumexp' role='button' style='color:white;'>Export</a></div></button><button id='sumX' onclick=\"$('#outsumWin').remove();\" class='xbutton greenb'><div id='xbuttondiv'><div><div id='centxbuttondiv'></div></div></div></button></div><div id=outsumbody' class='popUpWindow'><div class='beigemenutable scroll-pane' style='height:53%;width: 98%;margin-left: 1%;'><table id='outsumTab'><thead><tr><th>Player</th><th>Target</th><th>Coords</th><th>Internal Attacks</th><th>Hostile Attacks</th></tr></thead><tbody>";
        for (var i in tg.id) {
            outsumwin+="<tr><td class='playerblink'>"+tg.pn[i]+"</td><td class='cityblink'>"+tg.cn[i]+"</td><td class='coordblink shcitt' data='"+tg.id[i]+"'>"+tg.x[i]+":"+tg.y[i]+"</td><td>"+tg.fr[i]+"</td><td>"+tg.hos[i]+"</td></tr>";
        }
        outsumwin+="</tbody></table></div></div></div>";
        $("body").append(outsumwin);
        $("#outsumWin").draggable({ handle: ".popUpBar" , containment: "window", scroll: false});
        $("#outsumWin").resizable();
        var newTableObject = document.getElementById('outsumTab');
        sorttable.makeSortable(newTableObject);
        $("#outsumTab td").css("text-align","center");
        $("#outsumexp").click(function(event) {
            //var outputFile = window.prompt("What do you want to name your output file (Note: This won't have any effect on Safari)") || 'export';
            var outputFile = 'IncomingSum'+today.getDate()+Number(today.getMonth()+1)+today.getFullYear()+'.csv';

            // CSV
            exportTableToCSV.apply(this, [$('#outsumTab'), outputFile]);
        });
        setTimeout(function() {
            $("#outsumWin").css("z-index","4000");
        },300);
    }
    // combat reports summary
    $(document).ready(function() {
        var comsum="<button id='comsumGo' class='regButton greenb' style='margin-top: 40%;width: 95%;height: 25%;font-size: .8vw;'>Combat Summary</button>";
        var comsump="<button id='comsumpGo' class='regButton greenb' style='margin-left:3%;width: 45%;height: 26px;font-size: 12px;'>Combat Summary</button>";
        $("#announcementsGo").css("width","45%");
        $("#locatecityGo").after(comsum);
        $("#announcementsGo").after(comsump);
        $("#comsumGo").click(function() {
            comsumwin("city");
        });
        $("#comsumpGo").click(function() {
            comsumwin("player");
        });
        //$("head").append("<script type='text/javascript' src='https://cdn.datatables.net/1.10.12/js/jquery.dataTables.min.js'></script>");
    });
    //combat sum window
    function comsumwin(arg) {
        var comswin="<div id='comsumWin' style='width:60%;height:65%;left:30%' class='popUpBox'><div class='popUpBar'><span class='ppspan'>Combat Reports Summary</span><button id='sumX' onclick=\"$('#comsumWin').remove();\" class='xbutton greenb'><div id='xbuttondiv'><div><div id='centxbuttondiv'></div></div></div></button></div><div id=comsumbody' class='popUpWindow'><span style='margin-left:5%;'>Pick a Date to retrieve combat summary:    </span><input style='width:90px;' id='comsumDat' type='text' value='00/00/0000'><button class='regButton greenbuttonGo greenb' id='getcomSum' style='width:10%;margin-left:5%'> Go </Button><div id='comsumTabbody' style='margin:1%;'></div></div></div>";
        $("body").append(comswin);
        $("#comsumWin").draggable({ handle: ".popUpBar" , containment: "window", scroll: false});
        $("#comsumWin").resizable();
        $("#comsumDat").datepicker();
        $("#getcomSum").click(function() {
            comsumtab(arg);
        });
    }
    // combat sum table
    function comsumtab(arg) {
        var ata={sent:[0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],lost:[0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],survive:[0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0]};
        var ats={sent:[0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],lost:[0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],survive:[0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0]};
        var dt={sent:[0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],lost:[0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],survive:[0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0]};
        var aally=[];
        var dally=[];
        var apl=[];
        var aplcount=[];
        var dpl=[];
        var spl=[];
        var ajc=0;
        var siege={src:[],troops:[],ts:[]};
        var assc={real:0,fake:0};
        var siec={real:0,fake:0};
        var pluc={real:0,fake:0};
        var scoc=0;
        var day=$("#comsumDat").val().substring(3,5);
        var month=$("#comsumDat").val().substring(0,2);
        var rlines
        if (arg=="city") {
           // console.log(arg);
            rlines=$("#bottomspotinfo tr");
        }
        if (arg=="player") {
            //console.log(arg);
            rlines=$("#reportstBody tr");
        }
        //console.log(rlines);
        if( rlines.length > 0 ){
            var r=0;
            var loop=function() {
                var rday,rmonth,thiss,rid;
                if (arg=="city") {
                    thiss=rlines[r];
                    rday=$(thiss).children().first().next().html().substring(9,11);
                    rmonth=$(thiss).children().first().next().html().substring(12,14);
                }
                if (arg=="player") {
                    thiss=rlines[r];
                    rday=$(thiss).children().first().next().next().html().substring(9,11);
                    rmonth=$(thiss).children().first().next().next().html().substring(12,14);
                }
                if (day==rday && month==rmonth) {
                    if (arg=="city") {
                        rid=$(thiss).children().first().attr("data");
                    }
                    if (arg=="player") {
                        rid=$(thiss).children().first().next().attr("data");
                    }
                    var dat={r:rid};
                    jQuery.ajax({url: 'includes/gFrep.php',type: 'POST',aysnc:false,data: dat,
                                 success: function(data) {
                                     var rcounter="<span>Scanning Reports - "+r+"/"+rlines.length+"</span>";
                                     $("#comsumTabbody").html(rcounter);
                                     ajc++;
                                     var rdata=JSON.parse(data);
                                     //console.log(rdata.apan);
                                     //console.log(rdata.tpan);
                                     //console.log(aally.indexOf(rdata.apan));
                                     if (aally.indexOf(rdata.apan)<0) {
                                         aally.push(rdata.apan);
                                     }
                                     if (dally.indexOf(rdata.tpan)<0) {
                                         dally.push(rdata.tpan);
                                     }
                                     if (dpl.indexOf(rdata.tpn)<0) {
                                         dpl.push(rdata.tpn);
                                     }
                                     if (rdata.type==3) {
                                         scoc++;
                                     }
                                     if (rdata.type==1) {
                                         var tempv=[0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0];
                                         var tempts=0;
                                         $.each(rdata.ats, function(key, value) {
                                             tempv[key]=value;
                                             tempts+=value*ttts[key];
                                         });
                                         var tempcoord=rdata.acx+":"+rdata.acy;
                                         if (siege.src.indexOf(tempcoord)<0) {
                                             if (tempts<5000) {
                                                 siec.fake++;
                                             } else {
                                                 siec.real++;
                                             }
                                             if (apl.indexOf(rdata.apn)<0) {
                                                 apl.push(rdata.apn);
                                                 aplcount[apl.indexOf(rdata.apn)]=1;
                                             } else {
                                                 aplcount[apl.indexOf(rdata.apn)]+=1;
                                             }
                                             siege.src.push(tempcoord);
                                             siege.troops.push(tempv);
                                             siege.ts.push(tempts);
                                         } else {
                                             if (siege.ts[siege.src.indexOf(tempcoord)]<tempts) {
                                                 if (tempts>=5000 && siege.ts[siege.src.indexOf(tempcoord)]<5000) {
                                                     siec.fake--;
                                                     siec.real++;
                                                 }
                                                 siege.troops[siege.src.indexOf(tempcoord)]=tempv;
                                                 siege.ts[siege.src.indexOf(tempcoord)]=tempts;
                                             }
                                         }
                                         $.each(rdata.atlo, function(key, value) {
                                             ats.lost[key]+=value;
                                         });
                                         $.each(rdata.atle, function(key, value) {
                                             ats.survive[key]+=value;
                                         });
                                     } else {
                                         if (apl.indexOf(rdata.apn)<0) {
                                             apl.push(rdata.apn);
                                             aplcount[apl.indexOf(rdata.apn)]=1;
                                         } else {
                                             aplcount[apl.indexOf(rdata.apn)]+=1;
                                         }
                                         var tempts=0;
                                         $.each(rdata.ats, function(key, value) {
                                             ata.sent[key]+=value;
                                             tempts+=value*ttts[key];
                                         });
                                         //console.log(tempts);
                                         if (rdata.type==2) {
                                             if (tempts<5000) {
                                                 assc.fake++;
                                             } else {
                                                 assc.real++;
                                             }
                                         }
                                         if (rdata.type==0) {
                                             if (tempts<5000) {
                                                 pluc.fake++;
                                             } else {
                                                 pluc.real++;
                                             }
                                         }
                                         $.each(rdata.atlo, function(key, value) {
                                             ata.lost[key]+=value;
                                         });
                                         $.each(rdata.atle, function(key, value) {
                                             ata.survive[key]+=value;
                                         });
                                     }
                                     if (rdata.tts=="none") { //estimating killed troops by the puries
                                         var tskilled=rdata.puri.w*4000/170;
                                         dt.lost[18]+=Math.floor(tskilled);
                                     } else {
                                         for (var i in rdata.ttle) {
                                             dt.survive[i]+=rdata.ttle[i];
                                             dt.sent[i]+=rdata.tts[i];
                                             dt.lost[i]+=rdata.ttlo[i];
                                         }
                                         if ('reinforce' in rdata) {
                                             $.each(rdata.reinforce, function() {
                                                 if (spl.indexOf(this.pn)<0) {
                                                     spl.push(this.pn);
                                                 }
                                                 $.each(this.ttle, function (key, value) {
                                                     dt.survive[key]+=value;
                                                 });
                                                 $.each(this.ttlo, function (key, value) {
                                                     dt.lost[key]+=value;
                                                 });
                                                 $.each(this.tts, function (key, value) {
                                                     dt.sent[key]+=value;
                                                 });
                                             });
                                         }
                                     }
                                 }
                                });
                    if( ++r < rlines.length ){
                        setTimeout(loop, 500);
                    } else {
                        setTimeout(function() {
                            for (var i in siege.troops) {
                                for (var j in siege.troops[i]) {
                                    ats.sent[j]+=siege.troops[i][j];
                                }
                            }
                            drawcomsumtable();
                        }, 100*ajc);
                    }
                } else {
                    if( ++r < rlines.length ){
                        setTimeout(loop, 0);
                    } else {
                        setTimeout(function() {
                            for (var i in siege.troops) {
                                for (var j in siege.troops[i]) {
                                    ats.sent[j]+=siege.troops[i][j];
                                }
                            }
                            drawcomsumtable();
                        }, 100*ajc);
                    }
                }
            };
            loop();
        }
        function drawcomsumtable() {
            //console.log(at,dt);
            //console.log(aally,dally);
            //console.log(assc.real,assc.fake,siec.real,siec.fake,pluc.real,pluc.fake);
            var atps=[];
            var dtps=[];
            var tassent=0;
            var taasent=0;
            var taadied=0;
            var tasdied=0;
            var tddied=0;
            var comsumbody="<p><span style='font-weight:bold;color:#980d0d;'>Combat Summary for "+$("#CNjsspan").text()+"("+$("#coordspan").text()+")"+" on "+$("#comsumDat").val()+"</span><br><span>Reports: "+ajc+"</span>";
            if (assc.real>0) {
                comsumbody+=" | Real assaults: "+assc.real+"</span>";
            }
            if (assc.fake>0) {
                comsumbody+=" | Fake assaults: "+assc.fake+"</span>";
            }
            if (siec.real>0) {
                comsumbody+=" | Real sieges: "+siec.real+"</span>";
            }
            if (siec.fake>0) {
                comsumbody+=" | Fake sieges: "+siec.fake+"</span>";
            }
            if (pluc.real>0) {
                comsumbody+=" | Real plunders: "+pluc.real+"</span>";
            }
            if (pluc.fake>0) {
                comsumbody+=" | Fake plundes: "+pluc.fake+"</span>";
            }
            if (scoc>0) {
                comsumbody+=" | Scouts: "+scoc+"</span>";
            }
            comsumbody+="<br><span>Attacking alliance: "+aally+"</span><br>";
            comsumbody+="<span> Attackers: ";
            for (var i in apl) {
            comsumbody+=apl[i]+"("+aplcount[i]+") ";
            }
            comsumbody+="</span><div id='comsumatab' class='beigemenutable'>";
            comsumbody+="<table><thead><tr><th style='width:100px;height:45px;'></th>";
            for (var i in ata.sent) {
                if (ata.sent[i]>0 || ats.sent[i]>0) {
                    atps.push(i);
                    comsumbody+="<th><div class='"+tpicdiv[i]+"'></div></th>";
                }
            }
            comsumbody+="<th>Total</th></tr></thead><tbody><tr><td>Non-siege Sent</td>";
            for (var i in atps) {
                comsumbody+="<td>"+ata.sent[atps[i]].toLocaleString()+"</td>";
                taasent+=ata.sent[atps[i]]*ttts[atps[i]];
            }
            comsumbody+="<td>"+taasent.toLocaleString()+"</td></tr><tr><td>Non-siege Lost</td>";
            for (var i in atps) {
                comsumbody+="<td>"+ata.lost[atps[i]].toLocaleString()+"</td>";
                taadied+=ata.lost[atps[i]]*ttts[atps[i]];
            }
            comsumbody+="<td>"+taadied.toLocaleString()+"</td>";
            comsumbody+="<tr><td>Siege Sent</td>";
            for (var i in atps) {
                comsumbody+="<td>"+ats.sent[atps[i]].toLocaleString()+"</td>";
                tassent+=ats.sent[atps[i]]*ttts[atps[i]];
            }
            comsumbody+="<td>"+tassent.toLocaleString()+"</td></tr><tr><td>Siege Lost</td>";
            for (var i in atps) {
                comsumbody+="<td>"+ats.lost[atps[i]].toLocaleString()+"</td>";
                tasdied+=ats.lost[atps[i]]*ttts[atps[i]];
            }
            comsumbody+="<td>"+tasdied.toLocaleString()+"</td></tr></tbody></table>";
            comsumbody+="<span>Total Sent: "+(taasent+tassent).toLocaleString()+"</span><br><span>Total Lost: "+(taadied+tasdied).toLocaleString()+"</span></div></p><p><span>Defending alliance: "+dally+"</span><br>";
            comsumbody+="<span>Defender: "+dpl+"</span>";
            if (spl.length>0) {
                comsumbody+="<br><span>Supporters: ";
                for (var i in spl) {
                    comsumbody+=spl[i] +" ";
                }
                comsumbody+=" </span>";
            }
            comsumbody+="<div id='comsumdtab' class='beigemenutable'>";
            comsumbody+="<table><thead><tr><th style='width:100px;height:45px;'></th>";
            for (var i in dt.sent) {
                if (dt.sent[i]>0) {
                    dtps.push(i);
                    comsumbody+="<th><div class='"+tpicdiv[i]+"'></div></th>";
                }
            }
            comsumbody+="<th>Total(seen)</th><th>Total(Estimated)</th></tr></thead><tbody><tr><td>Lost</td>";
            for (var i in dtps) {
                comsumbody+="<td>"+dt.lost[dtps[i]].toLocaleString()+"</td>";
                tddied+=dt.lost[dtps[i]]*ttts[dtps[i]];
            }
            comsumbody+="<td>"+tddied.toLocaleString()+"</td><td>"+dt.lost[18].toLocaleString()+"</td></tr></tbody></table><span>Total lost: "+(tddied+dt.lost[18]).toLocaleString()+"</span></div></p>";
            $("#comsumTabbody").html(comsumbody);
            $("#comsumatab td").css({"text-align":"center","font-size":"15px"});
            $("#comsumatab th").css({"text-align":"center","font-size":"15px"});
            $("#comsumdtab td").css({"text-align":"center","font-size":"15px"});
            $("#comsumdtab th").css({"text-align":"center","font-size":"15px"});
        }
    }
    //set food warnings function
    $(document).ready(function() {
        var used=false;
        $("#councillorGo").click(function() {
            if (used==false) {
                used=true;
                var allfw="<button class='greenb' id='allfwGo' style='margin-top: 15px;margin-left:30px;font-size: 12px;border-radius: 6px;'>Save for city list</button>";
                var fwcityl=$("#organiser").clone(false).attr({id:"fwselc",style:"width:30%;height:30px;font-size:12 !important;border-radius:6px;margin-top:15px"});
                $("#saveFWchanges").before(allfw);
                $("#allfwGo").before(fwcityl);
                $("#allfwGo").click(function () {
                    var clist;
                    $.each(poll2.player.clc, function(key, value) {
                        if (key==$("#fwselc").val()) {
                            clist=value;
                        }
                    });
                    for (var i in clist) {
                        var cid=clist[i];
                        var dat={cid: cid, a: $("#FWhours").val()};
                        jQuery.ajax({url: 'includes/svFW.php',type: 'POST',aysnc:false,data: dat});
                    }
                });
            }
        });
    });
    // boss raiding tool
    /*$(document).ready(function() {
        jQuery.ajax({url: 'includes/pD.php',type: 'POST',aysnc:false,
                     success: function(data) {
                         pdata=JSON.parse(data);
                         //console.log(pdata);
                     }
                    });
        setTimeout(function(){
            //$("#cityDropdownMenu").change();
            var cid=$("#cityDropdownMenu").val();
            var dat={a:cid};
            jQuery.ajax({url: 'includes/gC.php',type: 'POST',aysnc:false, data: dat});
        },5000);*/
        var returnAllbut="<button id='returnAllb' style='right: 37%; margin-top: 55px;width: 150px;height: 30px !important; font-size: 12px !important; position: absolute;' class='regButton greenb'>Return All</button>";
        var raidbossbut="<button id='raidbossGo' style='left: 63%;margin-left: 10px;margin-top: 15px;width: 150px;height: 30px !important; font-size: 12px !important; position: absolute;' class='regButton greenb'>Locate Bosses</button>";
        var attackbut="<button id='attackGo' style='right: 67%;margin-left: 10px;margin-top: 55px;width: 150px;height: 30px !important; font-size: 12px !important; position: absolute;' class='regButton greenb'>Attack Sender</button>";
        var defbut="<button id='defGo' style='left: 63%;margin-left: 10px;margin-top: 55px;width: 150px;height: 30px !important; font-size: 12px !important; position: absolute;' class='regButton greenb'>Defense Sender</button>";
        var quickdefbut="<button id='quickdefCityGo' style='width:96%; margin-top:2%; margin-left:2%;' class='regButton greenbuttonGo greenb'>@ Quick Reinforcements @</button>";
        var neardefbut="<button id='ndefGo' style='left: 4%;margin-left: 10px;margin-top: 95px;width: 150px;height: 30px !important; font-size: 12px !important; position: absolute;' class='regButton greenb'> Nearest Defense</button>";
        var addtoatt="<button id='addtoAtt' style='margin-left: 7%;margin-top: -5%;width: 40%;height: 26px !important; font-size: 12px !important;' class='regButton greenb'>Add to Attack Sender</button>";
        var addtodef="<button id='addtoDef' style='margin-left: 7%;width: 40%;height: 26px !important; font-size: 12px !important;' class='regButton greenb'>Add to Defense Sender</button>";
        //bosstab
        var bosstab="<li id='bosshuntab' class='ui-state-default ui-corner-top' role='tab' tabindex='-1' aria-controls='warBossmanager'";
        bosstab+="aria-labeledby='ui-id-20' aria-selected='false' aria-expanded='false'>";
        bosstab+="<a href='#warBossmanager' class='ui-tabs-anchor' role='presentation' tabindex='-1' id='ui-id-20'>Find Bosses</a></li>";
        var bosstabbody="<div id='warBossmanager' aria-labeledby='ui-id-20' class='ui-tabs-panel ui-widget-content ui-corner-bottom' ";
        bosstabbody+=" role='tabpanel' aria-hidden='true' style='display: none;'><div id='fpdcdiv3' class='redheading' style='margin-left: 2%;' >CFunky's Boss Raiding tool:</div>";
        bosstabbody+="<div id='bossbox' class='beigemenutable scroll-pane' style='width: 96%; height: 85%; margin-left: 2%;'></div>";
        bosstabbody+="<div id='idletroops'></div></div>";
        //attack tab
        var attacktab="<li id='attacktab' class='ui-state-default ui-corner-top' role='tab' tabindex='-1' aria-controls='warAttackmanager'";
        attacktab+="aria-labeledby='ui-id-21' aria-selected='false' aria-expanded='false'>";
        attacktab+="<a href='#warAttackmanager' class='ui-tabs-anchor' role='presentation' tabindex='-1' id='ui-id-21'>Attack</a></li>";
        var attacktabbody="<div id='warAttackmanager' aria-labeledby='ui-id-21' class='ui-tabs-panel ui-widget-content ui-corner-bottom'  role='tabpanel' aria-hidden='true' style='display: none;'><div id='fpdcdiv3' class='redheading' style='margin-left: 2%;' >Attack Sender:</div><div id='attackbox' class='beigemenutable scroll-pane' style='width: 53%; height: 50%; float:left; margin-left: 1%; margin-right: 1%;'><table><thead><th></th><th>X</th><th>Y</th><th>Type</th></thead><tbody>";
        for (var i=1;i<8;i++) {
            attacktabbody+="<tr><td>Target "+i+" </td><td><input id='t"+i+"x' type='number' style='width: 85%'></td><td><input id='t"+i+"y' type='number' style='width: 85%'></td>";
            attacktabbody+="<td><select id='type"+i+"' class='greensel' style='font-size: 15px !important;width:95%;height:30px;'><option value='0'>Fake</option><option value='1'>Real</option></select></td></tr>";
        }
        attacktabbody+="</tbody></table></div><div id='picktype' class='beigemenutable scroll-pane' style='width: 43%; height: 50%;'></div><table><tr><td><span>Use percentage of troops:</span></td><td><input id='perc' type='number' style='width: 30px'>%</td><td></td></tr><tr><td><span>Send real as:</span></td><td><select id='realtype' class='greensel' style='font-size: 15px !important;width:95%;height:30px;'><option value='0'>Assault</option><option value='1'>Siege</option><option value='2'>Plunder</option></select></td><td></td></tr><tr><td><span>Send fake as:</span></td><td><select id='faketype' class='greensel' style='font-size: 15px !important;width:95%;height:30px;'><option value='0'>Assault</option><option value='1'>Siege</option><option value='2'>Plunder</option></select></td><td></td></tr><tr><td><input id='retcheck' class='clsubopti' type='checkbox' checked> Return all Troops</td><td colspan=2><input id='retHr' type='number' style='width: 20px' value='2'> Hours before attack</td></tr></table><table style='width:96%;margin-left:2%'><thead><tr style='text-align:center;'><th></th><th>Hr</th><th>Min</th><th>Sec</th><th colspan='2'>Date</th></tr><tr><td>Set Time: </td><td><input id='attackHr' type='number' style='width: 35px;height: 22px;font-size: 10px;' value='10'></td><td><input id='attackMin' style='width: 35px;height: 22px;font-size: 10px;' type='number' value='00'></td><td><input style='width: 35px;height: 22px;font-size: 10px;' id='attackSec' type='number' value='00'></td><td colspan='2'><input style='width:90px;' id='attackDat' type='text' value='00/00/0000'></td></tr></tbody></table><table style='margin-left: 10%; margin-top:20px;'><tbody><tr><td style='width: 20%'><button id='Attack' style='width: 95%;height: 30px !important; font-size: 12px !important;' class='regButton greenb'>Attack!</button></td><td style='width: 20%'><button id='Aexport' style='width: 95%;height: 30px !important; font-size: 12px !important;' class='regButton greenb'>Export Order</button></td><td style='width: 20%'><button id='Aimport' style='width: 95%;height: 30px !important; font-size: 12px !important;' class='regButton greenb'>Import Order</button></td></tr></tbody></table>";
        // defend tab
        var deftab="<li id='deftab' class='ui-state-default ui-corner-top' role='tab' tabindex='-1' aria-controls='warDefmanager'";
        deftab+="aria-labeledby='ui-id-22' aria-selected='false' aria-expanded='false'>";
        deftab+="<a href='#warDefmanager' class='ui-tabs-anchor' role='presentation' tabindex='-1' id='ui-id-22'>Defend</a></li>";
        var deftabbbody="<div id='warDefmanager' aria-labeledby='ui-id-21' class='ui-tabs-panel ui-widget-content ui-corner-bottom' ";
        deftabbbody+=" role='tabpanel' aria-hidden='true' style='display: none;'><div id='fpdcdiv3' class='redheading' style='margin-left: 2%;' >Defense Sender:</div>";
        deftabbbody+="<div><p style='font-size: 10px;'>Defense sender will split all the troops you choose to send according to the number of targets you input.</p></div>";
        deftabbbody+="<div id='defbox' class='beigemenutable scroll-pane' style='width: 53%; height: 50%; float:left; margin-left: 1%; margin-right: 1%;'>";
        deftabbbody+="<table><thead><th></th><th>X</th><th>Y</th></thead><tbody>";
        for (var i=1;i<15;i++) {
            deftabbbody+="<tr><td>Target "+i+" </td><td><input id='d"+i+"x' type='number' style='width: 85%'></td><td><input id='d"+i+"y' type='number' style='width: 85%'></td></tr>";
        }
        deftabbbody+="</tbody></table></div>";
        deftabbbody+="<div id='dpicktype' class='beigemenutable scroll-pane' style='width: 43%; height: 50%;'></div>";
        deftabbbody+="<table><tr><td><span>Use percentage of troops:</span></td><td><input id='defperc' type='number' style='width: 30px'>%</td><td></td></tr>";
        deftabbbody+="<tr><td><span>Select Departure:</span></td><td><select id='defdeparture' class='greensel' style='font-size: 15px !important;width:95%;height:30px;'>";
        deftabbbody+="<option value='0'>Now</option><option value='1'>Departure time</option><option value='2'>Arrival time</option></select></td><td></td></tr>";
        deftabbbody+="<tr id='dret'><td><input id='dretcheck' class='clsubopti' type='checkbox' checked> Return all Troops</td><td colspan=2><input id='dretHr' type='number' style='width: 20px' value='2'> Hours before departure</td></tr></table>";
        deftabbbody+="<table id='deftime' style='width:96%;margin-left:2%'><thead><tr style='text-align:center;'><th></th><th>Hr</th><th>Min</th><th>Sec</th><th colspan='2'>Date</th></tr>";
        deftabbbody+="<tr><td>Set Time: </td><td><input id='defHr' type='number' style='width: 35px;height: 22px;font-size: 10px;' value='10'></td><td><input id='defMin' style='width: 35px;height: 22px;font-size: 10px;' type='number' value='00'></td>";
        deftabbbody+="<td><input style='width: 35px;height: 22px;font-size: 10px;' id='defSec' type='number' value='00'></td><td colspan='2'><input style='width:90px;' id='defDat' type='text' value='00/00/0000'></td></tr></tbody></table>";
        deftabbbody+="<button id='Defend' style='width: 35%;height: 30px; font-size: 12px; margin:10px;' class='regButton greenb'>Send Defense</button>";
        var ndeftab="<li id='neardeftab' class='ui-state-default ui-corner-top' role='tab'>";
        ndeftab+="<a href='#warNdefmanager' class='ui-tabs-anchor' role='presentation'>Near Def</a></li>";
        var ndeftabbody="<div id='warNdefmanager' class='ui-tabs-panel ui-widget-content ui-corner-bottom' ";
        ndeftabbody+=" role='tabpanel' style='display: none;'><div id='fpdcdiv3' class='redheading' style='margin-left: 2%;' >Nearest defense:</div>";
        ndeftabbody+="<table><td colspan='2'> Choose city:</td><td><input style='width: 30px;height: 22px;font-size: 10px;' id='ndefx' type='number'> : <input style='width: 30px;height: 22px;font-size: 10px;' id='ndefy' type='number'></td>";
        ndeftabbody+="<td><button class='regButton greenb' id='ndefup' style='height:30px; width:70px;'>Update</button></td></table>";
        ndeftabbody+="<div id='Ndefbox' class='beigemenutable scroll-pane' style='width: 96%; height: 85%; margin-left: 2%;'></div>";

        //var expwin="<div id='ExpImp' style='width:250px;height:200px;background-color: #E2CBAC;-moz-border-radius: 10px;-webkit-border-radius: 10px;border-radius: 10px;border: 4px ridge #DAA520;position:absolute;right:100px;top:100px; z-index:1000000;'><div class=\"popUpBar\"> <span class=\"ppspan\">Import/Export attack orders</span>";
        var expwin="<div id='ExpImp' style='width:250px;height:200px;' class='popUpBox ui-draggable'><div class=\"popUpBar\"><span class=\"ppspan\">Import/Export attack orders</span>";
        expwin+="<button id=\"cfunkyX\" onclick=\"$('#ExpImp').remove();\" class=\"xbutton greenb\"><div id=\"xbuttondiv\"><div><div id=\"centxbuttondiv\"></div></div></div></button></div><div id='expbody' class=\"popUpWindow\">"
        expwin+="<textarea style='font-size:11px;width:97%;margin-left:1%;height:17%;' id='expstring' maxlength='200'></textarea><button id='applyExp' style='margin-left: 15px; width: 100px;height: 30px !important; font-size: 12px !important;' class='regButton greenb'>Apply</button></div></div>";

        var tabs = $( "#warcouncTabs" ).tabs();
        var ul = tabs.find( "ul" );
        $( bosstab ).appendTo( ul );
        $( attacktab ).appendTo( ul );
        $( deftab ).appendTo( ul );
        $( ndeftab ).appendTo( ul );
        tabs.tabs( "refresh" );
        $("#warcouncbox").css("width","460px")

        $('#warCidlemanager').after(bosstabbody);
        $('#warBossmanager').after(attacktabbody);
        $('#warAttackmanager').after(deftabbbody);
        $('#warDefmanager').after(ndeftabbody);
        $("#senddefCityGo").after(quickdefbut);
        $("#deftime").hide();
        $("#dret").hide();
        $("#defdeparture").change(function() {
           if ($("#defdeparture").val()==0) {
               $("#deftime").hide();
               $("#dret").hide();
           } else {
               $("#deftime").show();
               $("#dret").show();
           }
        });
        if (localStorage.getItem('attperc')) {
            $("#perc").val(localStorage.getItem('attperc'));
        } else {$("#perc").val(99);}
        if (localStorage.getItem('defperc')) {
            $("#defperc").val(localStorage.getItem('defperc'));
        } else {$("#defperc").val(99);}
        if (localStorage.getItem('retcheck')) {
            if (localStorage.getItem('retcheck')==1) {
                $("#retcheck").prop( "checked", true );
            }
            if (localStorage.getItem('retcheck')==0) {
                $("#retcheck").prop( "checked", false );
            }
        }
        if (localStorage.getItem('dretcheck')) {
            if (localStorage.getItem('rdetcheck')==1) {
                $("#dretcheck").prop( "checked", true );
            }
            if (localStorage.getItem('dretcheck')==0) {
                $("#dretcheck").prop( "checked", false );
            }
        }
        if (localStorage.getItem('retHr')) {
            $("#retHr").val(localStorage.getItem('retHr'));
        }
        if (localStorage.getItem('dretHr')) {
            $("#dretHr").val(localStorage.getItem('dretHr'));
        }
        $( "#attackDat" ).datepicker();
        $( "#defDat" ).datepicker();
        $('#bosshuntab').click(function() {
            if (beentoworld)
            {
                openbosswin();
            } else {
                alert("Press World Button");
            }
        });
        $("#warCounc").append(returnAllbut);
        $("#warCounc").append(attackbut);
        $("#warCounc").append(defbut);
        $("#warCounc").append(neardefbut);
        $("#coordstochatGo1").after(addtoatt);
        $("#addtoAtt").after(addtodef);
        $("#loccavwarconGo").css("right","65%");
        $("#idluniwarconGo").css("left","34%");
        $("#idluniwarconGo").after(raidbossbut);
        $('#returnAllb').click(function() {
            jQuery.ajax({url: 'includes/gIDl.php',type: 'POST',aysnc:false,
                         success: function(data) {
                             var thdata=JSON.parse(data);
                             $("#returnAll").remove();
                             openreturnwin(thdata);
                         }
                        });
        });
        $('#raidbossGo').click(function() {
            if (beentoworld)
            {
                $("#warcouncbox").show();
                tabs.tabs( "option", "active", 2 );
                $("#bosshuntab").trigger({type:"click",originalEvent:"1"});
            } else {
                alert("Press World Button");
            }
        });
        $('#attackGo').click(function() {
            $("#warcouncbox").show();
            tabs.tabs( "option", "active", 3 );
            $("#attacktab").trigger({type:"click",originalEvent:"1"});
        });
        $('#defGo').click(function() {
            $("#warcouncbox").show();
            tabs.tabs( "option", "active", 4 );
            $("#deftab").trigger({type:"click",originalEvent:"1"});
        });
        $('#ndefGo').click(function() {
            $("#warcouncbox").show();
            tabs.tabs( "option", "active", 5 );
            $("#neardeftab").trigger({type:"click",originalEvent:"1"});
        });
        $("#addtoAtt").click(function() {
            for (var i=1;i<8;i++) {
                if (!$("#t"+i+"x").val()) {
                    var tid=Number($("#showReportsGo").attr("data"));
                    var tempx;
                    var tempy;
                    tempx=Number(tid % 65536);
                    tempy=Number((tid-tempx)/65536);
                    $("#t"+i+"x").val(tempx);
                    $("#t"+i+"y").val(tempy);
                    break;
                }
            }
        });
        $("#addtoDef").click(function() {
            for (var i=1;i<15;i++) {
                if (!$("#d"+i+"x").val()) {
                    var tid=Number($("#showReportsGo").attr("data"));
                    var tempx;
                    var tempy;
                    tempx=Number(tid % 65536);
                    tempy=Number((tid-tempx)/65536);
                    $("#d"+i+"x").val(tempx);
                    $("#d"+i+"y").val(tempy);
                    break;
                }
            }
        });
        $("#Attack").click(function() {
            localStorage.setItem('attperc',$("#perc").val());
            localStorage.setItem('retHr',$("#retHr").val());
            if ($("#retcheck").prop( "checked")==true) {
                localStorage.setItem('retcheck',1);
            }
            if ($("#retcheck").prop( "checked")==false) {
                localStorage.setItem('retcheck',0);
            }
            SendAttack();
        });
        $("#Defend").click(function() {
            localStorage.setItem('defperc',$("#defperc").val());
            localStorage.setItem('dretHr',$("#dretHr").val());
            var defobj={targets:{x:[],y:[],dist:[],numb:0,cstr:[]},t:{tot:[],home:[],type:[],use:[],speed:[],amount:[]},perc:$("#defperc").val(),dep:$("#defdeparture").val(),ret:1,rettime:$("#dretHr").val(),hr:$("#defHr").val(),min:$("#defMin").val(),sec:$("#defSec").val(),date:$("#defDat").val(),dat:$("#defDat").datepicker('getDate')};
            if ($("#dretcheck").prop( "checked")==true) {
                localStorage.setItem('dretcheck',1);
                defobj.ret=1;
            }
            if ($("#dretcheck").prop( "checked")==false) {
                localStorage.setItem('dretcheck',0);
                defobj.ret=0;
            }
            var tempx;
            var tempy;
            for (var i=1;i<15;i++) {
                if ($("#d"+i+"x").val()) {
                    tempx=$("#d"+i+"x").val();
                    tempy=$("#d"+i+"y").val();
                    //console.log(tempx,tempy);
                    defobj.targets.x.push(tempx);
                    defobj.targets.y.push(tempy);
                    defobj.targets.cstr.push(tempx+":"+tempy);
                    defobj.targets.dist.push(Math.sqrt((tempx-city.x)*(tempx-city.x)+(tempy-city.y)*(tempy-city.y)));
                    defobj.targets.numb++;
                }
            }
            for (var i in poll2.city.tc) {
                if (poll2.city.tc[i]) {
                    defobj.t.tot.push(Math.ceil(poll2.city.tc[i]*Number($("#defperc").val())/100));
                    defobj.t.home.push(Math.ceil(poll2.city.th[i]*Number($("#defperc").val())/100));
                    defobj.t.type.push(Number(i));
                    if ($("#usedef"+i).prop( "checked")==true) {
                        defobj.t.speed.push(ttspeed[i]/ttspeedres[i]);
                        defobj.t.use.push(1);
                    } else {
                        defobj.t.speed.push(0)
                        defobj.t.use.push(0);
                    }
                    defobj.t.amount.push(0);
                }
            }
            SendDef(defobj);
        });
        $("#quickdefCityGo").click(function() {
            var tid=Number($("#showReportsGo").attr("data"));
            var tempx;
            var tempy;
            tempx=Number(tid % 65536);
            tempy=Number((tid-tempx)/65536);
            var defobj={targets:{x:[tempx],y:[tempy],dist:[],numb:1},t:{home:[],type:[],use:[],speed:[],amount:[]},perc:100,dep:0,ret:0,rettime:0,hr:0,min:0,sec:0,dat:0};
            defobj.targets.dist.push(Math.sqrt((tempx-city.x)*(tempx-city.x)+(tempy-city.y)*(tempy-city.y)));
            for (var i in poll2.city.th) {
                if (poll2.city.th[i]) {
                    defobj.t.home.push(Math.ceil(poll2.city.th[i]*Number($("#defperc").val())/100));
                    defobj.t.type.push(Number(i));
                    defobj.t.speed.push(ttspeed[i]/ttspeedres[i]);
                    defobj.t.use.push(1);
                    defobj.t.amount.push(0);
                }
            }
            SendDef(defobj);
        });
        $("#ndefup").click(function() {
            jQuery.ajax({url: 'overview/trpover.php',type: 'POST',aysnc:false,
                         success: function(data) {
                             var t=JSON.parse(data);
                             neardeftable(t);
                         }
                        });
        });
        $("#Aexport").click(function() {
            //$("body").append(expwin);
            //$("#ExpImp").draggable({ handle: ".popUpBar" , containment: "window", scroll: false});
            //$("#ExpImp").show();
            //console.log("exp");
            var Aexp={x:[],y:[],type:[],time:[]};
            for (var i=1;i<8;i++) {
                if ($("#t"+i+"x").val()) {
                    Aexp.x.push($("#t"+i+"x").val());
                    Aexp.y.push($("#t"+i+"y").val());
                    Aexp.type.push($("#type"+i).val());
                }
            }
            Aexp.time[0]=$("#attackHr").val();
            Aexp.time[1]=$("#attackMin").val();
            Aexp.time[2]=$("#attackSec").val();
            Aexp.time[3]=$("#attackDat").val();
            //$("#expstring").val(JSON.stringify(Aexp));
            var aa=prompt("Attack Orders Expot", JSON.stringify(Aexp));
        });
        $("#Aimport").click(function() {
            $("body").append(expwin);
            $("#ExpImp").draggable({ handle: ".popUpBar" , containment: "window", scroll: false});
            document.addEventListener('paste', function (evt) {
                //console.log(evt.clipboardData.getData('text/plain'));
                $("#expstring").val(evt.clipboardData.getData('text/plain'));
            });
            $("#applyExp").click(function() {
                Aimp($("#expstring").val());
                $("#ExpImp").remove();
              });
        });
    //});
    //import attack orders
    function Aimp(str) {
        Aexp=JSON.parse(str);
        //console.log(Aexp);
        for (var i=1; i<=Aexp.x.length; i++) {
            //console.log(i)
            $("#t"+i+"x").val(Aexp.x[i-1]);
            $("#t"+i+"y").val(Aexp.y[i-1]);
            $("#type"+i).val(Aexp.type[i-1]).change();
        }
        $("#attackHr").val(Aexp.time[0]);
        $("#attackMin").val(Aexp.time[1]);
        $("#attackSec").val(Aexp.time[2]);
        $("#attackDat").val(Aexp.time[3]);
    }
    // generating attack info
    function updateattack() {
        var t={home:[],type:[]};
        for (var i in poll2.city.tc) {
            if (poll2.city.tc[i]) {
                t.home.push(poll2.city.tc[i]);
                t.type.push(i);
            }
        }
        $.each(poll2.city.tq, function() {
            if (this.ttype==17) {
                if (t.type.indexOf(17)<0) {
                    t.type.push(17);
                    t.home.push(this.tc);
                } else {
                    t.home[t.type.indexOf(17)]+=this.tc;
                }
            }
        });
        var ttseltab="<table><thead><th>Troop Type</th><th>Use for real</th><th>Use for fake</th></thead><tbody>";
        for (var i in t.home) {
            //ttseltab+="<tr><td>"+ttname[t.type[i]]+"</td><td><input id='usereal"+t.type[i]+"' class='clsubopti' type='checkbox' checked></td>";
            ttseltab+="<tr><td style='height:40px;'><div class='"+tpicdiv[t.type[i]]+"'></div></td><td style='text-align: center;'><input id='usereal"+t.type[i]+"' class='clsubopti' type='checkbox' checked></td>";
            ttseltab+="<td style='text-align: center;'><input id='usefake"+t.type[i]+"' class='clsubopti' type='checkbox' checked></td></tr>";
        }
        ttseltab+="</tbody></table>";
        $("#picktype").html(ttseltab);
    }
    // generating defense info
    function updatedef() {
        var t={home:[],type:[]};
        for (var i in poll2.city.tc) {
            if (poll2.city.tc[i]) {
                t.home.push(poll2.city.tc[i]);
                t.type.push(i);
            }
        }
        var ttseltab="<table><thead><th>Troop Type</th><th>Use</th></thead><tbody>";
        for (var i in t.home) {
            //ttseltab+="<tr><td>"+ttname[t.type[i]]+"</td><td><input id='usereal"+t.type[i]+"' class='clsubopti' type='checkbox' checked></td>";
            ttseltab+="<tr><td style='height:40px;'><div class='"+tpicdiv[t.type[i]]+"'></div></td><td style='text-align: center;'><input id='usedef"+t.type[i]+"' class='clsubopti' type='checkbox' checked></td></tr>";
        }
        ttseltab+="</tbody></table>";
        $("#dpicktype").html(ttseltab);
    }
    // generating nearest def info
    function neardeftable(t) {
        var cx=$("#ndefx").val();
        var cy=$("#ndefy").val();
        var cont=Number(Math.floor(cx/100)+10*Math.floor(cy/100));
        var cit={x:[],y:[],dist:[],cn:[],thome:[],ts:[],id:[],time:[]};
        //console.log(cx,cy,cont);
        for (var i in t) {
            var tid=t[i].id;
            var tempx=Number(tid % 65536);
            var tempy=Number((tid-tempx)/65536);
            var tcont=Number(Math.floor(tempx/100)+10*Math.floor(tempy/100));
            //console.log(cont,tcont);
            if (cont==tcont) {
                if (t[i].Ballista_total>0 || t[i].Ranger_total>0 || t[i].Triari_total>0 || t[i].Priestess_total || t[i].Arbalist_total>0 || t[i].Praetor_total>0) {
                    cit.x.push(tempx);
                    cit.y.push(tempy);
                    var tdist=roundToTwo(Math.sqrt((tempx-cx)*(tempx-cx)+(tempy-cy)*(tempy-cy)));
                    cit.dist.push(tdist);
                    var tempt=[0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0];
                    tempt[1]=t[i].Ballista_total;
                    tempt[2]=t[i].Ranger_total;
                    tempt[3]=t[i].Triari_total;
                    tempt[4]=t[i].Priestess_total;
                    tempt[8]=t[i].Arbalist_total;
                    tempt[9]=t[i].Praetor_total;
                    var tempts=0;
                    for (var j in tempt) {
                        tempts+=tempt[j]*ttts[j];
                    }
                    cit.ts.push(tempts);
                    cit.thome.push(tempt);
                    cit.cn.push(t[i].c);
                    cit.id.push(tid);
                    var tspeed=0;
                    for (var j in tempt) {
                        if (tempt[j]>0) {
                            if (ttspeed[j]>tspeed) {
                                tspeed=ttspeed[j];
                            }
                        }
                    }
                    cit.time.push(tdist*tspeed);
                }
            }
        }
        var neardeftab="<table id='ndeftable'><thead><th></th><th>City</th><th>Coords</th><th>TS</th><th id='ndefdist'>Travel Time</th><th>type</th></thead><tbody>";
        for (var i in cit.x) {
            neardeftab+="<tr><td><button class='greenb chcity' id='cityGoTowm' a='"+cit.id[i]+"'>Go To</button></td><td>"+cit.cn[i]+"</td><td class='coordblink shcitt' data='"+cit.id[i]+"'>"+cit.x[i]+":"+cit.y[i]+"</td>";
            //style='font-size: 9px;border-radius: 6px;width: 80%;height: 22px;padding: 0;white-space: nowrap;'
            neardeftab+="<td>"+cit.ts[i]+"</td><td>"+Math.floor(cit.time[i]/60)+"h "+Math.floor(cit.time[i]%60)+"m</td><td><table>";
            for (var j in cit.thome[i]) {
                if (cit.thome[i][j]>0) {
                    neardeftab+="<td><div class='"+tpicdiv20[j]+"'></div></td>";
                }
            }
            neardeftab+="</table></td></tr>";
        }
        neardeftab+="</tbody></table>";
        $("#Ndefbox").html(neardeftab);
        $("#ndeftable td").css("text-align","center");
        $("#ndeftable td").css("height","26px");
        var newTableObject = document.getElementById('ndeftable');
        sorttable.makeSortable(newTableObject);
        $("#ndefdist").trigger({type:"click",originalEvent:"1"});
        $("#ndefdist").trigger({type:"click",originalEvent:"1"});
    }


    function clickevent(element) {
        var event = jQuery.Event("click");
            event.user = "foo";
    }


    //sending defense
    function SendDef(defobj) {
        $("#commandsPopUpBox").show();
        //setTimeout(function() {
        //    $("#commandsPopUpBox").hide();
        //},300);
        var commandtabs=$("#commandsdiv").tabs();
        commandtabs.tabs( "option", "active", 2 );
        $("#reintabb").trigger({type:"click",originalEvent:"1"});
        var targets=defobj.targets;
        var tarnumb=defobj.targets.numb;
        var t=defobj.t;
        var maxdist=Math.max.apply(Math, targets.dist);
        var time;
        //console.log(targets,tarnumb);
        //galley defend
        if (t.type.indexOf(14)>-1) {
            if (t.use[t.type.indexOf(14)]==1) {
                time=ttspeed[14]/ttspeedres[14]*maxdist;
                //console.log(time);
                var gali=t.type.indexOf(14);
                if (defobj.dep==0) {
                    var galnumb=Math.floor(t.home[gali]/tarnumb);
                } else {
                    var galnumb=Math.floor(t.tot[gali]/tarnumb);
                }
                var maxts=0;
                for (var i in t.home) {
                    if (i!=gali) {
                        if (t.use[i]==1) {
                            if (t.type[i]!=15) {
                                if (defobj.dep==0) {
                                    maxts+=Math.floor(t.home[i]*ttts[t.type[i]]/tarnumb);
                                } else {
                                    maxts+=Math.floor(t.tot[i]*ttts[t.type[i]]/tarnumb);
                                }
                            }
                        }
                    }
                }
                if (maxts<=galnumb*500) {
                    t.amount[gali]=Math.ceil(maxts/500);
                    for (var i in t.home) {
                        if (i!=gali) {
                            if (t.use[i]==1) {
                                if (defobj.dep==0) {
                                    t.amount[i]=Math.floor(t.home[i]/tarnumb);
                                } else {
                                    t.amount[i]=Math.floor(t.tot[i]/tarnumb);
                                }
                            }
                        }
                    }
                } else {
                    var rat=galnumb*500/maxts;
                    t.amount[gali]=galnumb;
                    for (var i in t.home) {
                        if (i!=gali) {
                            if (t.use[i]==1) {
                                if (t.type[i]!=15) {
                                    if (defobj.dep==0) {
                                        t.amount[i]=Math.floor(rat*t.home[i]/tarnumb);
                                    } else {
                                        t.amount[i]=Math.floor(rat*t.tot[i]/tarnumb);
                                    }
                                } else {
                                     if (defobj.dep==0) {
                                        t.amount[i]=Math.floor(t.home[i]/tarnumb);
                                    } else {
                                        t.amount[i]=Math.floor(t.tot[i]/tarnumb);
                                    }
                                }
                            }
                        }
                    }
                }
            }
            // normal def
        } else {
            time=Math.max.apply(Math, t.speed)*maxdist;
            for (var i in t.home) {
                if (t.use[i]==1) {
                    if (defobj.dep==0) {
                        t.amount[i]=Math.floor(t.home[i]/tarnumb);
                    } else {
                        t.amount[i]=Math.floor(t.tot[i]/tarnumb);
                    }
                }
            }
        }
        // sending def
        //console.log(t);
        var l=0; var end=targets.x.length;
        function dloop() {
            for (var i in t.home) {
                if (t.use[i]==1) {
                    $("#reiIP"+t.type[i]).val(t.amount[i]);
                }
            }
            $("#reinxcoord").val(targets.x[l]);
            $("#reinycoord").val(targets.y[l]);
            setTimeout(function(){
                $("#reinfcoordgo").trigger({type:"click",originalEvent:"1"});
            },100);
            $("#reinforcetimingselect").val(Number(defobj.dep)+1).change();
            if ($("#defdeparture").val()>0) {
                //console.log(defobj.min,defobj.hr,defobj.sec);
                var date=defobj.date+" "+defobj.hr+":"+defobj.min+":"+defobj.sec;
                $("#reinfotimeinp").val(date);
            }

            var event = jQuery.Event( "logged" );
            event.user = "foo";
            //$("#reinforceGo").trigger({type:"click",originalEvent:"1"});
            $("#reinforceGo").trigger({
                  type:"click",
                  originalEvent:"1"
                });
            l++;
            if (l<end) {
                setTimeout( dloop, 1500 );
            } else {
                $("#commandsPopUpBox").hide();
                setTimeout( function() {
                    art();
                }, 4000 );
            }
        }
        dloop();
        function art() { //setting return time for raids according to city view outgoing list
            //console.log(poll2.OGA);
            $("#commandsPopUpBox").hide();
            if (defobj.ret==1) {
                $(".toptdinncommtbl1:first").trigger({type:"click",originalEvent:"1"});
                setTimeout(function() {
                    $("#outgoingPopUpBox").hide();
                },500);
                var minddate = new Date();
                var first=true;
                for (var i in poll2.OGA) {
                    //console.log(targets.cstr,poll2.OGA[i][5]);
                    if (targets.cstr.indexOf(poll2.OGA[i][5])>-1) {
                        if (first) {
                            first=false;
                            var a=poll2.OGA[i][6].substr(30);
                            var b=a.substr(0,a.indexOf('<'));
                            var time=b.split(" ");
                            var ttime=time[2].split(":");
                            minddate.setHours(Number(ttime[0]));
                            minddate.setMinutes(Number(ttime[1]));
                            minddate.setSeconds(Number(ttime[2]));
                            //console.log(time[1]);
                            if (time[1]=="Tomorrow") {
                                minddate.setDate(minddate.getDate() + 1);
                                //console.log("tmrw");
                            } else if (time[1]!="Today") {
                                var ddate=time[1].split("/");
                                //console.log(ddate);
                                minddate.setDate(Number(ddate[1]));
                                minddate.setMonth(Number(ddate[0]));
                            }
                        } else {
                            var a=poll2.OGA[i][6].substr(30);
                            var b=a.substr(0,a.indexOf('<'));
                            var time=b.split(" ");
                            var ttime=time[2].split(":");
                            var d=new Date();
                            d.setHours(ttime[0]);
                            d.setMinutes(ttime[1]);
                            d.setSeconds(ttime[2]);
                            if (time[1]=="Tomorrow") {
                                //console.log("tmrw");
                                d.setDate(minddate.getDate() + 1);
                            } else if (time[1]!="Today") {
                                var ddate=time[1].split("/");
                                //console.log(ddate);
                                d.setDate(ddate[1]);
                                d.setMonth(ddate[0]);
                            }
                            //console.log(d,minddate);
                            if (d<minddate) {
                                minddate=d;
                            }
                        }
                    }
                }
                minddate.setHours(minddate.getHours()-defobj.rettime);
                //console.log(minddate);
                var hour=minddate.getHours();
                if (hour<10) {
                    hour="0"+hour;
                }
                var min=minddate.getMinutes();
                if (min<10) {
                    min="0"+min;
                }
                var sec=minddate.getSeconds();
                if (sec<10) {
                    sec="0"+sec;
                }
                var retdate=getFormattedDate(minddate)+" "+hour+":"+min+":"+sec;
                //console.log(retdate);
                $("#raidrettimesela").val(3).change();
                $("#raidrettimeselinp").val(retdate);
                //$("#donepoll2.OGAll").trigger({type:"click",originalEvent:"1"});
                $("#donepoll2.OGAll").trigger({
                  type:"click",
                  originalEvent:"1"
                });
                alert("Defense set and troops returned");
            } else {
                alert("Defense set");
            }
        }
        //console.log(defobj.ret,defobj.dep);
        /*if (defobj.ret==1) { //setting raids return time according to script calculated time, doesn't work for portals
            //console.log(1);
            $(".toptdinncommtbl1:first").trigger({type:"click",originalEvent:"1"});
            setTimeout(function() {
                $("#outgoingPopUpBox").hide();
            },500);
            if (defobj.dep=="2") {
               // console.log(12);
                var rh=Number(defobj.rettime);
                var hr=Number(defobj.hr)-(Math.floor(time/60)+rh);
                //console.log(hr);
                var returndate=defobj.dat;
                var min=defobj.min-Math.floor(time%60);
                if (min<0) {
                    min+=60;
                    hr-=1;
                }
                //console.log(hr);
                if (hr<0 && hr>=-24) {
                    hr+=24;
                    returndate.setDate(returndate.getDate() - 1);
                }
                //console.log(hr);
                if (hr<-24 && hr>= -48) {
                    hr+=48;
                    returndate.setDate(returndate.getDate() - 2);
                }
                if (hr<-48) {
                    hr+=72;
                    returndate.setDate(returndate.getDate() - 3);
                }
                //console.log(hr);
                if (hr<10) {hr="0"+hr;}
                //console.log(hr);
                //console.log(min,hr)
                var retdate=getFormattedDate(returndate)+" "+hr+":"+min+":"+defobj.sec;
                //console.log(retdate);
                $("#raidrettimesela").val(3).change();
            }
            if (defobj.dep=="1") {
                //console.log(11);
                var rh=Number(defobj.rettime);
                var hr=Number(defobj.hr)-rh;
                var returndate=defobj.dat;
                if (hr<0) {
                    hr+=24;
                    returndate.setDate(returndate.getDate() - 1);
                }
                var retdate=getFormattedDate(returndate)+" "+hr+":"+defobj.min+":"+defobj.sec;
                $("#raidrettimesela").val(3).change();
            }
            $("#raidrettimeselinp").val(retdate);
            $("#donepoll2.OGAll").trigger({type:"click",originalEvent:"1"});
        }*/
    }
    //sending attacks
    function SendAttack() {
        $("#commandsPopUpBox").show();
        //setTimeout(function() {
        //    $("#commandsPopUpBox").hide();
        //},300);
        var commandtabs=$("#commandsdiv").tabs();
        var pvptabs=$("#pvpTab").tabs();
        $("#pvptabb").trigger({type:"click",originalEvent:"1"});
        commandtabs.tabs( "option", "active", 1 );
        var targets={x:[],y:[],real:[],dist:[],cstr:[]};
        var fakenumb=0;
        var realnumb=0;
        var tempx;
        var tempy;
        for (var i=1;i<8;i++) {
            if ($("#t"+i+"x").val()) {
                tempx=$("#t"+i+"x").val();
                tempy=$("#t"+i+"y").val();
                //console.log(tempx,tempy);
                targets.x.push(tempx);
                targets.y.push(tempy);
                targets.cstr.push(tempx+":"+tempy);
                targets.real.push($("#type"+i).val());
                if ($("#type"+i).val()==1) {realnumb+=1;}
                else {fakenumb+=1;}
                targets.dist.push(Math.sqrt((tempx-city.x)*(tempx-city.x)+(tempy-city.y)*(tempy-city.y)));
            }
        }
        //console.log(fakenumb,realnumb);
        //console.log(targets);
        var t={home:[],type:[],real:[],fake:[],speed:[]};
        console.log(poll2.city.tc);
        for (var i in poll2.city.tc) {
            if (poll2.city.tc[i]) {
                t.home.push(Math.ceil(poll2.city.tc[i]*Number($("#perc").val())/100));
                t.type.push(Number(i));
                if ($("#usereal"+i).prop( "checked")==true) {
                t.speed.push(ttspeed[i]/ttspeedres[i]);
                } else {t.speed.push(0)}
            }
        }
        $.each(poll2.city.tq, function() {
            if (this.ttype==17) {
                if (t.type.indexOf(17)<0) {
                    t.type.push(17);
                    t.home.push(this.tc);
                } else {
                    t.home[t.type.indexOf(17)]+=this.tc;
                }
            }
        });
        var maxdist=Math.max.apply(Math, targets.dist);
        console.log(t);
        //console.log(t.type.indexOf(14));
        var time;
        //galley attack
        if (t.type.indexOf(14)>-1 && $("#usereal14").prop( "checked")==true) {
            time=ttspeed[14]/ttspeedres[14]*maxdist;
            //console.log(time);
            var gali=t.type.indexOf(14);
            var galnumb=Math.floor((t.home[gali]-5*fakenumb)/realnumb);
            t.real[gali]=galnumb;
            t.fake[gali]=5;
            var galcap=500*galnumb;
            var nongalts=0;
            for (var i in t.home) {
                if (i!=gali && t.type[i]!=17) {
                    if ($("#usereal"+t.type[i]).prop( "checked")==true) {
                        if ($("#usefake"+t.type[i]).prop( "checked")==true) {
                            nongalts+=ttts[t.type[i]]*(t.home[i]-Math.ceil(2500/ttts[t.type[i]])*fakenumb)/realnumb;
                        } else {nongalts+=ttts[t.type[i]]*(t.home[i])/realnumb;}
                    }
                }
                if (t.type[i]==17) {
                    if ($("#usereal"+t.type[i]).prop( "checked")==true) {
                        if ($("#usefake"+t.type[i]).prop( "checked")==true) {
                            if (t.home[i]>fakenumb) {
                                nongalts+=(t.home[i]-fakenumb)/realnumb;
                            } else {
                                nongalts+=1;
                            }
                        } else {
                            nongalts+=t.home[i]/realnumb;
                        }
                    }
                }
            }
            var fakerat=0;
            for (var i in t.home) {
                if (i!=gali) {
                    if ($("#usefake"+t.type[i]).prop( "checked")==true) {
                        fakerat+=ttts[t.type[i]]*t.home[i];
                    }
                }
            }
            for (var i in t.home) {
                if (i!=gali && t.type[i]!=17) {
                    if ($("#usefake"+t.type[i]).prop( "checked")==true) {
                        t.fake[i]=Math.ceil(2500*t.home[i]/fakerat);
                    }
                }
                if (t.type[i]==17) {
                    if ($("#usereal"+t.type[i]).prop( "checked")==true) {
                        if ($("#usefake"+t.type[i]).prop( "checked")==true) {
                            if (t.home[i]>=fakenumb+realnumb) {
                                t.fake[i]=1;
                                t.real[i]=t.home[i]-fakenumb;
                            } else {
                                t.fake[i]=0;
                                t.real[i]=t.home[i]/realnumb;
                            }
                        } else {
                            t.fake[i]=0;
                            t.real[i]=t.home[i]/realnumb;
                        }
                    } else if ($("#usefake"+t.type[i]).prop( "checked")==true) {
                        t.real[i]=0;
                        t.fake[i]=t.home[i]/fakenumb;
                    } else {
                        t.real[i]=0;
                        t.fake[i]=0;
                    }
                }
            }
            for (var i in t.home) {
                if (i!=gali && t.type[i]!=17) {
                    if ($("#usereal"+t.type[i]).prop( "checked")==true) {
                        if ($("#usefake"+t.type[i]).prop( "checked")==true) {
                            if (nongalts>galcap) {
                                t.real[i]=Math.floor(galcap/nongalts*(t.home[i]-t.fake[i]*fakenumb)/realnumb);
                            } else {
                                t.real[i]=Math.floor((t.home[i]-t.fake[i]*fakenumb)/realnumb);
                            }
                        } else {
                            if (nongalts>galcap) {
                                t.real[i]=Math.floor(galcap/nongalts*(t.home[i])/realnumb);
                            } else {
                                t.real[i]=Math.floor((t.home[i])/realnumb);
                            }
                            t.fake[i]=0;
                        }
                    }
                }
            }
        } else {
            time=Math.max.apply(Math, t.speed)*maxdist;
            var fakerat=0;
            for (var i in t.home) {
                if (t.type[i]!=17) {
                    if ($("#usefake"+t.type[i]).prop( "checked")==true) {
                        fakerat+=ttts[t.type[i]]*t.home[i];
                    }
                }
            }
            //console.log(fakerat)
            for (var i in t.home) {
                if (t.type[i]!=17) {
                    if ($("#usefake"+t.type[i]).prop( "checked")==true) {
                        t.fake[i]=Math.ceil(3000*t.home[i]/fakerat);
                    }
                }
            }
            for (var i in t.home) {
                if (t.type[i]!=17) {
                    if ($("#usereal"+t.type[i]).prop( "checked")==true) {
                        if ($("#usefake"+t.type[i]).prop( "checked")==true) {
                            t.real[i]=Math.floor((t.home[i]-t.fake[i]*fakenumb)/realnumb);
                        } else {
                            t.real[i]=Math.floor((t.home[i])/realnumb);
                        }
                    } else {t.real[i]=0;}
                }
                if (t.type[i]==17) {
                    if ($("#usereal"+t.type[i]).prop( "checked")==true) {
                        if ($("#usefake"+t.type[i]).prop( "checked")==true) {
                            if (t.home[i]>=fakenumb+realnumb) {
                                t.fake[i]=1;
                                t.real[i]=t.home[i]-fakenumb;
                            } else {
                                t.fake[i]=0;
                                t.real[i]=t.home[i]/realnumb;
                            }
                        } else {
                            t.fake[i]=0;
                            t.real[i]=t.home[i]/realnumb;
                        }
                    } else if ($("#usefake"+t.type[i]).prop( "checked")==true) {
                        t.real[i]=0;
                        t.fake[i]=t.home[i]/fakenumb;
                    } else {
                        t.real[i]=0;
                        t.fake[i]=0;
                    }
                }
            }
        }
        var l=0; var end=targets.x.length;
            function loop() {
                //console.log(l,end);
                if (targets.real[l]==1) {
                    if ($("#realtype").val()==0) {
                        pvptabs.tabs( "option", "active", 0 );
                        for (var i in t.real) {
                            $("#assIP"+t.type[i]).val(t.real[i]);
                        }
                        $("#assaultxcoord").val(targets.x[l]);
                        $("#assaultycoord").val(targets.y[l]);
                        setTimeout(function(){
                            //$("#assaultcoordgo").trigger({type:"click",originalEvent:"1"});
                            //$("#assaultcoordgo").trigger({type:"click",originalEvent:"1"});
                        },100);
                        $("#assaulttimingselect").val(3).change();
                        var date=$("#attackDat").val()+" "+$("#attackHr").val()+":"+$("#attackMin").val()+":"+$("#attackSec").val();
                        $("#assaulttimeinp").val(date);
                        $('#assaultGo').attr('disabled', false);
                        //$("#assaultGo").trigger({type:"click",originalEvent:"1"});
                       // $("#assaultGo").trigger({type:"click",originalEvent:"1"});
                        var event = jQuery.Event('click', {
                                                    'view': window,
                                                    'bubbles': true,
                                                    'cancelable': true
                                                  });
                        $("#assaultGo").trigger({type:"click",originalEvent:event});
                    }
                    if ($("#realtype").val()==1) {
                        pvptabs.tabs( "option", "active", 1 );
                        for (var i in t.real) {
                            $("#sieIP"+t.type[i]).val(t.real[i]);
                        }
                        $("#siexcoord").val(targets.x[l]);
                        $("#sieycoord").val(targets.y[l]);
                        setTimeout(function(){
                            //$("#siegecoordgo").trigger({type:"click",originalEvent:"1"});
                        },100);
                        $("#siegetimingselect").val(3).change();
                        var date=$("#attackDat").val()+" "+$("#attackHr").val()+":"+$("#attackMin").val()+":"+$("#attackSec").val();
                        $("#siegetimeinp").val(date);
                        $('#siegeGo').attr('disabled', false);
                       // $("#siegeGo").trigger({type:"click",originalEvent:"1"});
                        //$("#siegeGo").trigger({type:"click",originalEvent:"1"});
                        var event = jQuery.Event('click', {
                                                    'view': window,
                                                    'bubbles': true,
                                                    'cancelable': true
                                                  });
                        $("#siegeGo").trigger({type:"click",originalEvent:event});
                    }
                    if ($("#realtype").val()==2) {
                        pvptabs.tabs( "option", "active", 2 );
                        for (var i in t.real) {
                            $("#pluIP"+t.type[i]).val(Number(t.real[i]));
                        }
                        $("#pluxcoord").val(targets.x[l]);
                        $("#pluycoord").val(targets.y[l]);
                        setTimeout(function(){
                            //$("#plundercoordgo").trigger({type:"click",originalEvent:"1"});
                        },100);
                        $("#plundertimingselect").val(3).change();
                        var date=$("#attackDat").val()+" "+$("#attackHr").val()+":"+$("#attackMin").val()+":"+$("#attackSec").val();
                        $("#plundtimeinp").val(date);
                        $('#plunderGo').attr('disabled', false);
                        var event = jQuery.Event('click', {
                                                    'view': window,
                                                    'bubbles': true,
                                                    'cancelable': true
                                                  });
                        $("#plunderGo").trigger({type:"click",originalEvent:event});

                    }
                }
                if (targets.real[l]==0) {
                    //console.log("fake");
                    if ($("#faketype").val()==0) {
                        pvptabs.tabs( "option", "active", 0 );
                        //console.log("assault");
                        for (var i in t.real) {
                            $("#assIP"+t.type[i]).val(t.fake[i]);
                        }
                        $("#assaultxcoord").val(targets.x[l]);
                        $("#assaultycoord").val(targets.y[l]);
                        setTimeout(function(){
                           // $("#assaultcoordgo").trigger({type:"click",originalEvent:"1"});
                        },100);
                        $("#assaulttimingselect").val(3).change();
                        var date=$("#attackDat").val()+" "+$("#attackHr").val()+":"+$("#attackMin").val()+":"+$("#attackSec").val();
                        $("#assaulttimeinp").val(date);
                        $('#assaultGo').attr('disabled', false);
                       //$("#assaultGo").trigger({type:"click",originalEvent:"1"});
                        //$("#assaultGo").trigger({type:"click",originalEvent:"1"});
                        var event = jQuery.Event('click', {
                                                    'view': window,
                                                    'bubbles': true,
                                                    'cancelable': true
                                                  });
                        $("#assaultGo").trigger({type:"click",originalEvent:event});
                    }
                    if ($("#faketype").val()==1) {
                        pvptabs.tabs( "option", "active", 1 );
                        //console.log("siege");
                        for (var i in t.real) {
                            $("#sieIP"+t.type[i]).val(t.fake[i]);
                        }
                        $("#siexcoord").val(targets.x[l]);
                        $("#sieycoord").val(targets.y[l]);
                        setTimeout(function(){
                           // $("#siegecoordgo").trigger({type:"click",originalEvent:"1"});
                        },100);
                        $("#siegetimingselect").val(3).change();
                        var date=$("#attackDat").val()+" "+$("#attackHr").val()+":"+$("#attackMin").val()+":"+$("#attackSec").val();
                        $("#siegetimeinp").val(date);
                        $('#siegeGo').attr('disabled', false);
                        //$("#siegeGo").trigger({type:"click",originalEvent:"1"});
                         //$("#siegeGo").trigger({type:"click",originalEvent:"1"});
                         var event = jQuery.Event('click', {
                                                    'view': window,
                                                    'bubbles': true,
                                                    'cancelable': true
                                                  });
                        $("#siegeGo").trigger({type:"click",originalEvent:event});
                    }
                    if ($("#faketype").val()==2) {
                        pvptabs.tabs( "option", "active", 2 );
                        //console.log("plunder");
                        for (var i in t.real) {
                            $("#pluIP"+t.type[i]).val(t.fake[i]);
                        }
                        $("#pluxcoord").val(targets.x[l]);
                        $("#pluycoord").val(targets.y[l]);
                        setTimeout(function(){
                          //  $("#plundercoordgo").trigger({type:"click",originalEvent:"1"});
                        },100);
                        $("#plundertimingselect").val(3).change();
                        var date=$("#attackDat").val()+" "+$("#attackHr").val()+":"+$("#attackMin").val()+":"+$("#attackSec").val();
                        $("#plundtimeinp").val(date);
                        //$("#plunderGo").trigger({type:"click",originalEvent:"1"});
                        $('#plunderGo').attr('disabled', false);
                        var event = jQuery.Event('click', {
                                                    'view': window,
                                                    'bubbles': true,
                                                    'cancelable': true
                                                  });
                        $("#plunderGo").trigger({type:"click",originalEvent:event});
                    }
                }
                l++;
                if (l<end) {
                    setTimeout( loop, 1500 );
                } else {
                    setTimeout( function() {
                        art();
                    }, 4000 );
                }
            }
            loop();
        function art() { //setting return time for raids according to city view attacks list
            //console.log(poll2.OGA);
            $("#commandsPopUpBox").hide();
            if ($("#retcheck").prop( "checked")==true) {
                $(".toptdinncommtbl1:first").trigger({type:"click",originalEvent:"1"});
                setTimeout(function() {
                    $("#outgoingPopUpBox").hide();
                },500);
                var minddate = new Date();
                var first=true;
                for (var i in poll2.OGA) {
                    //console.log(targets.cstr,poll2.OGA[i][5]);
                    if (targets.cstr.indexOf(poll2.OGA[i][5])>-1) {
                        if (first) {
                            first=false;
                            var a=poll2.OGA[i][6].substr(30);
                            var b=a.substr(0,a.indexOf('<'));
                            var time=b.split(" ");
                            var ttime=time[2].split(":");
                            minddate.setHours(Number(ttime[0]));
                            minddate.setMinutes(Number(ttime[1]));
                            minddate.setSeconds(Number(ttime[2]));
                            //console.log(time[1]);
                            if (time[1]=="Tomorrow") {
                                minddate.setDate(minddate.getDate() + 1);
                                //console.log("tmrw");
                            } else if (time[1]!="Today") {
                                var ddate=time[1].split("/");
                                console.log(ddate);
                                minddate.setDate(Number(ddate[1]));
                                minddate.setMonth(Number(ddate[0]-1));
                                //console.log(minddate);
                            }
                        } else {
                            var a=poll2.OGA[i][6].substr(30);
                            var b=a.substr(0,a.indexOf('<'));
                            var time=b.split(" ");
                            var ttime=time[2].split(":");
                            var d=new Date();
                            d.setHours(ttime[0]);
                            d.setMinutes(ttime[1]);
                            d.setSeconds(ttime[2]);
                            if (time[1]=="Tomorrow") {
                                //console.log("tmrw");
                                d.setDate(minddate.getDate() + 1);
                            } else if (time[1]!="Today") {
                                var ddate=time[1].split("/");
                                //console.log(ddate);
                                d.setDate(ddate[1]);
                                d.setMonth(ddate[0]-1);
                                //console.log(minddate);
                            }
                            //console.log(d,minddate);
                            if (d<minddate) {
                                minddate=d;
                            }
                        }
                    }
                }
                minddate.setHours(minddate.getHours()-Number($("#retHr").val()));
                //console.log(minddate);
                var hour=minddate.getHours();
                if (hour<10) {
                    hour="0"+hour;
                }
                var min=minddate.getMinutes();
                if (min<10) {
                    min="0"+min;
                }
                var sec=minddate.getSeconds();
                if (sec<10) {
                    sec="0"+sec;
                }
                var retdate=getFormattedDate(minddate)+" "+hour+":"+min+":"+sec;
                //console.log(retdate);
                $("#raidrettimesela").val(3).change();
                $("#raidrettimeselinp").val(retdate);
                //$("#donepoll2.OGAll").trigger({type:"click",originalEvent:"1"});
                $("#donepoll2.OGAll").trigger({type:"click",originalEvent:"1"});
                alert("Attack set and troops returned");
            } else {
                alert("Attack set");
            }
        }
        //console.log(t);
        //return all troops according to script calculated travel time, doesn't include portal attacks.
        /*if ($("#retcheck").prop( "checked")==true) {
            $(".toptdinncommtbl1:first").trigger({type:"click",originalEvent:"1"});
            setTimeout(function() {
                $("#outgoingPopUpBox").hide();
            },500);
            var rh=Number($("#retHr").val());
            var hr=Number($("#attackHr").val())-(Math.floor(time/60)+rh);
            //console.log(hr);
            var returndate=$('#attackDat').datepicker('getDate');
            var min=$("#attackMin").val()-Math.floor(time%60);
            if (min<0) {
                min+=60;
                hr-=1;
            }
            //console.log(hr);
            if (hr<0 && hr>=-24) {
                hr+=24;
                returndate.setDate(returndate.getDate() - 1);
            }
            //console.log(hr);
            if (hr<-24 && hr>= -48) {
                hr+=48;
                returndate.setDate(returndate.getDate() - 2);
            }
            if (hr<-48) {
                hr+=72;
                returndate.setDate(returndate.getDate() - 3);
            }
            //console.log(hr);
            if (hr<10) {hr="0"+hr;}
            //console.log(hr);
            //console.log(min,hr)
            var retdate=getFormattedDate(returndate)+" "+hr+":"+min+":"+$("#attackSec").val();
            //console.log(retdate);
            $("#raidrettimesela").val(3).change();
            $("#raidrettimeselinp").val(retdate);
            $("#donepoll2.OGAll").trigger({type:"click",originalEvent:"1"});
        }*/
    }

    function openreturnwin(data) {
        //console.log(data);
        $(".toptdinncommtbl1:first").trigger({type:"click",originalEvent:"1"});
        setTimeout(function() {
            $("#outgoingPopUpBox").hide();
        },300);
        var selectcont=$("#idleCsel").clone(false).attr({id:"selcr",style:"width:40%;height:28px;font-size:11;border-radius:6px;margin:7px"});
        var returnwin="<div id='returnAll' style='width:300px;height:320px;background-color: #E2CBAC;-moz-border-radius: 10px;-webkit-border-radius: 10px;border-radius: 10px;border: 4px ridge #DAA520;position:absolute;right:100px;top:100px; z-index:1000000;'><div class=\"popUpBar\"> <span class=\"ppspan\">Return all troops in all cities</span>";
        returnwin+="<button id=\"cfunkyX\" onclick=\"$('#returnAll').remove();\" class=\"xbutton greenb\"><div id=\"xbuttondiv\"><div><div id=\"centxbuttondiv\"></div></div></div></button></div><div id='returnbody' class=\"popUpWindow\">"
        returnwin+="</div></div>";
        var selecttype="<select id='selType' class='greensel' style='width:50%;height:28px;font-size:11;border-radius:6px;margin:7px'><option value='1'>Offence and Defense</option><option value='2'>Offence</option><option value='3'>Defense</option></select><br>";
        var selectret=$("#raidrettimesela").clone(false).attr({id:"returnSel",style:"width:40%;height:28px;font-size:11;border-radius:6px;margin:7px"});
        var selecttime="<br><div id='timeblock' style='height:100px; width 95%'><div id='timesel' style='display: none;'><span style='text-align:left;font-weight:800;margin-left:2%;'>Input latest return time:</span><br><table style='width:80%;margin-left:10px'><thead><tr style='text-align:center;'><td>Hr</td><td>Min</td><td>Sec</td><td colspan='2'>Date</td></tr></thead><tbody>";
        selecttime+="<tr><td><input id='returnHr' type='number' style='width: 35px;height: 22px;font-size: 10px;padding: none !important;color: #000;' value='00'></td><td><input id='returnMin' style='width: 35px;height: 22px;font-size: 10px;padding: none !important;color: #000;' type='number' value='00'></td>";
        selecttime+="<td><input style='width: 35px;height: 22px;font-size: 10px;padding: none !important;color: #000;' id='returnSec' type='number' value='00'></td><td colspan='2'><input style='width:90px;' id='returnDat' type='text' value='00/00/0000'></td></tr></tbody></table></div></div>";
        var returnAllgo="<button id='returnAllGo' style='margin-left:30%; width: 35%;height: 30px !important; font-size: 12px !important; position: static !important;' class='regButton greenb'>Start Return All</button><br>";
        var nextcity="<button id='nextCity' style='display: none;margin-left:10%; width: 35%;height: 30px !important; font-size: 12px !important; position: static !important;' class='regButton greenb'>Next City</button>";
        var returntroops="<button id='returnTroops' style='display: none;margin-left:10%; width: 35%;height: 30px !important; font-size: 12px !important; position: static !important;' class='regButton greenb'>Return troops</button>";
        var selectlist=$("#organiser").clone(false).attr({id:"selClist",style:"width:40%;height:28px;font-size:11;border-radius:6px;margin:7px"});
        $("body").append(returnwin);
        $("#returnAll").draggable({ handle: ".popUpBar" , containment: "window", scroll: false});
        $("#returnbody").html(selectcont);
        //$("#selcr").attr("style","width:40%;height:28px;font-size:11;border-radius:6px;margin:7px");
        $("#selcr").after(selecttype);
        $("#selType").after(selectret);
        $("#returnSel").after(selectlist);
        $("#selClist").after(selecttime);
        $("#selClist").val("all").change();
        $(function() {
        $( "#returnDat" ).datepicker();
        });
        //$("#timesel").hide();
        $("#returnbody").append(returnAllgo);
        $("#returnAllGo").after(nextcity);
        $("#nextCity").after(returntroops);
        //$("#nextCity").hide();
        $("#returnSel").change(function() {
            if ($("#returnSel").val()==3) {
                $("#timesel").show();
            } else {
                $("#timesel").hide();
            }
        });
        var j,end,bb,cc,aa;
         var returncities={cid:[],cont:[]};
        $("#returnAllGo").click(function() {
            if ($("#selClist").val()=="all") {
                for (var i in data) {
                    var cont=data[i].c.co;
                    if ($("#selcr").val()==56) {
                        if($("#selType").val()==1) {
                            returncities.cid.push(data[i].i);
                            returncities.cont.push(cont);
                        }
                        if($("#selType").val()==2) {
                            if (data[i].tr.indexOf(5)>-1 || data[i].tr.indexOf(6)>-1 || data[i].tr.indexOf(10)>-1 || data[i].tr.indexOf(11)>-1 || data[i].tr.indexOf(12)>-1 || data[i].tr.indexOf(13)>-1 || data[i].tr.indexOf(14)>-1 || data[i].tr.indexOf(16)>-1) {
                                returncities.cid.push(data[i].i);
                                returncities.cont.push(cont);
                            }
                        }
                        if($("#selType").val()==3) {
                            if (data[i].tr.indexOf(1)>-1 || data[i].tr.indexOf(2)>-1 || data[i].tr.indexOf(3)>-1 || data[i].tr.indexOf(4)>-1 || data[i].tr.indexOf(8)>-1 || data[i].tr.indexOf(9)>-1 || data[i].tr.indexOf(15)>-1) {
                                returncities.cid.push(data[i].i);
                                returncities.cont.push(cont);
                            }
                        }
                    }
                    if (cont==Number($("#selcr").val())) {
                        if($("#selType").val()==1) {
                            returncities.cid.push(data[i].i);
                            returncities.cont.push(cont);
                        }
                        if($("#selType").val()==2) {
                            if (data[i].tr.indexOf(5)>-1 || data[i].tr.indexOf(6)>-1 || data[i].tr.indexOf(10)>-1 || data[i].tr.indexOf(11)>-1 || data[i].tr.indexOf(12)>-1 || data[i].tr.indexOf(13)>-1 || data[i].tr.indexOf(14)>-1 || data[i].tr.indexOf(16)>-1) {
                                returncities.cid.push(data[i].i);
                                returncities.cont.push(cont);
                            }
                        }
                        if($("#selType").val()==3) {
                            if (data[i].tr.indexOf(1)>-1 || data[i].tr.indexOf(2)>-1 || data[i].tr.indexOf(3)>-1 || data[i].tr.indexOf(4)>-1 || data[i].tr.indexOf(8)>-1 || data[i].tr.indexOf(9)>-1 || data[i].tr.indexOf(15)>-1) {
                                returncities.cid.push(data[i].i);
                                returncities.cont.push(cont);
                            }
                        }
                    }
                }
                //}
            } else {
                $.each(poll2.player.clc, function(key, value) {
                    if (key==$("#selClist").val()) {
                        returncities.cid=value;
                    }
                });
            }
            //console.log(returncities);
            $("#organiser").val("all").change();
            //$("#outgoingPopUpBox").open();
            bb=$("#returnSel").val();
            if (bb==3) {
                cc=$("#returnDat").val()+" "+$("#returnHr").val()+":"+$("#returnMin").val()+":"+$("#returnSec").val();
            } else {cc=0}
            j=0; end=returncities.cid.length;
            aa=returncities.cid[j];
            //console.log(aa);
            $("#cityDropdownMenu").val(aa).change();
            $("#returnTroops").show();
            $("#nextCity").show();
            //$("#returnAllGo").attr("id","nextCity").html("Next City");
            $("#returnAllGo").hide();
        });
        $("#returnTroops").click(function() {
            $("#raidrettimesela").val(bb).change();
            $("#raidrettimeselinp").val(cc);
            //$("#donepoll2.OGAll").trigger({type:"click",originalEvent:"1"});
            $("#donepoll2.OGAll").trigger({type:"click",originalEvent:"1"});

        });
        $("#nextCity").click(function() {
            j++;
            //console.log(j,end);
            if (j==end)  {
                alert("Return all complete");
                $("#returnAll").remove();
                         }
            else {
                aa=returncities.cid[j];
               //console.log(aa,j,end);
                $("#cityDropdownMenu").val(aa).change();
            }
        });
    }

    function openbosswin() {
        //console.log(bossinfo);
        var bosslist={name:[],x:[],y:[],lvl:[],distance:[],cid:[],time:[],cont:[]};
        for (var i in bossinfo.cont) {
            var distance=(Math.sqrt((bossinfo.x[i]-city.x)*(bossinfo.x[i]-city.x)+(bossinfo.y[i]-city.y)*(bossinfo.y[i]-city.y)));
            //if ((bossinfo.cont[i]==city.cont) && ((bossinfo.name[i]!="Triton")) && (bossinfo.data[i].charAt(0,1)==1)) {            
            if ((poll2.city.tc[2] || poll2.city.tc[3] || poll2.city.tc[4]|| poll2.city.tc[5]|| poll2.city.tc[6]|| poll2.city.tc[8]|| poll2.city.tc[9]|| poll2.city.tc[10]|| poll2.city.tc[11]) && poll2.city.tc[14]==0) {
                if (bossinfo.cont[i]==city.cont) {
                    if (poll2.city.tc[2] || poll2.city.tc[3] || poll2.city.tc[4]|| poll2.city.tc[5]|| poll2.city.tc[6]) {
                        var minutes=distance*ttspeed[2]/ttspeedres[2];
                        var time=Math.floor(minutes/60)+"h "+Math.floor(minutes % 60)+"m";
                    }
                    if (poll2.city.tc[8] || poll2.city.tc[9] || poll2.city.tc[10]|| poll2.city.tc[11]) {
                        var minutes=distance*ttspeed[8]/ttspeedres[8];
                        var time=Math.floor(minutes/60)+"h "+Math.floor(minutes % 60)+"m";
                    }
                    //bosslist.name.push(bossinfo.name[i]);
                    bosslist.x.push(bossinfo.x[i]);
                    bosslist.y.push(bossinfo.y[i]);
                    bosslist.cid.push(Number(bossinfo.y[i]*65536+bossinfo.x[i]));
                    bosslist.lvl.push(bossinfo.lvl[i]);
                    bosslist.distance.push(roundToTwo(distance));
                    bosslist.time.push(time);
                    bosslist.cont.push(bossinfo.cont[i]);
                }
            }
            if (distance<180) {
                if (poll2.city.tc[14] || poll2.city.tc[15] || poll2.city.tc[16]) {
                    var minutes=distance*ttspeed[14]/ttspeedres[14];
                    var time=Math.floor(minutes/60)+"h "+Math.floor(minutes % 60)+"m";
                    //bosslist.name.push(bossinfo.name[i]);
                    bosslist.x.push(bossinfo.x[i]);
                    bosslist.y.push(bossinfo.y[i]);
                    bosslist.cid.push(Number(bossinfo.y[i]*65536+bossinfo.x[i]));
                    bosslist.lvl.push(bossinfo.lvl[i]);
                    bosslist.distance.push(roundToTwo(distance));
                    bosslist.time.push(time);
                    bosslist.cont.push(bossinfo.cont[i]);
                }
            }
        }
        console.log(bosslist);
        //var bosswin="<table id='bosstable' class='beigetablescrollp sortable'><thead><tr><th></th><th>Type</th><th>Level</th><th>Coordinates</th><th>Travel Time</th><th id='hdistance'>Distance</th></tr></thead>";
        var bosswin="<table id='bosstable' class='beigetablescrollp sortable'><thead><tr><th>Coordinates</th><th>Level</th><th>Continent</th><th>Travel Time</th><th id='hdistance'>Distance</th></tr></thead>";
        bosswin+="<tbody>";
        for (var i in bosslist.x) {
            var j=bosses.name.indexOf(bosslist.name[i]);
            /*bosswin+="<tr id='bossline"+bosslist.cid[i]+"' class='dunginf'><td><button id='"+bosslist.cid[i]+"' class='greenb'>Hit Boss</button></td>";
            bosswin+="<td style='text-align: center;'><div class='"+bosses.pic[j]+"'></div></td><td style='text-align: center;'>"+bosslist.lvl[i]+"</td>";
            bosswin+="<td id='cl"+bosslist.cid[i]+"' class='coordblink shcitt' data='"+bosslist.cid[i]+"' style='text-align: center;'>"+bosslist.x[i]+":"+bosslist.y[i]+"</td><td style='text-align: center;'>"+bosslist.time[i]+"</td><td style='text-align: center;'>"+bosslist.distance[i]+"</td></tr>";*/
            bosswin+="<tr id='bossline"+bosslist.cid[i]+"' class='dunginf'><td id='cl"+bosslist.cid[i]+"' class='coordblink shcitt' data='"+bosslist.cid[i]+"' style='text-align: center;'>"+bosslist.x[i]+":"+bosslist.y[i]+"</td>";
            bosswin+="<td style='text-align: center;font-weight: bold;'>"+bosslist.lvl[i]+"</td><td style='text-align: center;'>"+bosslist.cont[i]+"</td>";
            bosswin+="<td style='text-align: center;'>"+bosslist.time[i]+"</td><td style='text-align: center;'>"+bosslist.distance[i]+"</td></tr>";
        }
        bosswin+="</tbody></table></div>";
        /*var idle="<table id='idleunits' class='beigetablescrollp'><tbody><tr><td style='text-align: center;'><span>Idle troops:</span></td>";
        for (var i in city.th) {
            var notallow=[0,1,7,12,13];
            if (notallow.indexOf(i)==-1) {
                if (city.th[i]>0) {
                    idle+="<td><div class='"+tpicdiv[i]+"' style='text-align: right;'></div></td><td style='text-align: left;'><span id='thbr"+i+"' style='text-align: left;'>"+city.th[i]+"</span></td>";
                }
            }
        }
        idle+="</tbody></table>";*/
        $("#bossbox").html(bosswin);
        //$("#idletroops").html(idle);
        var newTableObject = document.getElementById('bosstable');
        sorttable.makeSortable(newTableObject);
        //console.log(bosslist);
        setTimeout(function(){
            $("#hdistance").trigger("click");
            setTimeout(function(){$("#hdistance").trigger("click");},100);
        },100);
        for (var i in bosslist.x) {
            $("#cl"+bosslist.cid[i]).click(function() {
                $(this).css("color","red");
                setTimeout(function(){
                    $("#raidDungGo").trigger("click");
                },500);
            });
        }
    }

    var sum=true;
    //fill queue && convert && summary buttons
    $(document).ready(function() {
        var fillbut='<button id="fillque" class="greenb tooltipstered" style="height:18px; width:40px; margin-left:7px; margin-top:5px ; border-radius:4px ; font-size: 10px !important; padding: 0px;">Fill</button>';
        $('#sortbut').after(fillbut);
        $('#fillque').click(function() {
            var cid=poll2.city.cid;
            event.stopPropagation();
            var bB = $.post('/overview/fillq.php', { a: cid });
        });
         var convbut='<button id="convque" class="greenb tooltipstered" style="height:18px; width:60px; margin-left:7px; margin-top:5px ; border-radius:4px ; font-size: 10px !important; padding: 0px;">Convert</button>';
        $('#sortbut').after(convbut);
        $('#convque').click(function() {
            var cfd=poll2.city.cid;
            event.stopPropagation();
            var cB = $.post('/overview/mconv.php', { a: cfd });
        });
        var sumbut="<button class='tabButton greenb' id='Sum'>Summary</button>";
        $("#items").after(sumbut);
        $(".tabButton").width("110px");
        $("#Sum").click(function() {
            if(sum) {
                opensumwin();
            } else {
                $('#sumWin').show();
            }
        });
        $("#sumWin").click(function() {
            //console.log("popsum");
        });
        var suposum='<button id="suposum" class="greenb tooltipstered" style="height:18px; width:60px; margin-left:7px; margin-top:5px ; border-radius:4px ; font-size: 10px !important; padding: 0px;">Summary</button>';
        $("#reinfqbar").append(suposum);
        $("#suposum").click(function() {
            //$("#reinfqbar").trigger({type:"click",originalEvent:"1"});
            $("#reinfqbar").trigger({type:"click",originalEvent:"1"});

            opensupsum();
        });
    });

    // supporting armies summary
    function opensupsum() {
        var supsum="<div id='supsumWin' style='width:40%;height:70%;left:30%' class='popUpBox'><div class='popUpBar'><span class='ppspan'>Supporting armies Summary</span>";
        supsum+="<button id='supsumX' onclick=\"$('#supsumWin').remove();\" class='xbutton greenb'><div id='xbuttondiv'><div><div id='centxbuttondiv'></div></div></div></button></div><div id=supsumbody' class='popUpWindow'>";
        supsum+="<button class='greenb' style='font-size:14px;border-radius:6px; margin:4px;'><div class='button'><a href='#' id ='supsumexp' role='button' style='color:white;'>Export</a></div></button>";
        supsum+="<div class='beigemenutable scroll-pane' style='width:99%;height:90%;margin-left:4px;'><table id='supsumTab'><thead><th>City</th><th>Player</th><th colspan='2'>Troop type/Arriving time</th><th>Total TS</th><th>Cancel</th></thead><tbody>";
        $.each(poll2.city.trintr, function(key, value) {
            if ($.isNumeric(key)) {
                if ('tr' in value) {
                    var pn=pldata[this.p];
                    var cid=this.c;
                    var tempx;
                    var tempy;
                    var tempts=0;
                    tempx=Number(cid % 65536);
                    tempy=Number((cid-tempx)/65536);
                    supsum+="<tr><td class='coordblink shcitt' data='"+cid+"'>"+tempx+":"+tempy+"</td><td class='playerblink'>"+pn+"</td><td colspan='2'>";
                    $.each(this.tr, function() {
                        supsum+=this.tv+" "+ttname[this.tt]+",";
                        tempts+=this.tv*ttts[this.tt];
                    });
                    supsum+="</td><td>"+tempts+"</td><td><button class='greenb cansup' data='"+this.o+"' style='border-radius:6px;'>Recall</button></td></tr>";
                }
            }
        });

        $.each(poll2.OGR, function() {
            var cid=this[6]*65536+this[5];
            if (this[1]=="Incoming Reinforcement") {
                supsum+="<tr><td class='coordblink shcitt' data='"+cid+"'>"+this[5]+":"+this[6]+"</td><td class='playerblink'>"+this[7]+"</td><td colspan='2'>"+this[9].substring(38,60)+"<td>"+this[3]+"</td><td></td></tr>";
            }
        });
        supsum+="</tbody></table></div></div>";
        $("body").append(supsum);
        $( "#supsumWin" ).draggable({ handle: ".popUpBar" , containment: "window", scroll: false});
        $( "#supsumWin" ).resizable();
        //$( "#supsumTab" ).DataTable();
        $('#supsumTab').css("text-align","center");
        var newTableObject = document.getElementById('supsumTab');
        sorttable.makeSortable(newTableObject);
        $("#supsumexp").click(function(event) {
            //var outputFile = window.prompt("What do you want to name your output file (Note: This won't have any effect on Safari)") || 'export';
            var outputFile = 'SupportSum'+today.getDate()+Number(today.getMonth()+1)+today.getFullYear()+'.csv';

            // CSV
            exportTableToCSV.apply(this, [$('#supsumTab'), outputFile]);
        });
        $(".cansup").click(function() {
            var id=$(this).attr("data");
            var dat={a: id};
            jQuery.ajax({url: 'overview/reinreca.php',type: 'POST',aysnc:false, data: dat});
            $(this).remove();
        });
    };
    //summary window
    function opensumwin() {
        sum=false;
        //console.log(1);
        var sumwin="<div id='sumWin' style='width:60%;height:50%;left: 360px; z-index: 2000;' class='popUpBox'><div id='popsum' class='popUpBar'><span class=\"ppspan\">Cities Summaries</span> <button id=\"sumX\" onclick=\"$('#sumWin').hide();\" class=\"xbutton greenb\"><div id=\"xbuttondiv\"><div><div id=\"centxbuttondiv\"></div></div></div></button></div><div class=\"popUpWindow\" style='height:100%'>";
        sumwin+="<div id='sumdiv' class='beigetabspopup' style='background:none;border: none;padding: 0px;height:74%;'><ul id='sumtabs' role='tablist'><li role='tab'><a href='#resTab' role='presentation'>Resources</a></li>";
        sumwin+="<li role='tab'><a href='#troopsTab' role='presentation'>Troops</a></li><li role='tab'><a href='#raidTab' role='presentation'>Raids</a></li><li role='tab'><a href='#raidoverTab' role='presentation'>Raids Overview</a></li>";
        sumwin+="<li role='tab'><a href='#supportTab' role='presentation'>Support</a></li><li role='tab'><a href='#incomingTab' role='presentation'>Incoming</a></li></ul>";
        sumwin+="<div id='resTab'><button id='resup' class='greenb' style='font-size:14px;border-radius:6px; margin:4px;'>Update</button><button class='greenb' style='font-size:14px;border-radius:6px; margin:4px;'><div class='button'><a href='#' id ='resexp' role='button' style='color:white;'>Export</a></div></button><span id='respan' style='margin-left:50px;'>Show cities from: </span>";
        sumwin+="<div class='beigemenutable scroll-pane' style='width:99%;height:100%;margin-left:4px;' ><table id='restable'>";
        sumwin+="<thead><th>Name</th><th colspan='2'>Notes</th><th>Coords</th><th>Wood</th><th>(Storage)</th><th>Stones</th><th>(Storage)</th><th>Iron</th><th>(Storage)</th><th>Food</th><th>(Storage)</th><th>Carts</th><th>(Total)</th><th>Ships</th><th>(Total)</th><th>Score</th></thead></table></div></div>";
        sumwin+="<div id='troopsTab'><button id='troopsup' class='greenb' style='font-size:14px;border-radius:6px;margin:4px;'>Update</button><button class='greenb' style='font-size:14px;border-radius:6px; margin:4px;'><div class='button'><a href='#' id ='troopsexp' role='button' style='color:white;'>Export</a></div></button>";
        sumwin+="<button id='hidespfbut' class='greenb' style='font-size:14px;border-radius:6px;margin:4px;'>Hide Specific Troops Columns</button><span id='troopspan' style='margin-left:50px;'>Show cities from: </span>";
        sumwin+="<div  class='beigemenutable scroll-pane' style='width:99%;height:95%;margin-left:4px;'><table id='troopstable' style='width:250%'>";
        sumwin+="<thead><tr data='0'><th>Name</th><th style='width:150px;'>Notes</th><th>Coords</th><th class='spf'><div class='"+tpicdiv[8]+"'></div>(home)</th><th class='spf'>(Total)</th><th class='spf'><div class='"+tpicdiv[1]+"'></div>(home)</th><th class='spf'>(Total)</th><th class='spf'><div class='"+tpicdiv[11]+"'></div>(home)</th><th class='spf'>(Total)</th>";
        sumwin+="<th class='spf'><div class='"+tpicdiv[14]+"'></div>(home)</th><th class='spf'>(Total)</th><th class='spf'><div class='"+tpicdiv[0]+"'></div>(home)</th><th class='spf'>(Total)</th><th class='spf'><div class='"+tpicdiv[10]+"'></div>(home)</th><th class='spf'>(Total)</th><th class='spf'><div class='"+tpicdiv[9]+"'></div>(home)</th><th class='spf'>(Total)</th><th class='spf'><div class='"+tpicdiv[4]+"'></div>(home)</th><th class='spf'>(Total)</th><th class='spf'><div class='"+tpicdiv[12]+"'></div>(home)</th><th class='spf'>(Total)</th>";
        sumwin+="<th class='spf'><div class='"+tpicdiv[2]+"'></div>(home)</th><th class='spf'>(Total)</th><th class='spf'><div class='"+tpicdiv[13]+"'></div>(home)</th><th class='spf'>(Total)</th><th class='spf'><div class='"+tpicdiv[7]+"'></div>(home)</th><th class='spf'>(Total)</th><th class='spf'><div class='"+tpicdiv[17]+"'></div>(home)</th><th class='spf'>(Total)</th><th class='spf'><div class='"+tpicdiv[6]+"'></div>(home)</th><th class='spf'>(Total)</th><th class='spf'><div class='"+tpicdiv[15]+"'></div>(home)</th><th class='spf'>(Total)</th>";
        sumwin+="<th class='spf'><div class='"+tpicdiv[3]+"'></div>(home)</th><th class='spf'>(Total)</th><th class='spf'><div class='"+tpicdiv[5]+"'></div>(home)</th><th class='spf'>(Total)</th><th class='spf'><div class='"+tpicdiv[16]+"'></div>(home)</th><th class='spf'>(Total)</th><th>TS(home)</th><th>(Total)</th>";
        sumwin+="</tr></thead></table></div></div>";
        sumwin+="<div id='raidTab'><button id='raidup' class='greenb' style='font-size:14px;border-radius:6px; margin:4px;'>Update</button><span style='margin-left:50px;'>Number of reports to show: </span><select id='raidsturnc' class='greensel' style='border-radius:4px;'><option value='100'>100</option><option value='500'>500</option><option value='1000'>1000</option><option value='10000'>10000</option></select>";
        sumwin+="<div class='beigemenutable scroll-pane' style='width:99%;height:110%;margin-left:4px;' ><table id='raidtable'>";
        sumwin+="<thead><th>Report</th><th>Type</th><th>Cavern progress</th><th>losses</th><th>Carry</th><th>Date</th><th>Origin</th></thead></table></div></div>";
        sumwin+="<div id='raidoverTab'><button id='raidoverup' class='greenb' style='font-size:14px;border-radius:6px; margin:4px;'>Update</button><button class='greenb' style='font-size:14px;border-radius:6px; margin:4px;'><div class='button'><a href='#' id ='raidexp' role='button' style='color:white;'>Export</a></div></button><span id='raidspan' style='margin-left:50px;'>Show cities from: </span>";
        sumwin+="<div class='beigemenutable scroll-pane' style='width:99%;height:100%;margin-left:4px;' ><table id='raidovertable'>";
        sumwin+="<thead><th></th><th>Name</th><th colspan='2'>Notes</th><th>Coords</th><th>Raids</th><th>Out</th><th>In</th><th>Raiding TS</th><th>Resources</th></thead></table></div></div>";
        sumwin+="<div id='supportTab'><button id='supportup' class='greenb' style='font-size:14px;border-radius:6px; margin:4px;'>Update</button>";
        sumwin+="<div class='beigemenutable scroll-pane' style='width:99%;height:110%;margin-left:4px;' ><table id='supporttable'>";
        sumwin+="<thead><th></th><th>Player</th><th>City</th><th>Coords</th><th>Alliance</th><th>TS supporting</th><th>TS sending</th><th>TS returning</th></thead></table></div></div>";
        sumwin+="<div id='incomingTab'><button id='incomingup' class='greenb' style='font-size:14px;border-radius:6px; margin:4px;'>Update</button><span id='incomingspan' style='margin-left:50px;'>Show cities from: </span><span style='margin-left: 20px;color: green;'>Supporting</span><span>/</span><span style='color: #949400;'>Incoming before attack</span><span>/</span><span style='color: red;'>Incoming after attack</span>";
        sumwin+="<div class='beigemenutable scroll-pane' style='width:99%;height:110%;margin-left:4px;' ><table id='incomingtable' style='width:150%'>";
        sumwin+="<thead><th>Player</th><th>City</th><th>Coords</th><th># of attacks</th><th><div class='"+tpicdiv[2]+"'</div></th><th><div class='"+tpicdiv[3]+"'</div>/th><th><div class='"+tpicdiv[4]+"'</div></th><th><div class='"+tpicdiv[7]+"'</div></th><th><div class='"+tpicdiv[8]+"'</div></th><th><div class='"+tpicdiv[9]+"'</div></th><th><div class='"+tpicdiv[15]+"'</div></th><th><div class='"+tpicdiv[14]+"'</div></th><th><div class='"+tpicdiv[1]+"'</div></th><th>other</th><th colspan='2'>TS total</th><th>Next attack</th></thead></table></div></div>";
        sumwin+="</div></div>";
        $("#reportsViewBox").after(sumwin);
        $( "#sumWin" ).draggable({ handle: ".popUpBar" , containment: "window", scroll: false});
        $( "#sumWin" ).resizable();
        $(".popUpBar").click(function() {
            //console.log("popup");
            if ($(this).parent().attr("id")=="sumWin") {
                setTimeout(function() {
                    $("#sumWin").css("z-index",4001);
                },200);
            } else {
                setTimeout(function() {
                    $("#sumWin").css("z-index",3000);
                },200);
            }
        });
        $( "#sumdiv" ).tabs();
        var selres=$("#organiser").clone(false).attr({id:"selres",style:"height: 30px;width:150px;font-size:14px;border-radius:6px;margin:7px"});
        var seltroops=$("#organiser").clone(false).attr({id:"seltroops",style:"height: 30px;width:150px;font-size:14px;border-radius:6px;margin:7px"});
        var selraids=$("#organiser").clone(false).attr({id:"selraid",style:"height: 30px;width:150px;font-size:14px;border-radius:6px;margin:7px"});
        var selcres=$("#idleCsel").clone(false).attr({id:"selcres",style:"width:150px;height:30px;font-size:14px;border-radius:6px;margin:7px"});
        var selcraid=$("#idleCsel").clone(false).attr({id:"selcraid",style:"width:150px;height:30px;font-size:14px;border-radius:6px;margin:7px"});
        var selctroops=$("#idleCsel").clone(false).attr({id:"selctroops",style:"width:150px;height:30px;font-size:14px;border-radius:6px;margin:7px"});
        var selcinc=$("#idleCsel").clone(false).attr({id:"selcincoming",style:"width:150px;height:30px;font-size:14px;border-radius:6px;margin:7px"});
        $("#respan").after(selres);
        $("#troopspan").after(seltroops);
        $("#raidspan").after(selraids);
        $("#incomingspan").after(selcinc);
        $("#selres").after(selcres);
        $("#seltroops").after(selctroops);
        $("#selraid").after(selcraid);
        $("#seltroops").val("all").change();
        $("#selres").val("all").change();
        $("#selraid").val("all").change();
        $("#selcres").val(56).change();
        $("#selcraid").val(56).change();
        $("#selctroops").val(56).change();
        $("#selcincoming").val(56).change();
        //tabs.tabs( "refresh" );
        $("#hidespfbut").click(function() {
            if (hidespf) {
                hidespf=false;
                $(".spf").show();
                $(".nspf").hide();
                $("#troopstable").css("width","250%");
                $(this).html("Hide Specific Troops Columns");
            } else {
                hidespf=true;
                $(".spf").hide();
                $(".nspf").show();
                $("#troopstable").css("width","100%");
                $(this).html("Show Specific Troops Columns");
            }
        });
        $("#resup").click(function() {
            $("#selres").val("all").change();
            jQuery.ajax({url: 'overview/citover.php',type: 'POST',aysnc:false,
                         success: function(data) {
                             var sumres=JSON.parse(data);
                             updateres(sumres);
                         }
                        });
        });
        $("#troopsup").click(function() {
            $("#seltroops").val("all").change();
            var notes={id:[],notes:[]};
            jQuery.ajax({url: 'overview/citover.php',type: 'POST',aysnc:false,
                         success: function(data) {
                             var sumres=JSON.parse(data);
                             $.each(sumres, function() {
                                 notes.id.push(this.id);
                                 notes.notes.push(this.reference);
                             });
                             jQuery.ajax({url: 'overview/trpover.php',type: 'POST',aysnc:false,
                                          success: function(data) {
                                              var troopsres=JSON.parse(data);
                                              updatetroops(troopsres,notes);
                                          }
                                         });
                         }
                        });
        });
        $("#raidup").click(function() {
            jQuery.ajax({url: 'overview/rreps.php',type: 'POST',aysnc:false,
                         success: function(data) {
                             var raids=JSON.parse(data);
                             updateraids(raids,$("#raidsturnc").val());
                         }
                        });
        });
        $("#raidoverup").click(function() {
            $("#selraid").val("all").change();
            var notes={id:[],notes:[]};
            jQuery.ajax({url: 'overview/citover.php',type: 'POST',aysnc:false,
                         success: function(data) {
                             var sumres=JSON.parse(data);
                             $.each(sumres, function() {
                                 notes.id.push(this.id);
                                 notes.notes.push(this.reference);
                             });
                             jQuery.ajax({url: 'overview/graid.php',type: 'POST',aysnc:false,
                                          success: function(data) {
                                              var raids=JSON.parse(data);
                                              updateraidover(raids,notes);
                                          }
                                         });
                         }
                        });
        });
        $("#supportup").click(function() {
            jQuery.ajax({url: 'overview/reinover.php',type: 'POST',aysnc:false,
                         success: function(data) {
                             var support=JSON.parse(data);
                             updatesupport(support);
                         }
                        });
        });
        $("#incomingup").click(function() {
            jQuery.ajax({url: 'overview/incover.php',type: 'POST',aysnc:false,
                         success: function(data) {
                             var incoming=JSON.parse(data);
                             updateincoming(incoming);
                         }
                        });
        });
        var citylist=[];
        $("#seltroops").change(function() {
            filtertroops("troops");
        });
        $("#selctroops").change(function() {
            filtertroops("troops");
        });
        $("#selcincoming").change(function() {
            if ($("#selcincoming").val()=="56") {
                $("#incomingtable tr").each(function() {
                    $(this).show();
                });
            } else {
                $("#incomingtable tr").each(function() {
                    if ($($(this).children()[0]).is( "td" )) {
                        if ($(this).attr("cont")==$("#selcincoming").val()) {
                            $(this).show();
                        } else {
                            $(this).hide();
                        }
                    }
                });
            }
        });
        function filtertroops(type) {
            var clist=$("#sel"+type).val();
            var con=Number($("#selc"+type).val());
            var clistbool;
            var contbool;
            if (clist!="all") {
                //console.log("1");
                $.each(poll2.player.clc, function(key, value) {
                    if (key==clist) {
                        citylist=value;
                    }
                });
            }
            if (type=="raid") {
                type+="over";   
            }
            $("#"+type+"table tr").each(function() {
                if ($(this).attr("class")!="nofilter") {
                    if ($($(this).children()[0]).is( "td" )) {
                        if (con==56) {
                            contbool=true;
                        } else {
                            if (con==Number($(this).attr("cont"))) {
                                contbool=true;
                            } else {
                                contbool=false;
                            }
                        }
                        if ( clist=="all") {
                            clistbool=true;
                        } else {
                            if (citylist.indexOf(Number($(this).attr("data")))>-1) {
                                clistbool=true;
                            } else {
                                clistbool=false;
                            }
                        }
                        //console.log(clistbool,contbool);
                        if (clistbool && contbool) {
                            //console.log("show");
                            $(this).show();
                        } else {
                            //console.log("hide");
                            $(this).hide();
                        }
                    }
                }
            });
        }
        $("#selres").change(function() {
            filtertroops("res");
        });
        $("#selcres").change(function() {
            filtertroops("res");
        });
        $("#selraid").change(function() {
            filtertroops("raid");
        });
        $("#selcraid").change(function() {
            filtertroops("raid");
        });
    }
    //update incomings summary
    function updateincoming(data) {
        var inctab="<thead><th>Player</th><th>City</th><th>Coords</th><th># of attacks</th><th><div class='"+tpicdiv[2]+"'</div></th><th><div class='"+tpicdiv[3]+"'</div>/th><th><div class='"+tpicdiv[4]+"'</div></th><th><div class='"+tpicdiv[7]+"'</div></th><th><div class='"+tpicdiv[8]+"'</div></th><th><div class='"+tpicdiv[9]+"'</div></th><th><div class='"+tpicdiv[15]+"'</div></th><th><div class='"+tpicdiv[14]+"'</div></th><th><div class='"+tpicdiv[1]+"'</div></th><th>other</th><th colspan='2'>TS total</th><th>Next attack</th></thead><tbody>";
        var i=0;
        var ttd=[2,3,4,7,8,9,15,14,1];
        $.each(data.a, function(key,value) {
            var reinf=[0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0];
            var inc=[0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0];
            var incl=[0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0];
            var tsreinf=0;
            var tsinc=0;
            var tsincl=0;
            var tempx=Number(key%65536);
            var tempy=Number((key-tempx)/65536);
            var cont=Number(Math.floor(tempx/100)+10*Math.floor(tempy/100));
            var atime;
            inctab+="<tr cont='"+cont+"'><td class='playerblink'>"+this[0]+"</td><td>"+this[1]+"</td><td class='coordblink shcitt' data='"+key+"'>"+tempx+":"+tempy+"</td><td>"+this[3]+"</td>";
            var att=false;
            for (var i in this[9]) {
                if (this[9][i][5]=="3") {
                    if (this[9][i][7]=="on support" || this[9][i][7]=="home") {
                        if (this[9][i][3].length>0) {
                            for (var j in this[9][i][3]) {
                                var tmp=this[9][i][3][j].split(" ");
                                var tt=ttname.indexOf(tmp[1]);
                                var tts=Number(tmp[0].replace(/,/g,""));
                                if (ttd.indexOf(tt)==-1) {
                                    reinf[0]+=tts;
                                } else {
                                    reinf[tt]+=tts;
                                }
                                tsreinf+=ttts[tt]*tts;
                            }
                        }
                    } else {
                        for (var j in this[9][i][3]) {
                            var tmp=this[9][i][3][j].split(" ");
                            var tt=ttname.indexOf(tmp[1]);
                            var tts=Number(tmp[0].replace(/,/g,""));
                            if (att) {
                                if (ttd.indexOf(tt)==-1) {
                                    incl[tt]+=tts;
                                } else {
                                    incl[0]+=tts;
                                }
                                tsincl+=ttts[tt]*tts;
                            } else {
                                if (ttd.indexOf(tt)==-1) {
                                    inc[tt]+=tts;
                                } else {
                                    inc[0]+=tts;
                                }
                                tsinc+=ttts[tt]*tts;
                            }
                        }
                    }
                }
                if ((this[9][i][5]=="0" || this[9][i][5]=="1") && att==false) {
                    att=true;
                    atime=this[9][i][7];
                }
            }
            for (var i in ttd) {
                inctab+="<td><span style='color:green;'>"+reinf[ttd[i]].toLocaleString()+"</span>/<span style='color:#949400;'>"+inc[ttd[i]].toLocaleString()+"</span>/<span style='color:red;'>"+incl[ttd[i]].toLocaleString()+"</span></td>";
            }
            inctab+="<td><span style='color:green;'>"+reinf[0].toLocaleString()+"</span>/<span style='color:#949400;'>"+inc[0].toLocaleString()+"</span>/<span style='color:red;'>"+incl[0].toLocaleString()+"</span></td>";
            inctab+="<td colspan='2'><span style='color:green;'>"+tsreinf.toLocaleString()+"</span>/<span style='color:#949400;'>"+tsinc.toLocaleString()+"</span>/<span style='color:red;'>"+tsincl.toLocaleString()+"</span></td><td>"+atime+"</td>";
            inctab+="</tr>";
        });
        inctab+="</tbody>";
        $("#incomingtable").html(inctab);
        $("#incomingtable td").css("text-align","center");
        var newTableObject = document.getElementById('incomingtable');
        sorttable.makeSortable(newTableObject);
    }
    //update raid overview
    function updateraidover(data,notes) {
       // console.log(notes);
        var raidovertab="<thead><tr data='0'><th></th><th>Name</th><th colspan='2'>Notes</th><th>Coords</th><th>Raids</th><th>Out</th><th>In</th><th>Raiding TS</th><th>Resources</th></tr></thead><tbody>";
        $.each(data.a, function() {
            var cid=this[0];
            var not=notes.notes[notes.id.indexOf(cid)];
            var x=Number(cid%65536);
            var y=Number((cid-x)/65536);
            var con=Number(Math.floor(x/100)+10*Math.floor(y/100));
            raidovertab+="<tr data='"+cid+"' cont='"+con+"'><td><button style='height: 20px;padding-top: 3px;border-radius:6px;' class='greenb recraid' data='"+cid+"'>Recall Raids</button></td>";
            raidovertab+="<td data='"+cid+"' class='coordblink raidclink'>"+this[1]+"</td><td colspan='2'>"+not+"</td><td class='coordblink shcitt' data='"+cid+"'>"+x+":"+y+"</td><td>"+this[3]+"</td><td>"+this[6]+"</td><td>"+this[5]+"</td><td>"+this[4].toLocaleString()+"</td>";
            raidovertab+="<td>"+(this[7]+this[8]+this[9]+this[10]+this[11]).toLocaleString()+"</td></tr>";
        });
        raidovertab+="</tbody>";
        $("#raidovertable").html(raidovertab);
        $("#raidovertable td").css("text-align","center");
        var newTableObject = document.getElementById('raidovertable');
        sorttable.makeSortable(newTableObject);
        $(".raidclink").click(function() {
            var aa=$(this).attr("data");
            $("#organiser").val("all").change();
            $("#cityDropdownMenu").val(aa).change();
        });
        $(".recraid").click(function() {
            var id=$(this).attr("data");
            var dat={a: id};
            jQuery.ajax({url: 'overview/rcallall.php',type: 'POST',aysnc:false, data: dat});
            $(this).remove();
        });
        $("#raidexp").click(function(event) {
            //var outputFile = window.prompt("What do you want to name your output file (Note: This won't have any effect on Safari)") || 'export';
            var outputFile = 'RaidSum'+today.getDate()+Number(today.getMonth()+1)+today.getFullYear()+'.csv';

            // CSV
            exportTableToCSV.apply(this, [$('#raidovertable'), outputFile]);
        });
    }
    //update support summary
    function updatesupport(data) {
        var supporttab="<thead><th></th><th>Player</th><th>City</th><th>Coords</th><th>Alliance</th><th>TS supporting</th><th>TS sending</th><th>TS returning</th></thead><tbody>";
        $.each(data, function() {
            var tid=this[9][0][1];
            supporttab+="<tr><td><button class='greenb expsup' style='height: 20px;padding-top: 3px;border-radius:6px;'>Expand</button><button data='"+tid+"' class='greenb recasup' style='height: 20px;padding-top: 3px;border-radius:6px;'>Recall all</button>";
            supporttab+="</td><td class='playerblink'>"+this[0]+"</td><td>"+this[2]+"</td><td class='coordblink shcitt' data='"+tid+"'>"+this[3]+":"+this[4]+"</td><td class='allyblink'>"+this[1]+"</td><td>"+this[6]+"</td><td>"+this[7]+"</td><td>"+this[8]+"</td></tr>";
            supporttab+="<tr class='expsuptab'><td colspan='8'><div class='beigemenutable' style='width:98%;'><table><thead><th></th><th>City</th><th>Coords</th><th colspan='2'>Troops</th><th>Status</th><th>Arrival</th></thead><tbody>";
            for (var i in this[9]) {
                var sid=this[9][i][15];
                var status;
                var id=this[9][i][10];
                switch (this[9][i][0]) {
                    case 1:
                        supporttab+="<tr style='color: purple;'><td></td>";
                        status="Sending";
                        break;
                    case 2:
                        supporttab+="<tr style='color: green;'><td><button class='greenb recsup' data='"+id+"' style='height: 20px;padding-top: 3px;border-radius:6px;'>Recall</button></td>";
                        status="Reinforcing";
                        break;
                    case 0:
                        supporttab+="<tr style='color: #00858E;'><td></td>";
                        status="returning";
                        break;
                }
                supporttab+="<td data='"+sid+"' class='coordblink suplink'>"+this[9][i][11]+"</td><td class='coordblink shcitt' data='"+sid+"'>"+this[9][i][12]+":"+this[9][i][13]+"</td>";
                supporttab+="<td colspan='2'>";
                for (var j in this[9][i][8]) {
                    supporttab+=this[9][i][8][j]+",";
                }
                supporttab+="</td><td>"+status+"</td><td>"+this[9][i][9]+"</td></tr>";
            }
            supporttab+="</tbody></table></div></td></tr><tr class='usles'></tr>";
        });
        $("#supporttable").html(supporttab);
        $("#supporttable td").css("text-align","center");
        $(".expsuptab").toggle();
        $(".usles").hide();
        var newTableObject = document.getElementById('supporttable');
        sorttable.makeSortable(newTableObject);
        $(".suplink").click(function() {
            var cid=$(this).attr("data");
            $("#organiser").val("all").change();
            $("#cityDropdownMenu").val(cid).change();
        });
        $(".recsup").click(function() {
            var id=$(this).attr("data");
            var dat={a: id};
            jQuery.ajax({url: 'overview/reinreca.php',type: 'POST',aysnc:false, data: dat});
            $(this).remove();
        });
        $(".expsup").click(function() {
            $(this).parent().parent().next().toggle();
        });
        $(".recasup").click(function() {
            var id=$(this).attr("data");
            var dat={a: id};
            jQuery.ajax({url: 'overview/reinrecall.php',type: 'POST',aysnc:false, data: dat});
            $(this).remove();
        });
        $("#supportexp").click(function(event) {
            //var outputFile = window.prompt("What do you want to name your output file (Note: This won't have any effect on Safari)") || 'export';
            var outputFile = 'SupportSum'+today.getDate()+Number(today.getMonth()+1)+today.getFullYear()+'.csv';

            // CSV
            exportTableToCSV.apply(this, [$('#supporttable'), outputFile]);
        });
    }
    //update raids summary
    function updateraids(data,turnc) {
        var raidtab="<thead><th>Report</th><th>Type</th><th>Cavern progress</th><th>losses</th><th>Carry</th><th>Date</th><th>Origin</th></thead><tbody>";
        var i=0;
        $.each(data.b, function() {
            if (i<turnc) {
                if (this[2]<=2) {
                    raidtab+="<tr style='color:green;'>";
                }
                else if (2<this[2] && this[2]<=5) {
                    raidtab+="<tr style='color:#CF6A00;'>";
                }
                else if (this[2]>5 ) {
                    raidtab+="<tr style='color:red;'>";
                }
                raidtab+="<td class='gFrep' data='"+this[6]+"'><span class='unread'>Share report</td><td>"+this[0]+"</span></td><td>"+this[8]+"%"+"</td><td>"+this[2]+"%"+"</td><td>"+this[3]+"%"+"</td><td>"+this[4]+"</td><td>"+this[1]+"</td></tr>";
            }
            i++;
        });
        raidtab+="</tbody>";
        $("#raidtable").html(raidtab);
        $("#raidtable td").css("text-align","center");
        var newTableObject = document.getElementById('raidtable');
        sorttable.makeSortable(newTableObject);
    }
    //update res summary
    function updateres(data) {
        var restabb="<thead><tr data='0'><th>Name</th><th colspan='2'>Notes</th><th>Coords</th><th>Wood</th><th>(Storage)</th><th>Stones</th><th>(Storage)</th><th>Iron</th><th>(Storage)</th><th>Food</th><th>(Storage)</th><th>Carts</th><th>(Total)</th><th>Ships</th><th>(Total)</th><th>Score</th></tr></thead><tbody>";
        var woodtot=0;
        var irontot=0;
        var stonetot=0;
        var foodtot=0;
        var cartstot=0;
        var shipstot=0;
        $.each(data, function() {
            var cid=this.id;
            var x=Number(cid%65536);
            var y=Number((cid-x)/65536);
            var con=Number(Math.floor(x/100)+10*Math.floor(y/100));
            restabb+="<tr data='"+cid+"' cont='"+con+"'><td id='cn"+cid+"' class='coordblink'>"+this.city+"</td><td colspan='2'>"+this.reference+"</td><td class='coordblink shcitt' data='"+cid+"'>"+x+":"+y+"</td>";
            var res;
            var sto;
            cartstot+=this.carts_total;
            shipstot+=this.ships_total;
            for (var i=0; i<4; i++)
            {
                switch(i) {
                    case 0:
                        res=this.wood;
                        woodtot+=res;
                        sto=this.wood_storage;
                        break;
                    case 1:
                        res=this.stone;
                        stonetot+=res;
                        sto=this.stone_storage;
                        break;
                    case 2:
                        res=this.iron;
                        irontot+=res;
                        sto=this.iron_storage;
                        break;
                    case 3:
                        res=this.food;
                        foodtot+=res;
                        sto=this.food_storage;
                        break;
                }
                if (res/sto<0.9) {
                    restabb+="<td style='color:green;'>"+res.toLocaleString()+"</td><td>"+sto.toLocaleString()+"</td>";
                } else if ((res/sto<1) && (res/sto>=0.9)) {
                    restabb+="<td style='color:#CF6A00;'>"+res.toLocaleString()+"</td><td>"+sto.toLocaleString()+"</td>";
                } else if (res==sto) {
                    restabb+="<td style='color:red;'>"+res.toLocaleString()+"</td><td>"+sto.toLocaleString()+"</td>";
                }
            }
            restabb+="<td>"+this.carts_home.toLocaleString()+"</td><td>"+this.carts_total.toLocaleString()+"</td><td>"+this.ships_home+"</td><td>"+this.ships_total+"</td><td>"+this.score.toLocaleString()+"</td></tr>";
        });
        restabb+="</tbody>";
        $("#restable").html(restabb);
        $("#restable td").css("text-align","center");
        //$("#restable").fixedHeaderTable({ cloneHeadToFoot: true });
        var newTableObject = document.getElementById('restable');
        sorttable.makeSortable(newTableObject);
        var tottab="<div id='rsum' class='beigemenutable scroll-pane' style='width: 99%;margin-left: 4px;'><table><td>Total wood: </td><td>"+woodtot.toLocaleString()+"</td><td>Total stones: </td><td>"+stonetot.toLocaleString()+"</td><td>Total iron: </td><td>"+irontot.toLocaleString()+"</td><td>Total food: </td><td>"+foodtot.toLocaleString()+"</td>";
        tottab+="<td>Total carts: </td><td>"+cartstot.toLocaleString()+"</td><td>Total ships: </td><td>"+shipstot.toLocaleString()+"</td></table></div>";
        $("#rsum").remove();
        $("#resTab").append(tottab);
        $("#rsum td").css("text-align","center");
        $.each(data, function() {
            var aa=this.id;
            $("#cn"+aa).click(function() {
                $("#organiser").val("all").change();
                $("#cityDropdownMenu").val(aa).change();
            });
        });
        $("#resexp").click(function(event) {
            //var outputFile = window.prompt("What do you want to name your output file (Note: This won't have any effect on Safari)") || 'export';
            var outputFile = 'ResSum'+today.getDate()+Number(today.getMonth()+1)+today.getFullYear()+'.csv';

            // CSV
            exportTableToCSV.apply(this, [$('#restable'), outputFile]);
        });
    }
    //update troops summary
    function updatetroops(data,notes) {
        var troopstab="<thead><tr data='0'><th>Name</th><th style='width:150px;'>Notes</th><th>Coords</th><th class='spf'><div class='"+tpicdiv[8]+"'></div>(home)</th><th class='spf'>(Total)</th><th class='spf'><div class='"+tpicdiv[1]+"'></div>(home)</th><th class='spf'>(Total)</th><th class='spf'><div class='"+tpicdiv[11]+"'></div>(home)</th><th class='spf'>(Total)</th>";
        troopstab+="<th class='spf'><div class='"+tpicdiv[14]+"'></div>(home)</th><th class='spf'>(Total)</th><th class='spf'><div class='"+tpicdiv[0]+"'></div>(home)</th><th class='spf'>(Total)</th><th class='spf'><div class='"+tpicdiv[10]+"'></div>(home)</th><th class='spf'>(Total)</th><th class='spf'><div class='"+tpicdiv[9]+"'></div>(home)</th><th class='spf'>(Total)</th><th class='spf'><div class='"+tpicdiv[4]+"'></div>(home)</th><th class='spf'>(Total)</th><th class='spf'><div class='"+tpicdiv[12]+"'></div>(home)</th><th class='spf'>(Total)</th>";
        troopstab+="<th class='spf'><div class='"+tpicdiv[2]+"'></div>(home)</th><th class='spf'>(Total)</th><th class='spf'><div class='"+tpicdiv[13]+"'></div>(home)</th><th class='spf'>(Total)</th><th class='spf'><div class='"+tpicdiv[7]+"'></div>(home)</th><th class='spf'>(Total)</th><th class='spf'><div class='"+tpicdiv[17]+"'></div>(home)</th><th class='spf'>(Total)</th><th class='spf'><div class='"+tpicdiv[6]+"'></div>(home)</th><th class='spf'>(Total)</th><th class='spf'><div class='"+tpicdiv[15]+"'></div>(home)</th><th class='spf'>(Total)</th>";
        troopstab+="<th class='spf'><div class='"+tpicdiv[3]+"'></div>(home)</th><th class='spf'>(Total)</th><th class='spf'><div class='"+tpicdiv[5]+"'></div>(home)</th><th class='spf'>(Total)</th><th class='spf'><div class='"+tpicdiv[16]+"'></div>(home)</th><th class='spf'>(Total)</th>";
        troopstab+="<th class='nspf' style='width:350px'>Troops</th><th>TS Home</th><th>TS Total</th>";
        troopstab+="</tr></thead><tbody>";
        var arbstot=0;
        var balltot=0;
        var druidstot=0;
        var galltot=0;
        var guardstot=0;
        var horsetot=0;
        var praetorstot=0;
        var priesttot=0;
        var ramstot=0;
        var rangerstot=0;
        var scorptot=0;
        var scoutstot=0;
        var senatortot=0;
        var sorctot=0;
        var stingerstot=0;
        var triaritot=0;
        var vanqstot=0;
        var warshipstot=0;
        var tshome;
        var tstot;
        var strhome;
        var strtot;
        $.each(data, function() {
            tshome=0;
            tstot=0;
            var cid=this.id;
            var not=notes.notes[notes.id.indexOf(cid)];
            var x=Number(cid%65536);
            var y=Number((cid-x)/65536);
            var con=Number(Math.floor(x/100)+10*Math.floor(y/100));
            var strhome="<table><tr class='nofilter'>";
            var strtot="<table><tr class='nofilter'>";
            var thome;
            var ttot;
            var tt;
            troopstab+="<tr data='"+cid+"' cont='"+con+"'><td id='cnt"+cid+"' class='coordblink'>"+this.c+"</td><td style='width:150px;'>"+not+"</td><td class='coordblink shcitt' data='"+cid+"'>"+x+":"+y+"</td>";
            function makets() {
                /*if (thome>0) {
                    strhome+="<td style='width:50px;'>"+thome.toLocaleString()+"</td><td style='width:22px;'><div class='"+tpicdiv20[tt]+"'></div></td>";
                }*/
                if (ttot>0) {
                    strtot+="<td>"+thome.toLocaleString()+"/"+ttot.toLocaleString()+"</td><td style='width:22px;'><div class='"+tpicdiv20[tt]+"'></div></td>";
                }
                troopstab+="<td class='spf'>"+thome.toLocaleString()+"</td><td class='spf'>"+ttot.toLocaleString()+"</td>";
            }
            thome=this.Arbalist_home;
            ttot=this.Arbalist_total;
            tt=8;
            arbstot+=ttot;
            tshome+=2*thome;
            tstot+=2*ttot;
            makets();

            thome=this.Ballista_home;
            ttot=this.Ballista_total;
            tt=1;
            balltot+=ttot;
            tshome+=10*thome;
            tstot+=10*ttot;
            makets();

            thome=this.Druid_home;
            ttot=this.Druid_total;
            tt=11;
            druidstot+=ttot;
            tshome+=2*thome;
            tstot+=2*ttot;
            makets();

            thome=this.Galley_home;
            ttot=this.Galley_total;
            tt=14;
            galltot+=ttot;
            tshome+=100*thome;
            tstot+=100*ttot;
            makets();

            thome=this.Guard_home;
            ttot=this.Guard_total;
            tt=0;
            guardstot+=ttot;
            tshome+=thome;
            tstot+=ttot;
            makets();

            thome=this.Horseman_home;
            ttot=this.Horseman_total;
            tt=10;
            horsetot+=ttot;
            tshome+=2*thome;
            tstot+=2*ttot;
            makets();

            thome=this.Praetor_home;
            ttot=this.Praetor_total;
            tt=9;
            praetorstot+=ttot;
            tshome+=2*thome;
            tstot+=2*ttot;
            makets();

            thome=this.Priestess_home;
            ttot=this.Priestess_total;
            tt=4;
            priesttot+=ttot;
            tshome+=thome;
            tstot+=ttot;
            makets();

            thome=this.Ram_home;
            ttot=this.Ram_total;
            tt=12;
            ramstot+=ttot;
            tshome+=10*thome;
            tstot+=10*ttot;
            makets();

            thome=this.Ranger_home;
            ttot=this.Ranger_total;
            tt=2;
            rangerstot+=ttot;
            tshome+=thome;
            tstot+=ttot;
            makets();

            thome=this.Scorpion_home;
            ttot=this.Scorpion_total;
            tt=13;
            scorptot+=ttot;
            tshome+=10*thome;
            tstot+=10*ttot;
            makets();

            thome=this.Scout_home;
            ttot=this.Scout_total;
            scoutstot+=ttot;
            tt=7;
            tshome+=2*thome;
            tstot+=2*ttot;
            makets();

            thome=this.Senator_home;
            ttot=this.Senator_total;
            tt=17;
            senatortot+=ttot;
            tshome+=thome;
            tstot+=ttot;
            makets();

            thome=this.Sorcerer_home;
            ttot=this.Sorcerer_total;
            tt=6;
            sorctot+=ttot;
            tshome+=thome;
            tstot+=ttot;
            makets();

            thome=this.Stinger_home;
            ttot=this.Stinger_total;
            tt=15;
            stingerstot+=ttot;
            tshome+=100*thome;
            tstot+=100*ttot;
            makets();

            thome=this.Triari_home;
            ttot=this.Triari_total;
            tt=3;
            triaritot+=ttot;
            tshome+=thome;
            tstot+=ttot;
            makets();

            thome=this.Vanquisher_home;
            ttot=this.Vanquisher_total;
            tt=5;
            vanqstot+=ttot;
            tshome+=thome;
            tstot+=ttot;
            makets();

            thome=this.Warship_home;
            ttot=this.Warship_total;
            tt=16;
            warshipstot+=ttot;
            tshome+=400*thome;
            tstot+=400*ttot;
            makets();

            strhome+="</tr></table>";
            strtot+="</tr></table>";
            //troopstab+="<td class='nspf' colspan='2'>"+strhome+"</td><td class='nspf' colspan='2'>"+strtot+"</td><td>"+tshome.toLocaleString()+"</td><td>"+tstot.toLocaleString()+"</td></tr>";
            troopstab+="<td class='nspf'>"+strtot+"</td><td>"+tshome.toLocaleString()+"</td><td>"+tstot.toLocaleString()+"</td></tr>";
        });
        troopstab+="</tbody>";
        $("#troopstable").html(troopstab);
        if (hidespf) {
            $(".spf").hide();
            $(".nspf").show();
            $("#troopstable").css("width","100%");
        } else {
            $(".nspf").hide();
            $(".spf").show();
            $("#troopstable").css("width","250%");
        }
        $("#troopstable td").css("text-align","center");
        $("#troopstable td").css("padding-left","0%");
        var newTableObject = document.getElementById('troopstable');
        sorttable.makeSortable(newTableObject);
        var tottab="<div id='tsum' class='beigemenutable scroll-pane' style='width: 99%;margin-left: 4px;'><table style='font-size: 14px;width: 120%;'><tr><td>Total arbs: </td><td>Total balli: </td><td>Total druids: </td><td>Total galley: </td><td>Total guards: </td><td>Total horses: </td><td>Total praetor: </td><td>Total priest: </td><td>Total rams: </td><td>Total rangers: </td>";
        tottab+="<td>Total scorp: </td><td>Total scouts: </td><td>Total senator: </td><td>Total sorc: </td><td>Total stingers: </td><td>Total triari: </td><td>Total vanqs: </td><td>Total warship: </td></tr>";
        tottab+="<tr><td>"+arbstot.toLocaleString()+"</td><td>"+balltot.toLocaleString()+"</td>";
        tottab+="<td>"+druidstot.toLocaleString()+"</td>";
        tottab+="<td>"+galltot.toLocaleString()+"</td>";
        tottab+="<td>"+guardstot.toLocaleString()+"</td>";
        tottab+="<td>"+horsetot.toLocaleString()+"</td>";
        tottab+="<td>"+praetorstot.toLocaleString()+"</td>";
        tottab+="<td>"+priesttot.toLocaleString()+"</td>";
        tottab+="<td>"+ramstot.toLocaleString()+"</td>";
        tottab+="<td>"+rangerstot.toLocaleString()+"</td>";
        tottab+="<td>"+scorptot.toLocaleString()+"</td>";
        tottab+="<td>"+scoutstot.toLocaleString()+"</td>";
        tottab+="<td>"+senatortot.toLocaleString()+"</td>";
        tottab+="<td>"+sorctot.toLocaleString()+"</td>";
        tottab+="<td>"+stingerstot.toLocaleString()+"</td>";
        tottab+="<td>"+triaritot.toLocaleString()+"</td>";
        tottab+="<td>"+vanqstot.toLocaleString()+"</td>";
        tottab+="<td>"+warshipstot.toLocaleString()+"</td></tr></table></div>";
        $("#tsum").remove();
        $("#troopsTab").append(tottab);
        //$("#tsum td").css("text-align","center");
        $.each(data, function() {
            var aa=this.id;
            $("#cnt"+aa).click(function() {
                $("#organiser").val("all").change();
                $("#cityDropdownMenu").val(aa).change();
            });
        });
        $("#troopsexp").click(function(event) {
            //var outputFile = window.prompt("What do you want to name your output file (Note: This won't have any effect on Safari)") || 'export';
            var outputFile = 'TroopsSum'+today.getDate()+Number(today.getMonth()+1)+today.getFullYear()+'.csv';

            // CSV
            exportTableToCSV.apply(this, [$('#troopstable'), outputFile]);
        });
    }
    // auto demolish button
    $(document).ready(function() {
        var autodemoon=0;
        var autodemobut='<button id="autodemo" style="margin-left:2%;width:22%;border: none !important;color:white !important" class="regButton greenb">Quick Demo</button>';
        setTimeout(1000);
        $('#quickRefineGo').width('23%');
        $('#quickBuildMode').width('22%');
        $('#cityNotesGo').width('21%');
        $('#quickBuildMode').after(autodemobut);
        $('#autodemo').click(function() {
            console.log("autodemoon"+autodemoon);
            if (autodemoon==0) {
                autodemoon=1;
                $(this).removeClass('greenb');
                $(this).addClass('redb');
                //errorgo("Quick demolish on");background: 
                //$("#autodemo").removeClass('greenb').addClass('redb');
                //$("#autodemo").css('background','url(/images/rbut1.png);');
                //console.log(autodemoon);
            } else {
                autodemoon=0;
                //errorgo("Quick demolish off");
                //$("#autodemo").removeClass('redb').addClass('greenb');
                $(this).removeClass('redb');
                $(this).addClass('greenb');
                //$("#autodemo").css('background','url(/images/nrbut2.png);');
                //console.log(autodemoon);
            }           
        });

        $('#arrowNextDiv').click(function() {
            autodemoon=0;
            $("#autodemo").removeClass('couoffpos').addClass('couonpos');
        });

        $('#arrowPrevDiv').click(function() {
            autodemoon=0;
            $("#autodemo").removeClass('couoffpos').addClass('couonpos');
        });

        $('#ddctd').click(function() {
            autodemoon=0;
            $("#autodemo").removeClass('couoffpos').addClass('couonpos');
        });
        $('#quickBuildMode').click(function() {
            autodemoon=0;
            $("#autodemo").removeClass('couoffpos').addClass('couonpos');
        });


        $("#city_map").click(function() {
            //console.log(autodemoon);
            if (autodemoon==1) {
                //$('#buildingDemolishButton').trigger({type:"click",originalEvent:"1"});
                 $("#buildingDemolishButton").trigger({type:"click",originalEvent:"1"});
            }
        });
    });
    //setting nearest hub to a city
    function setnearhub() {
        var res=[0,0,0,0,1,150000,220000,150000,350000,0,0,0,0,1,0,0,0,0,0,150000,220000,150000,350000];
        var aa=poll2.city.mo;
        var hubs={cid:[],distance:[]};
        $.each(poll2.player.clc, function(key, value) {
            if (key==$("#selHub").val()) {
                hubs.cid=value;
            }
        });
        for (var i in hubs.cid) {
            var tempx=Number(hubs.cid[i] % 65536);
            var tempy=Number((hubs.cid[i]-tempx)/65536);
            hubs.distance.push(Math.sqrt((tempx-city.x)*(tempx-city.x)+(tempy-city.y)*(tempy-city.y)));
        }
        var mindist = Math.min.apply(Math, hubs.distance);
        var nearesthub=hubs.cid[hubs.distance.indexOf(mindist)];
        //aa[42]=nearesthub;
        //aa[43]=nearesthub;
        res[14]=nearesthub;
        res[15]=nearesthub;
        res[5]=$("#woodin").val();
        res[6]=$("#stonein").val();
        res[7]=$("#ironin").val();
        res[8]=$("#foodin").val();
        res[19]=$("#woodin").val();
        res[20]=$("#stonein").val();
        res[21]=$("#ironin").val();
        res[22]=$("#foodin").val();
        for (var k in res) {
            aa[28+Number(k)]=res[k];
        }
        var dat={a:JSON.stringify(aa),b:poll2.city.cid};
        //console.log(dat);
        jQuery.ajax({url: 'includes/mnio.php',type: 'POST',aysnc:false,data: dat});
    }
    // setting layouts
    $(document).ready(function() {
        $("#citynotes").draggable({ handle: ".popUpBar" , containment: "window", scroll: false});
        $('#citynotes').height('290px');
        $('#citynotes').width('495px');
        var layoutopttab="<li id='layoutopt' class='ui-state-default ui-corner-top' role='tab' tabindex='-1' aria-controls='layoutoptBody'";
        layoutopttab+="aria-labeledby='ui-id-60' aria-selected='false' aria-expanded='false'>";
        layoutopttab+="<a href='#layoutoptBody' class='ui-tabs-anchor' role='presentation' tabindex='-1' id='ui-id-60'>Layout Options</a></li>";
        var layoutoptbody="<div id='layoutoptBody' aria-labeledby='ui-id-60' class='ui-tabs-panel ui-widget-content ui-corner-bottom' ";
        layoutoptbody+=" role='tabpanel' aria-hidden='true' style='display: none;'><table><tbody><tr><td><input id='addnotes' class='clsubopti' type='checkbox'> Add Notes</td>";
        layoutoptbody+="<td><input id='addtroops' class='clsubopti' type='checkbox'> Add Troops</td></tr><tr><td><input id='addtowers' class='clsubopti' type='checkbox'> Add Towers</td><td><input id='addbuildings' class='clsubopti' type='checkbox'> Upgrade Cabins</td>";
        layoutoptbody+="<td> Cabin Lvl: <input id='cablev' type='number' style='width:22px;' value='7'></td></tr><tr><td><input id='addwalls' class='clsubopti' type='checkbox'> Add Walls</td>";
        layoutoptbody+="<td><input id='addhub' class='clsubopti' type='checkbox'> Set Nearest Hub With layout</td><td><input id='addfw' class='clsubopti' type='checkbox'> Set Food Warning <input id='foodw' type='number' style='width:22px;' value='8'></td>";
        layoutoptbody+="</tr><tr><td>Select Hubs list: </td><td id='selhublist'></td><td>";
        layoutoptbody+="<button id='nearhubAp' class='regButton greenb' style='width:130px; margin-left: 10%'>Set Nearest Hub</button></td></tr></tbody></table>";
        layoutoptbody+="<table><tbody><tr><td colspan='2'><input id='addres' class='clsubopti' type='checkbox'> Add Resources:</td><td id='buttd' colspan='2'></td></tr><tr><td>wood<input id='woodin' type='number' style='width:100px;' value='200000'></td><td>stones<input id='stonein' type='number' style='width:100px;' value='220000'></td>";
        layoutoptbody+="<td>iron<input id='ironin' type='number' style='width:100px;' value='200000'></td><td>food<input id='foodin' type='number' style='width:100px;' value='350000'></td></tr>";
        layoutoptbody+="</tbody></table></div>";
        var layoptbut="<button id='layoptBut' class='regButton greenb' style='width:150px;'>Save Settings</button>";
        var tabs = $( "#CNtabs" ).tabs();
        var ul = tabs.find( "ul" );
        $( layoutopttab ).appendTo( ul );
        tabs.tabs( "refresh" );
        $("#CNtabs").append(layoutoptbody);
        $("#buttd").append(layoptbut);
        $("#nearhubAp").click(function() {
            setnearhub();
        });
        $("#layoptBut").click(function() {
            localStorage.setItem('woodin',$("#woodin").val());
            localStorage.setItem('foodin',$("#foodin").val());
            localStorage.setItem('ironin',$("#ironin").val());
            localStorage.setItem('stonein',$("#stonein").val());
            localStorage.setItem('cablev',$("#cablev").val());
            localStorage.setItem('foodw',$("#foodw").val());
        });
        if (localStorage.getItem('foodw')) {
            $("#foodw").val(localStorage.getItem('foodw'));
        }
        if (localStorage.getItem('cablev')) {
            $("#cablev").val(localStorage.getItem('cablev'));
        }
        if (localStorage.getItem('woodin')) {
            $("#woodin").val(localStorage.getItem('woodin'));
        }
        if (localStorage.getItem('stonein')) {
            $("#stonein").val(localStorage.getItem('stonein'));
        }
        if (localStorage.getItem('ironin')) {
            $("#ironin").val(localStorage.getItem('ironin'));
        }
        if (localStorage.getItem('foodin')) {
            $("#foodin").val(localStorage.getItem('foodin'));
        }
        if (localStorage.getItem('foodwc')) {
            if (localStorage.getItem('foodwc')==1) {
                $("#addfw").prop( "checked", true );
            }
        }
        if (localStorage.getItem('atroops')) {
            if (localStorage.getItem('atroops')==1) {
                $("#addtroops").prop( "checked", true );
            }
        }
        if (localStorage.getItem('ares')) {
            if (localStorage.getItem('ares')==1) {
                $("#addres").prop( "checked", true );
            }
        }
        if (localStorage.getItem('abuildings')) {
            if (localStorage.getItem('abuildings')==1) {
                $("#addbuildings").prop( "checked", true );
            }
        }
        if (localStorage.getItem('anotes')) {
            if (localStorage.getItem('anotes')==1) {
                $("#addnotes").prop( "checked", true );
            }
        }
        if (localStorage.getItem('awalls')) {
            if (localStorage.getItem('awalls')==1) {
                $("#addwalls").prop( "checked", true );
            }
        }if (localStorage.getItem('atowers')) {
            if (localStorage.getItem('atowers')==1) {
                $("#addtowers").prop( "checked", true );
            }
        }
        if (localStorage.getItem('ahub')) {
            if (localStorage.getItem('ahub')==1) {
                $("#addhub").prop( "checked", true );
            }
        }
        $("#addfw").change(function() {
            if ($("#addfw").prop( "checked")==true) {
                localStorage.setItem('foodwc',1);
            } else {localStorage.setItem('foodwc',0);}
        });
        $("#addnotes").change(function() {
            if ($("#addnotes").prop( "checked")==true) {
                localStorage.setItem('anotes',1);
            } else {localStorage.setItem('anotes',0);}
        });
        $("#addres").change(function() {
            if ($("#addres").prop( "checked")==true) {
                localStorage.setItem('ares',1);
            } else {localStorage.setItem('ares',0);}
        });
        $("#addtroops").change(function() {
            if ($("#addtroops").prop( "checked")==true) {
                localStorage.setItem('atroops',1);
            } else {localStorage.setItem('atroops',0);}
        });
        $("#addbuildings").change(function() {
            if ($("#addbuildings").prop( "checked")==true) {
                localStorage.setItem('abuildings',1);
            } else {localStorage.setItem('abuildings',0);}
        });
        $("#addwalls").change(function() {
            if ($("#addwalls").prop( "checked")==true) {
                localStorage.setItem('awalls',1);
            } else {localStorage.setItem('awalls',0);}
        });
        $("#addtowers").change(function() {
            if ($("#addtowers").prop( "checked")==true) {
                localStorage.setItem('atowers',1);
            } else {localStorage.setItem('atowers',0);}
        });
        $("#addhub").change(function() {
            if ($("#addhub").prop( "checked")==true) {
                localStorage.setItem('ahub',1);
            } else {localStorage.setItem('ahub',0);}
        });

        $("#cityNotesGo").click(function() {
            $("#selHub").remove();
            var selhub=$("#organiser").clone(false).attr({id:"selHub",style:"width:100%;height:28px;font-size:11;border-radius:6px;margin:7px"});
            $("#selhublist").append(selhub);
            if (localStorage.getItem('hublist')) {
                $("#selHub").val(localStorage.getItem('hublist')).change();
            }
            $("#selHub").change(function() {
                localStorage.setItem('hublist',$("#selHub").val());
            });
            $('#funkylayoutl').remove();
            $('#funkylayoutw').remove();
            setTimeout(function(){
                var currentlayout=$('#currentLOtextarea').text();
                for (var i=20; i<currentlayout.length-20;i++) {
                    var tmpchar=currentlayout.charAt(i);
                    //console.log(tmpchar);
                    var cmp=new RegExp(tmpchar);
                    if (!(cmp.test(emptyspots))) {
                        //console.log(String(cmp));
                        currentlayout=currentlayout.replaceAt(i,"-");
                    }
                }
                //console.log(currentlayout);
                var selectbuttsw='<select id="funkylayoutw" style="font-size: 10px !important;margin-top:1%;margin-left:2%;width:45%;" class="regButton greenb"><option value="0">Select water layout</option>';
                var ww=1;
                selectbuttsw+='<option value="'+ww+'">2 sec rang/tri/galley</option>';
                layoutsw.push("[ShareString.1.3];########################-------#-------#####--------#--------###---------#---------##---------#---------##------#######------##-----##BGBGB##-----##----##GBGBGBG##----##----#BGBGBGBGB#----##----#BGBGBGBGB#---H#######BGBGTGBGB#######----#BGBGBGBGB#JSPX##----#BGBGBGBGB#----##----##GBGBGBG##G---##-----##BGGGB##BBBBG##------#######BBVVBB##---------#--GBV##VB##---------#--GBV###V###--------#---BBV#######-------#----BBV########################");
                remarksw.push("rangers/triari/galley"); notesw.push("166600 inf and 334 galley @ 10 days");
                troopcounw.push([0,0,92000,74600,0,0,0,0,0,0,0,0,0,0,334,0,0]);
                resw.push([0,0,0,0,1,150000,220000,150000,350000,0,0,0,0,1,0,0,0,0,0,150000,220000,150000,350000]);
                ww++;
                selectbuttsw+='<option value="'+ww+'">6 sec arbs/galley</option>';
                layoutsw.push("[ShareString.1.3];########################-------#-------#####--------#--------###---------#---------##---------#---------##------#######------##-----##BEBEB##-----##----##EBEBEBE##----##----#BEBEBEBEB#----##----#BEBEBEBEB#----#######BEBETEBEB#######----#BEBEBEBEB#SPJX##----#BEBEBEBEB#MH--##----##EBEBEBE##----##-----##BEBEB##BBBB-##------#######BBVVBB##---------#---BVTTVB##---------#---BVTTTV###--------#--BBBVTT#####-------#--BEBBV########################");
                remarksw.push("arbs/galley"); notesw.push("88300 inf and 354 galley @ 11.5 days");
                troopcounw.push([0,0,0,0,0,0,0,0,88300,0,0,0,0,0,354,0,0]);
                resw.push([0,0,0,0,1,150000,220000,150000,350000,0,0,0,0,1,0,0,0,0,0,150000,220000,150000,350000]);
                ww++;
                selectbuttsw+='<option value="'+ww+'">3 sec priestess/galley</option>';
                layoutsw.push("[ShareString.1.3];########################-------#-------#####--------#--------###---------#---------##---------#---------##------#######------##-----##BZBZB##-----##----##ZBZBZBZ##----##----#BZBZBZBZB#----##----#BZBZBZBZB#---H#######BZBZTZBZB#######----#BZBZBZBZB#JSPX##----#BZBZBZBZB#----##----##ZBZBZBZ##-Z--##-----##BZZZB##BBBBZ##------#######BBVVBB##---------#---BV##VB##---------#--ZBV###V###--------#---BBV#######-------#---ZBBV########################");
                remarksw.push("priestess/galley"); notesw.push("166600 inf and 334 galley @ 11 days");
                troopcounw.push([0,0,0,0,166600,0,0,0,0,0,0,0,0,0,334,0,0]);
                resw.push([0,0,0,0,1,150000,220000,150000,350000,0,0,0,0,1,0,0,0,0,0,150000,220000,150000,350000]);
                ww++;
                selectbuttsw+='<option value="'+ww+'">7 sec praetor/galley</option>';
                layoutsw.push("[ShareString.1.3];########################-------#-------#####--------#--------###---------#---------##---------#---------##------#######------##-----##BZBZB##-----##----##ZBZBZBZ##----##----#BZBZBZBZB#----##----#BZBZBZBZB#----#######BZBZTZBZB#######----#BZBZBZBZB#SPJX##----#BZBZBZBZB#MH--##----##ZBZBZBZ##----##-----##BZBZB##BBBBZ##------#######BBVVBB##---------#---BVTTVB##---------#---BVTTTV###--------#---BBVTT#####-------#--BZBBV########################");
                remarksw.push("praetors/galley"); notesw.push("86650 praetors and 347 galley @ 12 days");
                troopcounw.push([0,0,0,0,0,0,0,0,0,86650,0,0,0,0,347,0,0]);
                resw.push([0,0,0,0,1,150000,220000,150000,350000,0,0,0,0,1,0,0,0,0,0,150000,220000,150000,350000]);
                ww++;
                selectbuttsw+='<option value="'+ww+'">2 sec vanq/galley+senator</option>';
                layoutsw.push("[ShareString.1.3];########################-------#-------#####--------#--------###---------#---------##---------#---------##------#######------##-----##BGBGB##-----##----##BBGBGBB##----##----#BGBGBGBGB#----##----#BGBGBGBGB#---H#######BGBGTGBGB#######----#BGBGBGBGB#JSPX##----#BGBGBGBGB#----##----##BBGBGBB##---B##-----##BGBGB##BBBBZ##------#######BBVVBB##---------#---BV##VB##---------#---BV###V###--------#---BBV#######-------#--BBBBV########################");
                remarksw.push("vanq/galley+senator"); notesw.push("193300 inf and 387 galley @ 10 days");
                troopcounw.push([0,0,0,0,0,193300,0,0,0,0,0,0,0,0,387,0,0]);
                resw.push([0,0,0,0,1,150000,220000,150000,350000,0,0,0,0,1,0,0,0,0,0,150000,220000,150000,350000]);
                ww++;
                selectbuttsw+='<option value="'+ww+'">5 sec horses/galley</option>';
                layoutsw.push("[ShareString.1.3];########################-------#-------#####--------#--------###---------#---------##---------#---------##------#######------##-----##BEBEB##-----##----##EBEBEBE##----##----#BEBEBEBEB#----##----#BEBEBEBEB#---H#######BEBETEBEB#######----#BEBEBEBEB#JSPX##----#BEBEBEBEB#-M--##----##EBEBEBB##----##-----##BEBEB##BBBB-##------#######BBVVBB##---------#---BV##VB##---------#---BV###V###--------#--BBBV#######-------#--BEBBV########################");
                remarksw.push("horses/galley"); notesw.push("90000 cav and 360 galley @ 10.5 days");
                troopcounw.push([0,0,0,0,0,0,0,0,0,0,90000,0,0,0,360,0,0]);
                resw.push([0,0,0,0,1,150000,220000,150000,350000,0,0,0,0,1,0,0,0,0,0,150000,220000,150000,350000]);
                ww++;
                selectbuttsw+='<option value="'+ww+'">5 sec sorc/galley</option>';
                layoutsw.push("[ShareString.1.3];########################-------#-------#####--------#--------###---------#---------##---------#---------##------#######------##-----##JBJBJ##-----##----##BJBJBJB##----##----#JBJBJBJBJ#----##----#JBJBJBJBJ#---H#######JBJBTBJBJ#######----#JBJBJBJBJ#-S-X##----#JBJBJBJBJ#----##----##BJBJBJB##JJ--##-----##JBJBJ##BBBBJ##------#######BBVVBB##---------#--JBV##VB##---------#--JBV###V###--------#---BBV#######-------#---JBBV########################");
                remarksw.push("sorc/galley"); notesw.push("156600 sorc and 314 galley @ 13.5 days");
                troopcounw.push([0,0,0,0,0,0,156600,0,0,0,0,0,0,0,314,0,0]);
                resw.push([0,0,0,0,1,150000,220000,150000,350000,0,0,0,0,1,0,0,0,0,0,150000,220000,150000,350000]);
                ww++;
                selectbuttsw+='<option value="'+ww+'">vanqs+ports+senator</option>';
                layoutsw.push("[ShareString.1.3];########################-------#-------#####--------#--------###---------#---------##---------#---------##------#######------##-----##BBBBB##-----##----##BBGBGBB##----##----#BGBGBGBGB#----##----#BGBBBBBGB#----#######BBBGTGBBB#######----#BGBBBBBGB#PPJX##----#BGBGBGBGB#BBBB##----##BBGBGBB##BBBB##-----##BBBBB##BBBBB##------#######-BRRBB##---------#----R##RZ##---------#----R###R###--------#----SR#######-------#----MSR########################");
                remarksw.push("vanqs+senator+ports"); notesw.push("264k infantry @ 10 days");
                troopcounw.push([0,0,0,100000,0,164000,0,0,0,0,0,0,0,0,0,0,0]);
                resw.push([0,0,0,0,1,150000,220000,150000,350000,0,0,0,0,1,0,0,0,0,0,150000,220000,150000,350000]);
                ww++;
                selectbuttsw+='<option value="'+ww+'">main hub</option>';
                layoutsw.push("[ShareString.1.3];########################-------#-------#####--------#---PPPPP###---------#---PPPPPP##---------#---PPPPPP##------#######PPPPPP##-----##-----##PPPPP##----##SLSDSAS##PPPP##----#-SDSMSDS-#PPPP##----#-SLSMSAS-#PPPP#######-SDSTSDS-#######----#-SLSMSAS-#----##----#-SDSMSDS-#----##----##SLSDSAS##----##-----##-----##-----##------#######--RR--##---------#ZB--RTTR-##---------#PJ--RTTTR###--------#-----RTT#####-------#------R########################");
                remarksw.push("main hub"); notesw.push("14 mil w/s 23 mil iron 15 mil food 8200 carts 240 boats");
                troopcounw.push([0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0]);
                resw.push([0,0,0,0,1,500000,500000,500000,500000,0,0,0,0,1,0,0,0,0,0,500000,500000,500000,500000]);
                ww++;
                selectbuttsw+='<option value="'+ww+'">palace storage</option>';
                layoutsw.push("[ShareString.1.3]:########################-------#-----PP#####--------#-----PPP###---------#-----PPPP##---------#-----PPPP##------#######--PPPP##-----##SASLS##-PPPP##----##ASASLSL##PPPP##----#SASASLSLS#-PPP##----#SASASLSLS#JPPP#######SASA#LSLS#######----#SASASLSLS#----##----#SASASLSLS#----##----##ASASLSL##----##-----##SASLS##-----##------#######------##---------#---------##---------#---------###--------#--------#####-------#-------########################");
                remarksw.push("palace storage"); notesw.push("40 mil w/s 6200 carts");
                troopcounw.push([0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0]);
                resw.push([0,0,0,0,1,500000,500000,500000,500000,0,0,0,0,1,0,0,0,0,0,500000,500000,500000,500000]);
                ww++;
                selectbuttsw+='<option value="'+ww+'">palace feeder</option>';
                layoutsw.push("[ShareString.1.3];########################-PPPPPP#PPPPPPP#####--PPPPPP#PPPPPPPP###---PPPPPP#PPPPPPPPP##---PPPPPP#PPPPPPPPP##----PP#######PPPPPP##-----##----J##PPPPP##----##-A-----##PPPP##----#-SSS-----#PPPP##----#-AAA-----#PPPP#######-SSST----#######----#-LLL-----#----##----#-SSS-----#----##----##-L-----##----##-----##-----##-----##------#######--__--##---------#----_##_-##---------#----_###_###--------#-----_#######-------#------_########################");
                remarksw.push("palace feeder"); notesw.push("8.75 mil w/s 16400 carts");
                troopcounw.push([0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0]);
                resw.push([0,0,0,0,1,500000,500000,500000,500000,0,0,0,0,1,0,0,0,0,0,500000,500000,500000,500000]);
                ww++;
                selectbuttsw+='<option value="'+ww+'">palace Hub mixed</option>';
                layoutsw.push("[ShareString.1.3];########################-------#PPPPPPP#####--------#PPPPPPPP###---------#PPPPPPPPP##---------#PPPPPPPPP##------#######PPPPPP##-----##-----##PPPPP##----##-------##PPPP##----#SLSASLSAS#PPPP##----#SASLSASLS#JPPP#######SLSATLSAS#######----#SASLSASLS#----##----#SLSASLSAS#----##----##-------##----##-----##-----##-----##------#######--__--##---------#----_TT_-##---------#----_TTT_###--------#-----_TT#####-------#------_########################");
                remarksw.push("palace Hub mixed"); notesw.push("24.57 mil w/s 11000 carts");
                troopcounw.push([0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0]);
                resw.push([0,0,0,0,1,500000,500000,500000,500000,0,0,0,0,1,0,0,0,0,0,500000,500000,500000,500000]);
                ww++;
                selectbuttsw+='<option value="'+ww+'">Stingers</option>';
                layoutsw.push("[ShareString.1.3];########################-------#-------#####--------#--------###---------#---------##---------#---------##------#######------##-----##-----##-----##----##-------##----##----#---------#----##----#---------#----#######----T----#######----#---------#SPHX##----#---------#-M--##----##-------##----##-----##-----##BBBB-##------#######BBVVBB##---------#---BVTTVB##---------#---BVTTTV###--------#---BBVTT#####-------#----BBV########################");
                remarksw.push("stingers"); notesw.push("3480 stingers @ 84 days");
                troopcounw.push([0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,3480,0]);
                resw.push([0,0,0,0,1,500000,500000,500000,500000,0,0,0,0,1,0,0,0,0,0,500000,500000,500000,500000]);
                ww++;
                selectbuttsw+='<option value="'+ww+'">Warships</option>';
                layoutsw.push("[ShareString.1.3];########################-------#-------#####--------#--------###---------#---------##---------#---------##------#######------##-----##-----##-----##----##-------##----##----#---------#----##----#---------#----#######----T----#######----#---------#SPHX##----#---------#-M--##----##-------##----##-----##-----##BBBB-##------#######BBVVBB##---------#---BVTTVB##---------#---BVTTTV###--------#---BBVTT#####-------#----BBV########################");
                remarksw.push("warships"); notesw.push("870 warships @ 42 days");
                troopcounw.push([0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,870]);
                resw.push([0,0,0,0,1,500000,500000,500000,500000,0,0,0,0,1,0,0,0,0,0,500000,500000,500000,500000]);
                selectbuttsw+='</select>';

                var selectbuttsl='<select id="funkylayoutl" style="font-size: 10px !important;margin-top:1%;margin-left:2%;width:45%;" class="regButton greenb"><option value="0">Select land layout</option>';
                var ll=1;
                selectbuttsl+='<option value="'+ll+'">1 sec vanqs</option>';
                layoutsl.push("[ShareString.1.3]:########################-------#-------#####--------#--------###---------#---------##---------#---------##------#######------##-----##GBGBG##-----##----##BGBGBGB##----##----#GBGBGBGBG#----##----#GBGBGBGBG#----#######GBGBTBGBG#######----#GBGBGBGBG#----##----#GBGBGBGBG#----##----##BGBGBGB##----##GGGGG##GBGBG##-----##BBBBB-#######------##GGGGGG--H#---------##BBBBBB--J#---------###GGGG---X#--------#####BB----S#-------########################");
                remarksl.push("vanqs"); notesl.push("180000 vanqs @ 2 days");
                troopcounl.push([0,0,0,0,0,180000,0,0,0,0,0,0,0,0,0,0,0]);
                resl.push([0,0,0,0,1,150000,220000,150000,350000,0,0,0,0,1,0,0,0,0,0,150000,220000,150000,350000]);
                ll++;
                selectbuttsl+='<option value="'+ll+'">2 sec vanqs</option>';
                layoutsl.push("[ShareString.1.3]:########################BBB--JX#-------#####BGBG--PP#--------###-BBBBB-MS#---------##-BGBGB--H#---------##-BGBGB#######------##-ZBB-##BBBBB##-----##----##BBGBGBB##----##----#BGBGBGBGB#----##----#BGBBBBBGB#----#######BGBGTGBGB#######----#BGBBBBBGB#----##----#BGBGBGBGB#----##----##BBGBGBB##----##-----##BBBBB##-----##------#######------##---------#---------##---------#---------###--------#--------#####-------#-------########################");
                remarksl.push("vanqs"); notesl.push("264000 vanqs @ 6 days");
                troopcounl.push([0,0,0,0,0,264000,0,0,0,0,0,0,0,0,0,0,0]);
                resl.push([0,0,0,0,1,150000,220000,150000,350000,0,0,0,0,1,0,0,0,0,0,150000,220000,150000,350000]);
                ll++;
                selectbuttsl+='<option value="'+ll+'">3 sec vanqs raiding</option>';
                layoutsl.push("[ShareString.1.3];########################----PJX#-------#####BB----PP#--------###BGBGB--SS#---------##BBBBB--MP#---------##BGBGB-#######------##BBBBB##BBBBB##-----##--G-##BBGBGBB##----##----#BBBBBBBBB#----##----#BGBGBGBGB#----#######BBBBTBBBB#######----#BGBGBGBGB#----##----#BBBBBBBBB#----##----##BBGBGBB##----##-----##BBBBB##-----##------#######--__--##---------#----_##_-##---------#----_###_###--------#-----_#######-------#------_########################");
                remarksl.push("vanqs"); notesl.push("296000 vanqs @ 10 days");
                troopcounl.push([0,0,0,0,0,296000,0,0,0,0,0,0,0,0,0,0,0]);
                resl.push([0,0,0,0,1,150000,220000,150000,350000,0,0,0,0,1,0,0,0,0,0,150000,220000,150000,350000]);
                ll++;
                selectbuttsl+='<option value="'+ll+'">2 sec rang/3 sec tri</option>';
                layoutsl.push("[ShareString.1.3]:########################BB---JX#-------#####BGBGB-PP#--------###-BGBGB-MS#---------##-BGBGB--H#---------##-BGBGB#######------##--BBB##BGBGB##-----##----##BBGBGBB##----##----#BGBGBGBGB#----##----#BGBGBGBGB#----#######BGBGTGBGB#######----#BGBGBGBGB#----##----#BGBGBGBGB#----##----##BBGBGBB##----##-----##BBBBB##-----##------#######------##---------#---------##---------#---------###--------#--------#####-------#-------########################");
                remarksl.push("rangers/triari"); notesl.push("236000 inf @ 6.5 days");
                troopcounl.push([0,0,160000,76000,0,0,0,0,0,0,0,0,0,0,0,0,0]);
                resl.push([0,0,0,0,1,150000,220000,150000,350000,0,0,0,0,1,0,0,0,0,0,150000,220000,150000,350000]);
                ll++;
                selectbuttsl+='<option value="'+ll+'">6 sec praetors</option>';
                layoutsl.push("[ShareString.1.3]:########################BB---JX#-------#####BZBZB-PP#--------###-BZBZB-MS#---------##-BZBZB--H#---------##-BZBZB#######------##--BBB##BZBZB##-----##----##ZBZBZBZ##----##----#BZBZBZBZB#----##----#BZBZBZBZB#----#######BZBZTZBZB#######----#BZBZBZBZB#----##----#BZBZBZBZB#----##----##BBZBZBB##----##-----##BZBZB##-----##------#######------##---------#---------##---------#---------###--------#--------#####-------#-------########################");
                remarksl.push("praetors"); notesl.push("110000 praetors @ 7.5 days");
                troopcounl.push([0,0,0,0,0,0,0,0,0,110000,0,0,0,0,0,0,0]);
                resl.push([0,0,0,0,1,150000,220000,150000,350000,0,0,0,0,1,0,0,0,0,0,150000,220000,150000,350000]);
                ll++;
                selectbuttsl+='<option value="'+ll+'">4 sec horses</option>';
                layoutsl.push("[ShareString.1.3]:########################BB---JX#-------#####BEBEB-PP#--------###-BEBEB-MS#---------##-BEBEB--H#---------##-BEBEB#######------##--ZBB##BEBEB##-----##----##EBEBEBE##----##----#BEBEBEBEB#----##----#BEBEBEBEB#----#######BEBETEBEB#######----#BEBEBEBEB#----##----#BEBEBEBEB#----##----##BBEBEBE##----##-----##BEBEB##-----##------#######------##---------#---------##---------#---------###--------#--------#####-------#-------########################");
                remarksl.push("horses"); notesl.push("106000 horses @ 5 days");
                troopcounl.push([0,0,0,0,0,0,0,0,0,0,106000,0,0,0,0,0,0]);
                resl.push([0,0,0,0,1,150000,220000,150000,350000,0,0,0,0,1,0,0,0,0,0,150000,220000,150000,350000]);
                ll++;
                selectbuttsl+='<option value="'+ll+'">5 sec horses</option>';
                layoutsl.push("[ShareString.1.3]:########################-B---JX#-------#####BEBEB-PP#--------###-BEBEB-MS#---------##-BEBEB-PH#---------##-BEBEB#######------##--BBB##BBBBB##-----##----##BBEBEBB##----##----#BEBEBEBEB#----##----#BEBEBEBEB#----#######BEBBTBBEB#######----#BEBEBEBEB#----##----#BEBEBEBEB#----##----##BBEBEBB##----##-----##BBBBB##-----##------#######------##---------#---------##---------#---------###--------#--------#####-------#-------########################");
                remarksl.push("horses"); notesl.push("124000 horses @ 7 days");
                troopcounl.push([0,0,0,0,0,0,0,0,0,0,124000,0,0,0,0,0,0]);
                resl.push([0,0,0,0,1,150000,220000,150000,350000,0,0,0,0,1,0,0,0,0,0,150000,220000,150000,350000]);
                ll++;
                selectbuttsl+='<option value="'+ll+'">5 sec arbs</option>';
                layoutsl.push("[ShareString.1.3]:########################BB---JX#-------#####BEBEB-PP#--------###-BEBEB-MS#---------##-BEBEB--H#---------##-BEBEB#######------##--BBB##BEBEB##-----##----##EBEBEBE##----##----#BEBEBEBEB#----##----#BEBEBEBEB#----#######BEBETEBEB#######----#BEBEBEBEB#----##----#BEBEBEBEB#----##----##BBEBEBB##----##-----##BEBEB##-----##------#######------##---------#---------##---------#---------###--------#--------#####-------#-------########################");
                remarksl.push("arbs"); notesl.push("110000 arbs @ 6.5 days");
                troopcounl.push([0,0,0,0,0,0,0,0,110000,0,0,0,0,0,0,0,0]);
                resl.push([0,0,0,0,1,150000,220000,150000,350000,0,0,0,0,1,0,0,0,0,0,150000,220000,150000,350000]);
                ll++;
                selectbuttsl+='<option value="'+ll+'">6 sec arbs</option>';
                layoutsl.push("[ShareString.1.3]:########################BB---JX#-------#####BEBEB-PP#--------###-BBBEB-MS#---------##-BEBEB--H#---------##-BEBEB#######------##--BBB##BBBBB##-----##----##BBEBEBB##----##----#BEBEBEBEB#----##----#BEBEBEBEB#----#######BEBETEBEB#######----#BEBEBEBEB#----##----#BEBEBEBEB#----##----##BBEBEBB##----##-----##BBBBB##-----##------#######------##---------#---------##---------#---------###--------#--------#####-------#-------########################");
                remarksl.push("arbs"); notesl.push("124000 arbs @ 8.5 days");
                troopcounl.push([0,0,0,0,0,0,0,0,124000,0,0,0,0,0,0,0,0]);
                resl.push([0,0,0,0,1,150000,220000,150000,350000,0,0,0,0,1,0,0,0,0,0,150000,220000,150000,350000]);
                ll++;
                selectbuttsl+='<option value="'+ll+'">4 sec sorc</option>';
                layoutsl.push("[ShareString.1.3]:########################BJBJ--X#-------#####JBJBJ--S#--------###-JBJBJ--M#---------##-JBJBJ--H#---------##-JBJBJ#######------##-ZBJB##JBJBJ##-----##----##BJBJBJB##----##----#JBJBJBJBJ#----##----#JBJBJBJBJ#----#######JBJBTBJBJ#######----#JBJBJBJBJ#----##----#JBJBJBJBJ#----##----##BJBJBJB##----##-----##JBJBJ##-----##------#######------##---------#---------##---------#---------###--------#--------#####-------#-------########################");
                remarksl.push("sorc"); notesl.push("176000 sorc @ 8 days");
                troopcounl.push([0,0,0,0,0,0,176000,0,0,0,0,0,0,0,0,0,0]);
                resl.push([0,0,0,0,1,150000,220000,150000,350000,0,0,0,0,1,0,0,0,0,0,150000,220000,150000,350000]);
                ll++;
                selectbuttsl+='<option value="'+ll+'">5 sec sorc</option>';
                layoutsl.push("[ShareString.1.3]:########################BBB---X#-------#####BJBJB--P#--------###-BJBJB-MS#---------##-BJBJB--H#---------##-BJBJB#######------##-ZBBB##BJBJB##-----##----##JBJBJBJ##----##----#BJBJBJBJB#----##----#BJBJBJBJB#----#######BJBJTJBJB#######----#BJBJBJBJB#----##----#BJBJBJBJB#----##----##BBJBJBB##----##-----##BJBJB##-----##------#######------##---------#---------##---------#---------###--------#--------#####-------#-------########################");
                remarksl.push("sorc"); notesl.push("224000 sorc @ 13 days");
                troopcounl.push([0,0,0,0,0,0,224000,0,0,0,0,0,0,0,0,0,0]);
                resl.push([0,0,0,0,1,150000,220000,150000,350000,0,0,0,0,1,0,0,0,0,0,150000,220000,150000,350000]);
                ll++;
                selectbuttsl+='<option value="'+ll+'">10 sec druids</option>';
                layoutsl.push("[ShareString.1.3]:########################-J----X#-------#####JBJB--MS#--------###BJBJB---H#---------##BJBJB----#---------##BJBJB-#######------##BJBJB##BJBJB##-----##-JBJ##JBJBJBJ##----##----#BJBJBJBJB#----##----#BJBJBJBJB#----#######BJBJTJBJB#######----#BJBJBJBJB#----##----#BJBJBJBJB#----##----##JBJBJBJ##----##-----##BJBJB##-----##------#######------##---------#---------##---------#---------###--------#--------#####-------#-------########################");
                remarksl.push("druids"); notesl.push("102000 druids @ 12 days");
                troopcounl.push([0,0,0,0,0,0,0,0,0,0,0,102000,0,0,0,0,0]);
                resl.push([0,0,0,0,1,150000,220000,150000,350000,0,0,0,0,1,0,0,0,0,0,150000,220000,150000,350000]);
                ll++;
                selectbuttsl+='<option value="'+ll+'">scorp/rams</option>';
                layoutsl.push("[ShareString.1.3]:########################BBYB--X#-------#####BYBYB---#--------###-BYBYB-MS#---------##-BYBYB--H#---------##-BYBYB#######------##-BYBB##BYBYB##-----##----##YBYBYBY##----##----#BYBYBYBYB#----##----#BYBYBYBYB#----#######BYBYTYBYB#######----#BYBYBYBYB#----##----#BYBYBYBYB#----##----##YBYBYBY##----##-----##BYBYB##-----##------#######------##---------#---------##---------#---------###--------#--------#####-------#-------########################");
                remarksl.push("scorp/rams"); notesl.push("21600 siege engines @ 7.5 days");
                troopcounl.push([0,0,0,0,0,0,0,0,0,0,0,0,5500,16100,0,0,0]);
                resl.push([0,0,0,0,1,150000,220000,150000,350000,0,0,0,0,1,0,0,0,0,0,150000,220000,150000,350000]);
                ll++;
                selectbuttsl+='<option value="'+ll+'">ballista</option>';
                layoutsl.push("[ShareString.1.3]:########################BBBB--X#-------#####BYBYB---#--------###-BYBYB-MS#---------##-BYBYB--H#---------##-BYBYB#######------##-BBBB##BBBBB##-----##----##BBYBYBB##----##----#BYBYBYBYB#----##----#BYBYBYBYB#----#######BYBYTYBYB#######----#BYBYBYBYB#----##----#BYBYBYBYB#----##----##BBYBYBB##----##-----##BBBBB##-----##------#######------##---------#---------##---------#---------###--------#--------#####-------#-------########################");
                remarksl.push("ballista"); notesl.push("25600 siege engines @ 10.5 days");
                troopcounl.push([0,25600,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0]);
                resl.push([0,0,0,0,1,150000,220000,150000,350000,0,0,0,0,1,0,0,0,0,0,150000,220000,150000,350000]);
                selectbuttsl+='</select>';

                $('#removeoverlayGo').after(selectbuttsl);
                $('#funkylayoutl').after(selectbuttsw);
                $('#funkylayoutl').change(function() {
                    var newlayout=currentlayout;
                    //console.log($('#funkylayoutl').val());
                    for (var j=1; j<layoutsl.length; j++) {
                        if ($('#funkylayoutl').val()==j) {
                            for (var i=20; i<currentlayout.length;i++) {
                                var tmpchar=layoutsl[j].charAt(i);
                                var cmp=new RegExp(tmpchar);
                                if (!(cmp.test(emptyspots))) {
                                    //console.log(String(cmp));
                                    newlayout=newlayout.replaceAt(i,tmpchar);
                                    //currentlayout=currentlayout.replaceAt(i,tmpchar);
                                }
                            }
                            //console.log(newlayout);
                            //console.log(currentlayout);
                            //$('#removeoverlayGo').trigger({type:"click",originalEvent:"1"});
                            $('#overlaytextarea').val(newlayout);
                            setTimeout(function(){$('#applyoverlayGo').trigger({type:"click",originalEvent:"1"});},300);
                            if ($("#addnotes").prop("checked")==true) {
                                //setTimeout(function(){$('#citynotesTab').trigger({type:"click",originalEvent:"1"});},200);
                                //$('#CNremarks').val("");
                                //$('#citynotestextarea').val("");
                                //setTimeout(function(){$('#citnotesaveb').trigger({type:"click",originalEvent:"1"});},100);
                                $('#CNremarks').val(remarksl[j]);
                                $('#citynotestextarea').val(notesl[j]);
                                setTimeout(function(){$('#citnotesaveb').trigger({type:"click",originalEvent:"1"}); },100);
                            }
                            var aa=poll2.city.mo;
                            //console.log(aa);
                            if ($("#addtroops").prop("checked")==true) {
                                for (var k in troopcounl[j]) {
                                    aa[9+Number(k)]=troopcounl[j][k];
                                }
                            }
                            if ($("#addwalls").prop("checked")==true) {
                                aa[26]=1;
                            }
                            if ($("#addtowers").prop("checked")==true) {
                                aa[27]=1;
                            }
                            if ($("#addhub").prop("checked")==true) {
                                var hubs={cid:[],distance:[]};
                                $.each(poll2.player.clc, function(key, value) {
                                    if (key==$("#selHub").val()) {
                                        hubs.cid=value;
                                    }
                                });
                                for (var i in hubs.cid) {
                                    var tempx=Number(hubs.cid[i] % 65536);
                                    var tempy=Number((hubs.cid[i]-tempx)/65536);
                                    hubs.distance.push(Math.sqrt((tempx-city.x)*(tempx-city.x)+(tempy-city.y)*(tempy-city.y)));
                                }
                                var mindist = Math.min.apply(Math, hubs.distance);
                                var nearesthub=hubs.cid[hubs.distance.indexOf(mindist)];
                                resl[j][14]=nearesthub;
                                resl[j][15]=nearesthub;
                            } else {
                                resl[j][14]=0;
                                resl[j][15]=0;
                            }
                            if ($("#addres").prop("checked")==true) {
                                resl[j][5]=$("#woodin").val();
                                resl[j][6]=$("#stonein").val();
                                resl[j][7]=$("#ironin").val();
                                resl[j][8]=$("#foodin").val();
                                resl[j][19]=$("#woodin").val();
                                resl[j][20]=$("#stonein").val();
                                resl[j][21]=$("#ironin").val();
                                resl[j][22]=$("#foodin").val();
                                for (var k in resl[j]) {
                                    aa[28+Number(k)]=resl[j][k];
                                }
                            }
                            if ($("#addbuildings").prop("checked")==true) {
                                aa[51]=[1,$("#cablev").val()];
                                aa[1]=1;
                            }
                            if ($("#addfw").prop("checked")==true) {
                                var fw=$("#foodw").val();
                                var datf={cid: poll2.city.cid,a: fw};
                                jQuery.ajax({url: 'includes/svFW.php',type: 'POST',aysnc:false,data: datf});
                            }

                            //console.log(aa);
                           //var aaa=JSON.stringify(aa);
                            var dat={a:JSON.stringify(aa),b:poll2.city.cid};
                            //console.log(dat);
                            jQuery.ajax({url: 'includes/mnio.php',type: 'POST',aysnc:false,data: dat});

                        }
                    }
                });
                $('#funkylayoutw').change(function() {
                    var newlayout=currentlayout;
                    //console.log($('#funkylayoutw').val());
                    for (var j=1; j<layoutsw.length; j++) {
                        if ($('#funkylayoutw').val()==j) {
                            for (var i=20; i<currentlayout.length;i++) {
                                var tmpchar=layoutsw[j].charAt(i);
                                var cmp=new RegExp(tmpchar);
                                if (!(cmp.test(emptyspots))) {
                                    //console.log(String(cmp));
                                    newlayout=newlayout.replaceAt(i,tmpchar);
                                }
                            }
                            //$('#removeoverlayGo').trigger({type:"click",originalEvent:"1"});
                            $('#overlaytextarea').val(newlayout);
                            setTimeout(function(){$('#applyoverlayGo').trigger({type:"click",originalEvent:"1"});},300);
                            if ($("#addnotes").prop("checked")==true) {
                                //setTimeout(function(){$('#citynotesTab').trigger({type:"click",originalEvent:"1"});},200);
                                //$('#CNremarks').val("");
                                //$('#citynotestextarea').val("");
                                //setTimeout(function(){$('#citnotesaveb').trigger({type:"click",originalEvent:"1"});},100);
                                $('#CNremarks').val(remarksw[j]);
                                $('#citynotestextarea').val(notesw[j]);
                                setTimeout(function(){$('#citnotesaveb').trigger({type:"click",originalEvent:"1"}); },100);
                            }
                            var aa=poll2.city.mo;
                            //console.log(aa);
                            if ($("#addtroops").prop("checked")==true) {
                                for (var k in troopcounw[j]) {
                                    aa[9+Number(k)]=troopcounw[j][k];
                                }
                            }
                            if ($("#addwalls").prop("checked")==true) {
                                aa[26]=1;
                            }
                            if ($("#addtowers").prop("checked")==true) {
                                aa[27]=1;
                            }
                            if ($("#addhub").prop("checked")==true) {
                                var hubs={cid:[],distance:[]};
                                $.each(poll2.player.clc, function(key, value) {
                                    if (key==$("#selHub").val()) {
                                        hubs.cid=value;
                                    }
                                });
                                for (var i in hubs.cid) {
                                    var tempx=Number(hubs.cid[i] % 65536);
                                    var tempy=Number((hubs.cid[i]-tempx)/65536);
                                    hubs.distance.push(Math.sqrt((tempx-city.x)*(tempx-city.x)+(tempy-city.y)*(tempy-city.y)));
                                }
                                var mindist = Math.min.apply(Math, hubs.distance);
                                var nearesthub=hubs.cid[hubs.distance.indexOf(mindist)];
                                resw[j][14]=nearesthub;
                                resw[j][15]=nearesthub;
                            } else {
                                resw[j][14]=0;
                                resw[j][15]=0;
                            }
                            if ($("#addres").prop("checked")==true) {
                                resw[j][5]=$("#woodin").val();
                                resw[j][6]=$("#stonein").val();
                                resw[j][7]=$("#ironin").val();
                                resw[j][8]=$("#foodin").val();
                                resw[j][19]=$("#woodin").val();
                                resw[j][20]=$("#stonein").val();
                                resw[j][21]=$("#ironin").val();
                                resw[j][22]=$("#foodin").val();
                                for (var k in resw[j]) {
                                    aa[28+Number(k)]=resw[j][k];
                                }
                            }
                            if ($("#addbuildings").prop("checked")==true) {
                                aa[51]=[1,$("#cablev").val()];
                                aa[1]=1;
                            }
                            if ($("#addfw").prop("checked")==true) {
                                var fw=$("#foodw").val();
                                var datf={cid: cotg.city.id(),a: fw};
                                jQuery.ajax({url: 'includes/svFW.php',type: 'POST',aysnc:false,data: datf});
                            }
                            //console.log(aa);
                           //var aaa=JSON.stringify(aa);
                            var dat={a:JSON.stringify(aa),b:cotg.city.id()};
                            //console.log(dat);
                            jQuery.ajax({url: 'includes/mnio.php',type: 'POST',aysnc:false,data: dat});
                        }
                    }
                });
            },500);
        });
    });

    $(document).ready(function() {
        $("#loccavwarconGo").click(function() {
            //createTable();
            setTimeout(function(){getDugRows();}, 1000);
        });
        $("#raidmantab").click(function() {
            setTimeout(function(){getDugRows();}, 1000);
        });
        $("#allianceIncomings").parent().click(function() {
            setTimeout(function(){incomings();}, 4000);
        });
        $("#ui-id-37").click(function() {
            setTimeout(function(){incomings();}, 1000);
        });
        if (localStorage.getItem("raidbox")==1) {
            var raidboxback="<button class='regButton greenb' id='raidboxb' style='width:120px; margin-left: 2%;'>Return Raiding Box</button>";
            $("#squaredung td").find(".squarePlayerInfo").before(raidboxback);
            $("#raidboxb").click(function() {
                localStorage.setItem("raidbox","0");
                $("#raidboxb").remove();
            });
        }
       /* var cancelallya="<input id='cancelAllya' type='checkbox' checked='checked'> Cancel attack if same alliance";
        var cancelallys="<input id='cancelAllys' type='checkbox' checked='checked'> Cancel attack if same alliance";
        var cancelallyp="<input id='cancelAllyp' type='checkbox' checked='checked'> Cancel attack if same alliance";
        var cancelallyc="<input id='cancelAllyc' type='checkbox' checked='checked'> Cancel attack if same alliance";
        $("#assaulttraveltime").parent().next().html(cancelallya);
        $("#siegetraveltime").parent().next().html(cancelallys);
        $("#plundtraveltime").parent().next().html(cancelallyp);
        $("#scouttraveltime").parent().next().html(cancelallyc);*/
        $("#assaultGo").click(function() {
            if ($("#cancelAllya").prop("checked")==false) {
                //console.log("cancel pressed");
                setTimeout(function() {
                    /*
                    $(".shAinf").each(function() {
                        var tid=$(this).parent().next().find(".cityblink").attr("data")
                        var tx=tid%65536;
                        var ty=(tid-tx)/65536;
                        if (tx==$("#assaultxcoord").val() && ty==$("#assaultycoord").val()) {
                            var aid=$(this).attr("data");
                            //console.log(aid);
                            var dat={a: aid,b:1};
                            jQuery.ajax({url: 'includes/UaO.php',type: 'POST',aysnc:false, data:dat});
                        }
                    });
                    $(".shPinf").each(function() {
                        var tid=$(this).parent().next().find(".cityblink").attr("data")
                        var tx=tid%65536;
                        var ty=(tid-tx)/65536;
                        if (tx==$("#assaultxcoord").val() && ty==$("#assaultycoord").val()) {
                            var aid=$(this).attr("data");
                            //console.log(aid);
                            var dat={a: aid,b:1};
                            jQuery.ajax({url: 'includes/UpO.php',type: 'POST',aysnc:false, data:dat});
                        }
                    });*/
                    var cstr=$("#assaultxcoord").val() +":"+ $("#assaultycoord").val();
                    for (var i in poll2.OGA) {
                        if (cstr==poll2.OGA[i][5]) {
                            var aid=poll2.OGA[i][2];
                            if (poll2.OGA[i][1]=="Assault") {
                                $(".shAinf").each(function() {
                                    if ($(this).attr("data")==aid) {
                                        $(this).trigger({type:"click",originalEvent:"1"});
                                        setTimeout(function() {
                                            $("#cancAtkIfAlly").trigger({type:"click",originalEvent:"1"});
                                            $("#doneOG").trigger({type:"click",originalEvent:"1"});
                                            $("#outgoingPopUpBox").hide();
                                        },400);
                                    }
                                });
                            } else {
                                $(".shPinf").each(function() {
                                    if ($(this).attr("data")==aid) {
                                        $(this).trigger({type:"click",originalEvent:"1"});
                                        setTimeout(function() {
                                            $("#cancAtkIfAlly").trigger({type:"click",originalEvent:"1"});
                                            $("#doneOG").trigger({type:"click",originalEvent:"1"});
                                            $("#outgoingPopUpBox").hide();
                                        },400);
                                    }
                                });
                            }
                            /*var dat={a: aid,b:1};
                            if (poll2.OGA[i][1]=="Assault") {
                                jQuery.ajax({url: 'includes/UaO.php',type: 'POST',aysnc:false, data:dat});
                            } else {
                                jQuery.ajax({url: 'includes/UpO.php',type: 'POST',aysnc:false, data:dat});
                            }*/
                        }
                    }
                },4000);
            }
        });
        $("#plunderGo").click(function() {
            if ($("#cancelAllyp").prop("checked")==false) {
                //console.log("cancel pressed");
                setTimeout(function() {
                    /*$(".shAinf").each(function() {
                        var tid=$(this).parent().next().find(".cityblink").attr("data")
                        var tx=tid%65536;
                        var ty=(tid-tx)/65536;
                        if (tx==$("#pluxcoord").val() && ty==$("#pluycoord").val()) {
                            var aid=$(this).attr("data");
                            //console.log(aid);
                            var dat={a: aid,b:1};
                            jQuery.ajax({url: 'includes/UaO.php',type: 'POST',aysnc:false, data:dat});
                        }
                    });
                    $(".shPinf").each(function() {
                        var tid=$(this).parent().next().find(".cityblink").attr("data")
                        var tx=tid%65536;
                        var ty=(tid-tx)/65536;
                        if (tx==$("#pluxcoord").val() && ty==$("#pluycoord").val()) {
                            var aid=$(this).attr("data");
                            //console.log(aid);
                            var dat={a: aid,b:1};
                            jQuery.ajax({url: 'includes/UpO.php',type: 'POST',aysnc:false, data:dat});
                        }
                    });*/
                    var cstr=$("#pluxcoord").val() +":"+ $("#pluycoord").val();
                    for (var i in poll2.OGA) {
                        if (cstr==poll2.OGA[i][5]) {
                            var aid=poll2.OGA[i][2];
                            var dat={a: aid,b:1};
                            if (poll2.OGA[i][1]=="Plunder") {
                                $(".shAinf").each(function() {
                                    if ($(this).attr("data")==aid) {
                                        $(this).trigger({type:"click",originalEvent:"1"});
                                        setTimeout(function() {
                                            $("#cancAtkIfAlly").trigger({type:"click",originalEvent:"1"});
                                            $("#doneOG").trigger({type:"click",originalEvent:"1"});
                                            $("#outgoingPopUpBox").hide();
                                        },400);
                                    }
                                });
                            } else {
                                $(".shPinf").each(function() {
                                    if ($(this).attr("data")==aid) {
                                        $(this).trigger({type:"click",originalEvent:"1"});
                                        setTimeout(function() {
                                            $("#cancAtkIfAlly").trigger({type:"click",originalEvent:"1"});
                                            $("#doneOG").trigger({type:"click",originalEvent:"1"});
                                            $("#outgoingPopUpBox").hide();
                                        },400);
                                    }
                                });
                            }
                        }
                    }
                },4000);
            }
        });
        $("#scoutGo").click(function() {
            if ($("#cancelAllyc").prop("checked")==false) {
                //console.log("cancel pressed");
                setTimeout(function() {
                    /*$(".shAinf").each(function() {
                        var tid=$(this).parent().next().find(".cityblink").attr("data")
                        var tx=tid%65536;
                        var ty=(tid-tx)/65536;
                        if (tx==$("#scoxcoord").val() && ty==$("#scoycoord").val()) {
                            var aid=$(this).attr("data");
                            //console.log(aid);
                            var dat={a: aid,b:1};
                            jQuery.ajax({url: 'includes/UaO.php',type: 'POST',aysnc:false, data:dat});
                        }
                    });
                    $(".shPinf").each(function() {
                        var tid=$(this).parent().next().find(".cityblink").attr("data")
                        var tx=tid%65536;
                        var ty=(tid-tx)/65536;
                        if (tx==$("#scoxcoord").val() && ty==$("#scoycoord").val()) {
                            var aid=$(this).attr("data");
                            //console.log(aid);
                            var dat={a: aid,b:1};
                            jQuery.ajax({url: 'includes/UpO.php',type: 'POST',aysnc:false, data:dat});
                        }
                    });*/
                    var cstr=$("#scoxcoord").val() +":"+ $("#scoycoord").val();
                    for (var i in poll2.OGA) {
                        if (cstr==poll2.OGA[i][5]) {
                            var aid=poll2.OGA[i][2];
                            var dat={a: aid,b:1};
                            if (poll2.OGA[i][1]=="Scout") {
                                $(".shAinf").each(function() {
                                    if ($(this).attr("data")==aid) {
                                        $(this).trigger({type:"click",originalEvent:"1"});
                                        setTimeout(function() {
                                            $("#cancAtkIfAlly").trigger({type:"click",originalEvent:"1"});
                                            $("#doneOG").trigger({type:"click",originalEvent:"1"});
                                            $("#outgoingPopUpBox").hide();
                                        },400);
                                    }
                                });
                            } else {
                                $(".shPinf").each(function() {
                                    if ($(this).attr("data")==aid) {
                                        $(this).trigger({type:"click",originalEvent:"1"});
                                        setTimeout(function() {
                                            $("#cancAtkIfAlly").trigger({type:"click",originalEvent:"1"});
                                            $("#doneOG").trigger({type:"click",originalEvent:"1"});
                                            $("#outgoingPopUpBox").hide();
                                        },400);
                                    }
                                });
                            }
                        }
                    }
                },4000);
            }
        });
        $("#siegeGo").click(function() {
            if ($("#cancelAllys").prop("checked")==false) {
                //console.log("cancel pressed");
                setTimeout(function() {
                    //console.log("ally");
                    /*$(".shAinf").each(function() {
                        var tid=$(this).parent().next().find(".cityblink").attr("data")
                        var tx=tid%65536;
                        var ty=(tid-tx)/65536;
                        //console.log(tx,ty);
                        if (tx==$("#siexcoord").val() && ty==$("#sieycoord").val()) {
                            var aid=$(this).attr("data");
                            //console.log(aid);
                            var dat={a: aid,b:1};
                            jQuery.ajax({url: 'includes/UaO.php',type: 'POST',aysnc:false, data:dat});
                        }
                    });
                    $(".shPinf").each(function() {
                        var tid=$(this).parent().next().find(".cityblink").attr("data")
                        var tx=tid%65536;
                        var ty=(tid-tx)/65536;
                        //console.log(tx,ty);
                        if (tx==$("#siexcoord").val() && ty==$("#sieycoord").val()) {
                            var aid=$(this).attr("data");
                            //console.log(aid);
                            var dat={a: aid,b:1};
                            jQuery.ajax({url: 'includes/UpO.php',type: 'POST',aysnc:false, data:dat});
                        }
                    });*/
                    var cstr=$("#siexcoord").val() +":"+ $("#sieycoord").val();
                    for (var i in poll2.OGA) {
                        if (cstr==poll2.OGA[i][5]) {
                            var aid=poll2.OGA[i][2];
                            var dat={a: aid,b:1};
                            if (poll2.OGA[i][1]=="Siege") {
                                $(".shAinf").each(function() {
                                    if ($(this).attr("data")==aid) {
                                        $(this).trigger({type:"click",originalEvent:"1"});
                                        setTimeout(function() {
                                            $("#cancAtkIfAlly").trigger({type:"click",originalEvent:"1"});
                                            $("#doneOG").trigger({type:"click",originalEvent:"1"});
                                            $("#outgoingPopUpBox").hide();
                                        },400);
                                    }
                                });
                            } else {
                                $(".shPinf").each(function() {
                                    if ($(this).attr("data")==aid) {
                                        $(this).trigger({type:"click",originalEvent:"1"});
                                        setTimeout(function() {
                                            $("#cancAtkIfAlly").trigger({type:"click",originalEvent:"1"});
                                            $("#doneOG").trigger({type:"click",originalEvent:"1"});
                                            $("#outgoingPopUpBox").hide();
                                        },400);
                                    }
                                });
                            }
                        }
                    }
                },4000);
            }
        });
    });

    function incomings() {
        //  below will give u a variable called totbonus which contains all the possible speed bonuses that can be in game
        var totbonus=[];
        totbonus[0]=0;
        for (var i=1; i<201; i++){
            totbonus[i]=totbonus[i-1]+0.5;
        }
        //  below will give u an array with all possible speeds for instance, now the script runs and it detects a speed by below function
        var navyspeed = [];
        var scoutspeed = [];
        var cavspeed = [];
        var infspeed = [];
        var artspeed = [];
        var senspeed = [];
        var temp;
        for (var i in totbonus) {
            temp=5/(1+totbonus[i]*1.0/100);
            navyspeed[i]=roundToTwo(temp);
            temp=8/(1+totbonus[i]*1.0/100);
            scoutspeed[i]=roundToTwo(temp);
            temp=10/(1+totbonus[i]*1.0/100);
            cavspeed[i]=roundToTwo(temp);
            temp=20/(1+totbonus[i]*1.0/100);
            infspeed[i]=roundToTwo(temp);
            temp=30/(1+totbonus[i]*1.0/100);
            artspeed[i]=roundToTwo(temp);
            temp=40/(1+totbonus[i]*1.0/100);
            senspeed[i]=roundToTwo(temp);
        }
        $("#iaBody tr").each(function() {
            if ($(':nth-child(2)',this).text()=="-") {
                var tid=$(':nth-child(5)',this).children().children().attr("data");
                var sid=$(':nth-child(10)',this).children().attr("data");
                var tx=tid%65536;
                var sx=sid%65536;
                var ty=(tid-tx)/65536;
                var sy=(sid-sx)/65536;
                var tcont=Math.floor(tx/100)+Math.floor(ty/100)*10;
                var scont=Math.floor(sx/100)+Math.floor(sy/100)*10;
                var dist=Math.sqrt((ty-sy)*(ty-sy)+(tx-sx)*(tx-sx));
                var atime=$(':nth-child(6)',this).text();
                var stime=$(':nth-child(11)',this).text();
                var hdiff=atime.substring(0,2)-stime.substring(0,2);
                var mdiff=atime.substring(3,5)-stime.substring(3,5);
                var sdiff=atime.substring(6,8)-stime.substring(6,8);
                var time;
                if (hdiff>=0) {time=60*hdiff;}
                else {time=(24+hdiff)*60;}
                time+=mdiff;
                time+=sdiff/60;
                var ispeed=roundToTwo(time/dist);
                //console.log(time/dist,ispeed,dist,time);
                var nspeed=roundToTwo((time-60)/dist);
                //console.log(cavspeed);
                //console.log(scoutspeed);
// below will return -1 if calculated speed is not found inside the speed arrays and the correct index if it is found within the speed arrays
                var zns = navyspeed.indexOf(nspeed);
                var zss = scoutspeed.indexOf(ispeed);
                var zcs = cavspeed.indexOf(ispeed);
                var zis = infspeed.indexOf(ispeed);
                var zas = artspeed.indexOf(ispeed);
                var zsn = senspeed.indexOf(ispeed);
// below use ispeed and above return values to get the correct incoming troop type
                // big big Credit for Dhruv for improving the incoming identifier!
                if (tcont==scont) {
                    if (ispeed>30) {
                        if(zsn == -1){$(':nth-child(2)',this).text("Tower?/Sen");}
                        else
                        {$(':nth-child(2)',this).text("senator "+totbonus[zsn]+"%");}
                    }
                    if (ispeed>20 && ispeed<=30) {
                        if(zsn == -1 && zas == -1){$(':nth-child(2)',this).text("Tower?/Art/Sen");}
                        if(zsn == -1 && zas != -1){$(':nth-child(2)',this).text("Artillery "+totbonus[zas]+"%");}
                        if(zsn != -1 && zas == -1){$(':nth-child(2)',this).text("Senator "+totbonus[zsn]+"%");}
                        if(zsn != -1 && zas != -1){$(':nth-child(2)',this).text("Art "+totbonus[zas]+"%"+"/"+"Sen "+totbonus[zsn]+"%");}
                    }
                    if (ispeed==20){$(':nth-child(2)',this).text("Inf 0%/Art 50%/Sen 100%");}
                    if (ispeed>=15 && ispeed<20) {
                        if(zis == -1 && zas == -1){$(':nth-child(2)',this).text("Tower?/Inf &above");}
                        if(zis == -1 && zas != -1){$(':nth-child(2)',this).text("Artillery "+totbonus[zas]+"%");}
                        if(zis != -1 && zas == -1){$(':nth-child(2)',this).text("Infantry "+totbonus[zis]+"%");}
                        if(zis != -1 && zas != -1){$(':nth-child(2)',this).text("Inf "+totbonus[zis]+"%"+"/"+"Art "+totbonus[zas]+"%");}
                    }
                    if (ispeed>=10 && ispeed<15) {
                        if(zis == -1 && zcs == -1){$(':nth-child(2)',this).text("Tower?/Cav &above");}
                        if(zis == -1 && zcs != -1){$(':nth-child(2)',this).text("Cav "+totbonus[zcs]+"%");}
                        if(zis != -1 && zcs == -1){$(':nth-child(2)',this).text("Inf "+totbonus[zis]+"%");}
                        if(zis != -1 && zcs != -1){$(':nth-child(2)',this).text("Cav "+totbonus[zcs]+"%"+"/"+"Inf "+totbonus[zis]+"%");}
                    }
                    if (ispeed>8 && ispeed<10) {
                        if(zcs == -1){$(':nth-child(2)',this).text("Tower?/Cav &above");}
                        else
                        {$(':nth-child(2)',this).text("Cav "+totbonus[zcs]+"%");}
                    }
                    if (ispeed>5 && ispeed<=8){
                        if(zss == -1 && zcs == -1){$(':nth-child(2)',this).text("Tower?/Scout &above");}
                        if(zss == -1 && zcs != -1){$(':nth-child(2)',this).text("Cav "+totbonus[zcs]+"%");}
                        if(zss != -1 && zcs == -1){$(':nth-child(2)',this).text("Scout "+totbonus[zss]+"%");}
                        if(zss != -1 && zcs != -1){$(':nth-child(2)',this).text("Scout "+totbonus[zss]+"%"+"/"+"Cav "+totbonus[zcs]+"%");}
                    }
                    if (ispeed==5){$(':nth-child(2)',this).text("Navy 0%/Scout 60%/Cav 100%");}
                    if (ispeed>=4 && ispeed<5) {
                        if(zss == -1 && zns == -1){$(':nth-child(2)',this).text("Tower?/scout &above");}
                        if(zss == -1 && zns != -1){$(':nth-child(2)',this).text("Navy "+totbonus[zns]+"%");}
                        if(zss != -1 && zns == -1){$(':nth-child(2)',this).text("Scout "+totbonus[zss]+"%");}
                        if(zss != -1 && zns != -1){$(':nth-child(2)',this).text("Navy "+totbonus[zns]+"%"+"/"+"Scout "+totbonus[zss]+"%");}
                    }
                    if (ispeed<4){
                        if(zns == -1){$(':nth-child(2)',this).text("Tower?/Navy &above");}
                        else
                        {$(':nth-child(2)',this).text("Navy "+totbonus[zns]+"%");}
                    }
                                   } else if ($(':nth-child(1)',this).html()) {$(':nth-child(2)',this).text("Portal");}
               else {
                   if(zns != -1){$(':nth-child(2)',this).text("Navy "+totbonus[zns]+"%");}
                   else{$(':nth-child(2)',this).text("Tower?/Navy");}
               }
              }
        });
    }
    // getting research and faith info
    $(document).ready(function() {
        jQuery.ajax({url: 'includes/gaLoy.php',type: 'POST',aysnc:false,
                         success: function(data) {
                             var ldata=JSON.parse(data);
                             setloyal(ldata);
                         }
                        });
        var reslvl;
        function setloyal(ldata) {
            //console.log(ldata);
            var faith=0;
            //domdis
            $.each(ldata.t, function(key, value) {
                if (key==1) {
                    $.each(this, function(key, value) {
                        evarafaith+=this.f;
                    });
                }
                if (key==2) {
                    $.each(this, function(key, value) {
                        vexifaith+=this.f;
                    });
                }
                if (key==3) {
                    $.each(this, function(key, value) {
                        domdisfaith+=this.f;
                    });
                }
                if (key==4) {
                    $.each(this, function(key, value) {
                        cyndrosfaith+=this.f;
                    });
                }
                if (key==5) {
                    $.each(this, function(key, value) {
                        meriusfaith+=this.f;
                    });
                }
                if (key==6) {
                    $.each(this, function(key, value) {
                        ylannafaith+=this.f;
                    });
                }
                if (key==7) {
                    $.each(this, function(key, value) {
                        ibriafaith+=this.f;
                    });
                }
                if (key==8) {
                    $.each(this, function(key, value) {
                        naerafaith+=this.f;
                    });
                }
            });
            ylannafaith=Math.min(ylannafaith,100);
            naerafaith=Math.min(naerafaith,100);
            vexifaith=Math.min(vexifaith,100);
            cyndrosfaith=Math.min(cyndrosfaith,100);
            domdisfaith=Math.min(domdisfaith,100);
            ibriafaith=Math.min(ibriafaith,100);
            evarafaith=Math.min(evarafaith,100);
            meriusfaith=Math.min(meriusfaith,100);
            //attack power faith bonuses
            ttres[0]+=Math.floor(0.5*ylannafaith)/100;
            ttres[1]+=Math.floor(0.5*ylannafaith)/100;
            ttres[2]+=Math.floor(0.5*naerafaith)/100;
            ttres[3]+=Math.floor(0.5*naerafaith)/100;
            ttres[4]+=Math.floor(0.5*naerafaith)/100;
            ttres[5]+=Math.floor(0.5*vexifaith)/100;
            ttres[6]+=Math.floor(0.5*vexifaith)/100;
            ttres[7]+=Math.floor(0.5*vexifaith)/100;
            ttres[8]+=Math.floor(0.5*naerafaith)/100;
            ttres[9]+=Math.floor(0.5*naerafaith)/100;
            ttres[10]+=Math.floor(0.5*vexifaith)/100;
            ttres[11]+=Math.floor(0.5*vexifaith)/100;
            ttres[12]+=Math.floor(0.5*cyndrosfaith)/100;
            ttres[13]+=Math.floor(0.5*cyndrosfaith)/100;
            ttres[14]+=Math.floor(0.5*ylannafaith)/100;
            ttres[15]+=Math.floor(0.5*ylannafaith)/100;
            ttres[16]+=Math.floor(0.5*cyndrosfaith)/100;

            //faith travel speed bonuses
            ttspeedres[1]+=0.5*domdisfaith/100;
            ttspeedres[2]+=0.5*ibriafaith/100;
            ttspeedres[3]+=0.5*ibriafaith/100;
            ttspeedres[4]+=0.5*ibriafaith/100;
            ttspeedres[5]+=0.5*ibriafaith/100;
            ttspeedres[6]+=0.5*ibriafaith/100;
            ttspeedres[7]+=0.5*ibriafaith/100;
            ttspeedres[8]+=0.5*ibriafaith/100;
            ttspeedres[9]+=0.5*ibriafaith/100;
            ttspeedres[10]+=0.5*ibriafaith/100;
            ttspeedres[11]+=0.5*ibriafaith/100;
            ttspeedres[12]+=0.5*domdisfaith/100;
            ttspeedres[13]+=0.5*domdisfaith/100;
            ttspeedres[14]+=0.5*domdisfaith/100;
            ttspeedres[15]+=0.5*domdisfaith/100;
            ttspeedres[16]+=0.5*domdisfaith/100;
            ttspeedres[17]+=0.5*evarafaith/100;
            }
        setTimeout(function(){
        $(".resPop").each(function() {
            if($(this).attr('data')==8) { //inf speed
                var ranktext=$(this).text();
                var cmp=new RegExp("complete")
                if (cmp.test(ranktext)) {
                    reslvl=12;
                }
                else var reslvl=Number(ranktext.match(/\d+/gi));
                for (var i in ttspeedres) {
                    if (isinf[i]) {
                ttspeedres[i]+=resbonus[reslvl];
                    }
                }
                ttspeedres[6]+=resbonus[reslvl];
                //console.log(ttres[2]);
            }
            if($(this).attr('data')==9) { //cav speed
                var ranktext=$(this).text();
                var cmp=new RegExp("complete")
                if (cmp.test(ranktext)) {
                    reslvl=12;
                }
                else var reslvl=Number(ranktext.match(/\d+/gi));
                for (var i in ttspeedres) {
                    if (iscav[i]) {                        
                ttspeedres[i]+=resbonus[reslvl];
                    }
                }
                ttspeedres[11]+=resbonus[reslvl];
                //console.log(ttres[2]);
            }
            if($(this).attr('data')==13) { //navy speed
                var ranktext=$(this).text();
                var cmp=new RegExp("complete")
                if (cmp.test(ranktext)) {
                    reslvl=12;
                }
                else var reslvl=Number(ranktext.match(/\d+/gi));
                ttspeedres[14]+=resbonus[reslvl];
                ttspeedres[15]+=resbonus[reslvl];
                ttspeedres[16]+=resbonus[reslvl];
                //console.log(ttres[2]);
            }
            if($(this).attr('data')==14) { //senator speed
                var ranktext=$(this).text();
                var cmp=new RegExp("complete")
                if (cmp.test(ranktext)) {
                    reslvl=12;
                }
                else var reslvl=Number(ranktext.match(/\d+/gi));
                ttspeedres[17]+=resbonus[reslvl];
            }
            if($(this).attr('data')==30) { //rangers
                var ranktext=$(this).text();
                var cmp=new RegExp("complete")
                if (cmp.test(ranktext)) {
                    reslvl=12;
                }
                else var reslvl=Number(ranktext.match(/\d+/gi));
                ttres[2]+=resbonus[reslvl];
                //console.log(ttres[2]);
            }
            if($(this).attr('data')==31) { //triari
                var ranktext=$(this).text();
                var cmp=new RegExp("complete")
                if (cmp.test(ranktext)) {
                    reslvl=12;
                }
                else
                    var reslvl=Number(ranktext.match(/\d+/gi));
                ttres[3]+=resbonus[reslvl];
            }
            if($(this).attr('data')==32) { //priestess
                var ranktext=$(this).text();
                var cmp=new RegExp("complete")
                if (cmp.test(ranktext)) {
                    reslvl=12;
                }
                else
                    var reslvl=Number(ranktext.match(/\d+/gi));
                ttres[4]+=resbonus[reslvl];
            }
            if($(this).attr('data')==33) { //vanqs
                var ranktext=$(this).text();
                var cmp=new RegExp("complete")
                if (cmp.test(ranktext)) {
                    reslvl=12;
                }
                else
                    var reslvl=Number(ranktext.match(/\d+/gi));
                ttres[5]+=resbonus[reslvl];
            }
            if($(this).attr('data')==34) { //sorcs
                var ranktext=$(this).text();
                var cmp=new RegExp("complete")
                if (cmp.test(ranktext)) {
                    reslvl=12;
                }
                else
                    var reslvl=Number(ranktext.match(/\d+/gi));
                ttres[6]+=resbonus[reslvl];
            }
            if($(this).attr('data')==35) { //arbs
                var ranktext=$(this).text();
                var cmp=new RegExp("complete")
                if (cmp.test(ranktext)) {
                    reslvl=12;
                }
                else
                    var reslvl=Number(ranktext.match(/\d+/gi));
                ttres[8]+=resbonus[reslvl];
            }
            if($(this).attr('data')==36) { //praetors
                var ranktext=$(this).text();
                var cmp=new RegExp("complete")
                if (cmp.test(ranktext)) {
                    reslvl=12;
                }
                else
                    var reslvl=Number(ranktext.match(/\d+/gi));
                ttres[9]+=resbonus[reslvl];
            }
            if($(this).attr('data')==37) { //horseman
                var ranktext=$(this).text();
                var cmp=new RegExp("complete")
                if (cmp.test(ranktext)) {
                    reslvl=12;
                }
                else
                    var reslvl=Number(ranktext.match(/\d+/gi));
                ttres[10]+=resbonus[reslvl];
            }
            if($(this).attr('data')==38) { //druid
                var ranktext=$(this).text();
                var cmp=new RegExp("complete")
                if (cmp.test(ranktext)) {
                    reslvl=12;
                }
                else
                    var reslvl=Number(ranktext.match(/\d+/gi));
                ttres[11]+=resbonus[reslvl];
            }
            if($(this).attr('data')==43) { //stinger
                var ranktext=$(this).text();
                var cmp=new RegExp("complete")
                if (cmp.test(ranktext)) {
                    reslvl=12;
                }
                else
                    var reslvl=Number(ranktext.match(/\d+/gi));
                ttres[15]+=resbonus[reslvl];
            }
            if($(this).attr('data')==44) { //galley
                var ranktext=$(this).text();
                var cmp=new RegExp("complete")
                if (cmp.test(ranktext)) {
                    reslvl=12;
                }
                else
                    var reslvl=Number(ranktext.match(/\d+/gi));
                ttres[14]+=resbonus[reslvl];
            }
            if($(this).attr('data')==45) { //warships
                var ranktext=$(this).text();
                var cmp=new RegExp("complete")
                if (cmp.test(ranktext)) {
                    reslvl=12;
                }
                else
                    var reslvl=Number(ranktext.match(/\d+/gi));
                ttres[16]+=resbonus[reslvl];
            }
            if($(this).attr('data')==46) { //scouts
                var ranktext=$(this).text();
                //console.log(ranktext);
                var cmp=new RegExp("complete")
                if (cmp.test(ranktext)) {
                    reslvl=12;
                }
                else
                    var reslvl=Number(ranktext.match(/\d+/gi));
                ttres[7]+=resbonus[reslvl];
            }
        });
        },5000);
        //console.log(ttres,ttspeedres);
    });

    document.getElementById('raidDungGo').onclick = function() {
            //createTable();
        setTimeout(function(){setbossloot();}, 1000);
        }; 

    function createTable() {
        $('#cfunkydiv').remove();
        var ptworow=$("#Progress").html();
        if (ptworow==0)
        {ptworow=100;}
  var plevorow=$("#dunglevelregion").html();
  var ptropneed = Math.ceil(loot[plevorow]*((100-ptworow)*0.008+1)/10);
        var outtable="<div id='cfunkydiv' style='width:500px;height:330px;background-color: #E2CBAC;-moz-border-radius: 10px;-webkit-border-radius: 10px;border-radius: 10px;border: 4px ridge #DAA520;position:absolute;right:10px;top:100px; z-index:1000000;'><div class=\"popUpBar\"> <span class=\"ppspan\">Suggested Raiding Numbers - Caver progress "+ptworow+"%</span> <button id=\"cfunkyX\" onclick=\"$('#cfunkydiv').remove();\" class=\"xbutton greenb\"><div id=\"xbuttondiv\"><div><div id=\"centxbuttondiv\"></div></div></div></button></div><div class=\"popUpWindow\">";
        outtable+="<table><thead><th>Lvl</th><th>Estimated Loot</th><th>Vanqs/Rangers<br>druids</th><th>Sorcs</th><th>Praetors</th><th>Arbs/Horses</th></thead>";
        outtable+="<tbody><tr><td>1</td><td>400</td><td>"+Math.ceil(loot[1]*((100-ptworow)*0.008+1)/10)+"</td><td>"+Math.ceil(loot[1]*((100-ptworow)*0.008+1)/5)+"</td><td>"+Math.ceil(loot[1]*((100-ptworow)*0.008+1)/20)+"</td><td>"+Math.ceil(loot[1]*((100-ptworow)*0.008+1)/15)+"</td></tr>";
        outtable+="<tr><td>2</td><td>1000</td><td>"+Math.ceil(loot[2]*((100-ptworow)*0.008+1)/10)+"</td><td>"+Math.ceil(loot[2]*((100-ptworow)*0.008+1)/5)+"</td><td>"+Math.ceil(loot[2]*((100-ptworow)*0.008+1)/20)+"</td><td>"+Math.ceil(loot[2]*((100-ptworow)*0.008+1)/15)+"</td></tr>";
        outtable+="<tr><td>3</td><td>4500</td><td>"+Math.ceil(loot[3]*((100-ptworow)*0.008+1)/10)+"</td><td>"+Math.ceil(loot[3]*((100-ptworow)*0.008+1)/5)+"</td><td>"+Math.ceil(loot[3]*((100-ptworow)*0.008+1)/20)+"</td><td>"+Math.ceil(loot[3]*((100-ptworow)*0.008+1)/15)+"</td></tr>";
        outtable+="<tr><td>4</td><td>15000</td><td>"+Math.ceil(loot[4]*((100-ptworow)*0.008+1)/10)+"</td><td>"+Math.ceil(loot[4]*((100-ptworow)*0.008+1)/5)+"</td><td>"+Math.ceil(loot[4]*((100-ptworow)*0.008+1)/20)+"</td><td>"+Math.ceil(loot[4]*((100-ptworow)*0.008+1)/15)+"</td></tr>";
        outtable+="<tr><td>5</td><td>33000</td><td>"+Math.ceil(loot[5]*((100-ptworow)*0.008+1)/10)+"</td><td>"+Math.ceil(loot[5]*((100-ptworow)*0.008+1)/5)+"</td><td>"+Math.ceil(loot[5]*((100-ptworow)*0.008+1)/20)+"</td><td>"+Math.ceil(loot[5]*((100-ptworow)*0.008+1)/15)+"</td></tr>";
        outtable+="<tr><td>6</td><td>60000</td><td>"+Math.ceil(loot[6]*((100-ptworow)*0.008+1)/10)+"</td><td>"+Math.ceil(loot[6]*((100-ptworow)*0.008+1)/5)+"</td><td>"+Math.ceil(loot[6]*((100-ptworow)*0.008+1)/20)+"</td><td>"+Math.ceil(loot[6]*((100-ptworow)*0.008+1)/15)+"</td></tr>";
        outtable+="<tr><td>7</td><td>120000</td><td>"+Math.ceil(loot[7]*((100-ptworow)*0.008+1)/10)+"</td><td>"+Math.ceil(loot[7]*((100-ptworow)*0.008+1)/5)+"</td><td>"+Math.ceil(loot[7]*((100-ptworow)*0.008+1)/20)+"</td><td>"+Math.ceil(loot[7]*((100-ptworow)*0.008+1)/15)+"</td></tr>";
        outtable+="<tr><td>8</td><td>201000</td><td>"+Math.ceil(loot[8]*((100-ptworow)*0.008+1)/10)+"</td><td>"+Math.ceil(loot[8]*((100-ptworow)*0.008+1)/5)+"</td><td>"+Math.ceil(loot[8]*((100-ptworow)*0.008+1)/20)+"</td><td>"+Math.ceil(loot[8]*((100-ptworow)*0.008+1)/15)+"</td></tr>";
        outtable+="<tr><td>9</td><td>300000</td><td>"+Math.ceil(loot[9]*((100-ptworow)*0.008+1)/10)+"</td><td>"+Math.ceil(loot[9]*((100-ptworow)*0.008+1)/5)+"</td><td>"+Math.ceil(loot[9]*((100-ptworow)*0.008+1)/20)+"</td><td>"+Math.ceil(loot[9]*((100-ptworow)*0.008+1)/15)+"</td></tr>";
        outtable+="<tr><td>10</td><td>446000</td><td>"+Math.ceil(loot[10]*((100-ptworow)*0.008+1)/10)+"</td><td>"+Math.ceil(loot[10]*((100-ptworow)*0.008+1)/5)+"</td><td>"+Math.ceil(loot[10]*((100-ptworow)*0.008+1)/20)+"</td><td>"+Math.ceil(loot[10]*((100-ptworow)*0.008+1)/15)+"</td></tr>";
        outtable+="</tbody></table><div><button id='raidboxopt' class='regButton greenb' style='width:160px; margin: 1%;' >Dont show this again </button></div>";
        outtable+="</div></div>";
        $( "body" ).append(outtable);
        $( "#cfunkydiv" ).draggable({ handle: ".popUpBar" , containment: "window", scroll: false});
        $("#raidboxopt").click(function() {
            localStorage.setItem("raidbox","1");
            var raidboxback="<button class='regButton greenb' id='raidboxb' style='width:120px; margin-left: 2%;'>Return Raiding Box</button>";
            $("#squaredung td").find(".squarePlayerInfo").before(raidboxback);
            $("#raidboxb").click(function() {
                localStorage.setItem("raidbox","0");
                $("#raidboxb").remove();
            });
        });
        if (localStorage.getItem("raidbox")==1) {
            $('#cfunkydiv').remove();
        }
    }
    function setbossloot() {
        var ttm=[0];
        var ttc=0;
        var bosslvl=$("#dunglevelregion").html();
        var bosstype=$("#dungtypespot").html();
        var tnumb=[0];
        $("#raidingTable tr").each(function() {
            var temp=$(this).find("td:nth-child(3)").text();
            var n = temp.search("/");
            temp=temp.substring(0,n);
            temp=temp.replace(",","");
            var troops=Number(temp);
            //console.log(troops);
            var temp1=$(this).attr('id');
            var tt=Number(temp1.match(/\d+/gi));
            //console.log(temp1);
            if (tt!==7) {
                if (troops>0) {
                    ttc+=1;
                    ttm[ttc-1]=tt;
                    tnumb[ttc-1]=troops;
                }
            }
        });

            if (bosstype=="Triton") {
                for (i=0; i<ttc+1; i++) {
                    $('#cfunkydiv').remove();
                    if (ttm[i]>13) {
                        if (isart[ttm[i]]) {
                            var amount=Math.ceil(bossdefw[bosslvl-1]*4/(ttres[ttm[i]]*ttattack[ttm[i]]));
                            amount=Math.max(amount,bossmts[bosslvl-1]/ttts[ttm[i]]);
                            if (amount<=tnumb[i]) {
                                $('#raidIP'+ttm[i]).val(amount);
                            } else {
                                message="Error, you need at least " + amount + " " + ttname[ttm[i]]+"!";
                                errorgo(message);
                            }
                        } else {
                            var amount=Math.ceil(bossdef[bosslvl-1]*4/(ttres[ttm[i]]*ttattack[ttm[i]]));
                            amount=Math.max(amount,bossmts[bosslvl-1]/ttts[ttm[i]]);
                            if (amount<=tnumb[i]) {
                                $('#raidIP'+ttm[i]).val(amount);
                            } else {
                                message="Error, you need at least " + amount + " " + ttname[ttm[i]]+"!";
                                errorgo(message);
                            }
                        }
                    }
                }
            }
                    else if (bosstype=="Cyclops") {
                        for (i=0; i<ttc+1; i++) {
                            $('#cfunkydiv').remove();
                            if (ttm[i]<13) {
                                if (iscav[ttm[i]]) {
                                    var amount=Math.ceil(bossdefw[bosslvl-1]*4/(ttres[ttm[i]]*ttattack[ttm[i]]));
                                    amount=Math.max(amount,bossmts[bosslvl-1]/ttts[ttm[i]]);
                                    if (amount<=tnumb[i]) {
                                        $('#raidIP'+ttm[i]).val(amount);
                                    } else {
                                        message="Error, you need at least " + amount + " " + ttname[ttm[i]]+"!";
                                        errorgo(message);
                                    }
                                } else {
                                    var amount=Math.ceil(bossdef[bosslvl-1]*4/(ttres[ttm[i]]*ttattack[ttm[i]]));
                                    amount=Math.max(amount,bossmts[bosslvl-1]/ttts[ttm[i]]);
                                    if (amount<=tnumb[i]) {
                                        $('#raidIP'+ttm[i]).val(amount);
                                    } else {
                                        message="Error, you need at least " + amount + " " + ttname[ttm[i]]+"!";
                                        errorgo(message);
                                    }
                                }
                            }
                        }
                    }
                    else if (bosstype=="Andar's Colosseum Challenge") {
                        for (i=0; i<ttc+1; i++) {
                            $('#cfunkydiv').remove();
                            if (ttm[i]<13) {
                                if (iscav[ttm[i]]) {
                                    var amount=Math.ceil(bossdefw[bosslvl-1]*4/(ttres[ttm[i]]*ttattack[ttm[i]]));
                                    amount=Math.max(amount,bossmts[bosslvl-1]/ttts[ttm[i]]);
                                    if (amount<=tnumb[i]) {
                                        $('#raidIP'+ttm[i]).val(amount);
                                    } else {
                                        message="Error, you need at least " + amount + " " + ttname[ttm[i]]+"!";
                                        errorgo(message);
                                    }
                                } else {
                                    var amount=Math.ceil(bossdef[bosslvl-1]*4/(ttres[ttm[i]]*ttattack[ttm[i]]));
                                    amount=Math.max(amount,bossmts[bosslvl-1]/ttts[ttm[i]]);
                                    if (amount<=tnumb[i]) {
                                        $('#raidIP'+ttm[i]).val(amount);
                                    } else {
                                        message="Error, you need at least " + amount + " " + ttname[ttm[i]]+"!";
                                        errorgo(message);
                                    }
                                }
                            }
                        }
                    }
                    else if (bosstype=="Dragon") {
                        for (i=0; i<ttc+1; i++) {
                            $('#cfunkydiv').remove();
                            if (ttm[i]<13) {
                                if (isinf[ttm[i]]) {
                                    var amount=Math.ceil(bossdefw[bosslvl-1]*4/(ttres[ttm[i]]*ttattack[ttm[i]]));
                                    amount=Math.max(amount,bossmts[bosslvl-1]/ttts[ttm[i]]);
                                    if (amount<=tnumb[i]) {
                                        $('#raidIP'+ttm[i]).val(amount);
                                    } else {
                                        message="Error, you need at least " + amount + " " + ttname[ttm[i]]+"!";
                                        errorgo(message);
                                    }
                                } else {
                                    var amount=Math.ceil(bossdef[bosslvl-1]*4/(ttres[ttm[i]]*ttattack[ttm[i]]));
                                    amount=Math.max(amount,bossmts[bosslvl-1]/ttts[ttm[i]]);
                                    if (amount<=tnumb[i]) {
                                        $('#raidIP'+ttm[i]).val(amount);
                                    } else {
                                        message="Error, you need at least " + amount + " " + ttname[ttm[i]]+"!";
                                        errorgo(message);
                                    }
                                }
                            }
                        }
                    }
                    else if (bosstype=="Romulus and Remus") {
                        for (i=0; i<ttc+1; i++) {
                            $('#cfunkydiv').remove();
                            if (ttm[i]<13) {
                                if (isinf[ttm[i]]) {
                                    var amount=Math.ceil(bossdefw[bosslvl-1]*4/(ttres[ttm[i]]*ttattack[ttm[i]]));
                                    amount=Math.max(amount,bossmts[bosslvl-1]/ttts[ttm[i]]);
                                    if (amount<=tnumb[i]) {
                                        $('#raidIP'+ttm[i]).val(amount);
                                    } else {
                                        message="Error, you need at least " + amount + " " + ttname[ttm[i]]+"!";
                                        errorgo(message);
                                    }
                                } else {
                                    var amount=Math.ceil(bossdef[bosslvl-1]*4/(ttres[ttm[i]]*ttattack[ttm[i]]));
                                    amount=Math.max(amount,bossmts[bosslvl-1]/ttts[ttm[i]]);
                                    if (amount<=tnumb[i]) {
                                        $('#raidIP'+ttm[i]).val(amount);
                                    } else {
                                        message="Error, you need at least " + amount + " " + ttname[ttm[i]]+"!";
                                        errorgo(message);
                                    }
                                }
                            }
                        }
                    }
                    else if (bosstype=="Gorgon") {
                        for (i=0; i<ttc+1; i++) {
                            $('#cfunkydiv').remove();
                            if (ttm[i]<13) {
                                if (ismgc[ttm[i]]) {
                                    var amount=Math.ceil(bossdefw[bosslvl-1]*4/(ttres[ttm[i]]*ttattack[ttm[i]]));
                                    amount=Math.max(amount,bossmts[bosslvl-1]/ttts[ttm[i]]);
                                    if (amount<=tnumb[i]) {
                                        $('#raidIP'+ttm[i]).val(amount);
                                    } else {
                                        message="Error, you need at least " + amount + " " + ttname[ttm[i]]+"!";
                                        errorgo(message);
                                    }
                                } else {
                                    var amount=Math.ceil(bossdef[bosslvl-1]*4/(ttres[ttm[i]]*ttattack[ttm[i]]));
                                    amount=Math.max(amount,bossmts[bosslvl-1]/ttts[ttm[i]]);
                                    if (amount<=tnumb[i]) {
                                        $('#raidIP'+ttm[i]).val(amount);
                                    } else {
                                        message="Error, you need at least " + amount + " " + ttname[ttm[i]]+"!";
                                        errorgo(message);
                                    }
                                }
                            }
                        }
                    }
                    else if (bosstype=="GM Gordy") {
                        for (i=0; i<ttc+1; i++) {
                            $('#cfunkydiv').remove();
                            if (ttm[i]<13) {
                                if (ismgc[ttm[i]]) {
                                    var amount=Math.ceil(bossdefw[bosslvl-1]*4/(ttres[ttm[i]]*ttattack[ttm[i]]));
                                    amount=Math.max(amount,bossmts[bosslvl-1]/ttts[ttm[i]]);
                                    if (amount<tnumb[i]) {
                                        $('#raidIP'+ttm[i]).val(amount);
                                    } else {
                                        message="Error, you need at least " + amount + " " + ttname[ttm[i]]+"!";
                                        errorgo(message);
                                    }
                                } else {
                                    var amount=Math.ceil(bossdef[bosslvl-1]*4/(ttres[ttm[i]]*ttattack[ttm[i]]));
                                    amount=Math.max(amount,bossmts[bosslvl-1]/ttts[ttm[i]]);
                                    if (amount<=tnumb[i]) {
                                        $('#raidIP'+ttm[i]).val(amount);
                                    } else {
                                        message="Error, you need at least " + amount + " " + ttname[ttm[i]]+"!";
                                        errorgo(message);
                                    }
                                }
                            }
                        }
                    }
                    else { createTable();}
    }


    function getDugRows() {
        $('#dungloctab th:contains("Distance")').trigger({type:"click",originalEvent:"1"});
        $('#dungloctab th:contains("Distance")').trigger({type:"click",originalEvent:"1"});
        //$('#maxcavcomp').remove();
        var maxcomp='<select id="maxcavcomp" style="font-size: 10px !important;width:27%;height:27px;" class="greensel"><option value="75">Maximum completion</option>';
        maxcomp+='<option value="100">100%</option><option value="85">85%</option><option value="70">70%</option><option value="55">55%</option><option value="40">40%</option><option value="25">25%</option><option value="10">10%</option><option value="75">75%(default)</option>';
        $('#secwarCspan').width('35%').css("font-size", "9px");
        $('#raidLTinfogselWC').width('20%');
        if (!$('#maxcavcomp').length) {
            $('#raidLTinfogselWC').before(maxcomp);
        }
        $("#dungloctab tr").each(function() {
            var buttont=$(this).find( "button");
            var buttonid=buttont.attr('id');
            var tworow=$(this).find( "td:nth-child(2)").text();
            var threerow=$(this).find( "td:nth-child(3)").text();
            if(buttonid) {
                var reference=buttonid.substring(8);
                buttont.addClass('Cfunk');
                buttont.attr('dd',reference);
                buttont.attr('dt',tworow);
                buttont.attr('dtt',threerow);
            }

        $(buttont).click(function() {
            var count=Number($('.splitRaid').children('option').length)-1;
            var troopshome=[0];
            var ttc=0;
            var tt=["non"];
            var ttm=[0];
            var butid=$(this).attr('dd');
            var dungtext=$(this).attr('dt');
            var progress=$(this).attr('dtt');
            var temp=dungtext.match(/\d+/gi);
            numbs[0]=Number(temp);
            temp=progress.match(/\d+/gi);
            numbs[1]=Math.min(temp,$('#maxcavcomp').val());
            //console.log(numbs[1]);
            setTimeout(function(){

                $(".tninput").each(function() {
                    ttc+=1;
                    var trpinpid=$(this).attr('id');
                    //tt[ttc-1]=trpinpid;
                    ttm[ttc-1]=Number(trpinpid.match(/\d+/gi));
                });
                //console.log(ttc);
                //console.log(ttm);
                if(ttc==1) {
                    troopshome[0]=$('#rval'+ttm[0]).val();
                    //console.log(troopshome);
                    numbs[2]=Math.ceil(loot[numbs[0]]*((100-numbs[1])*0.008+1)/ttloot[ttm[0]]);
                    if(Number(troopshome)>=numbs[2]) {
                        var previ=false;
                        for (var i=2; i<=count;i++) {
                            if (troopshome[0]/(i*numbs[2])<=0.90) {
                                if (i-1>1) {
                                    if (previ){
                                        $('#WCcomcount').val(i-1).change();
                                        $('#wcraidunti').val(1).change();
                                    }
                                    else {
                                        $('#rval'+ttm[0]).val(numbs[2]);
                                        $('#WCcomcount').val(i-1);
                                        $('#wcraidunti').val(1).change();
                                    }
                                } else {
                                    $('#rval'+ttm[0]).val(numbs[2]);
                                    $('#WCcomcount').val(i-1);
                                    $('#wcraidunti').val(1).change();
                                }
                                break;
                            }
                            else if(i==count) {
                                        $('#WCcomcount').val(i).change();
                                        $('#wcraidunti').val(1).change();
                                    }
                            if (troopshome[0]/(i*numbs[2])>=1.1) {
                                previ=false;
                                }
                            else {
                                previ=true;
                                }
                        }
                    } else {
                        message="Error, you need at least " + numbs[2] + " " + ttname[ttm[0]]+"!";
                        errorgo(message);
                        if (troopshome[0]/(numbs[2])<=0.90) {
                            $('#wcraidunti').val(3).change();
                        }
                    }
                }
                if(ttc==2) {
                    //console.log(2);
                    if (ttm[1]==14) {
                        troopshome[0]=$('#rval'+ttm[0]).val();
                        numbs[2]=Math.ceil(loot[numbs[0]]*((100-numbs[1])*0.008+1)/ttloot[ttm[0]]);
                        if(Number(troopshome)>=numbs[2]) {
                            var previ=false;
                        for (var i=2; i<=count;i++) {
                            if (troopshome[0]/(i*numbs[2])<=0.90) {
                                if (i-1>1) {
                                    if (previ){
                                        $('#WCcomcount').val(i-1).change();
                                        $('#wcraidunti').val(1).change();
                                    }
                                    else {
                                        $('#rval'+ttm[0]).val(numbs[2]);
                                        $('#WCcomcount').val(i-1);
                                        $('#wcraidunti').val(1).change();
                                    }
                                } else {
                                    $('#rval'+ttm[0]).val(numbs[2]);
                                    $('#WCcomcount').val(i-1);
                                    $('#wcraidunti').val(1).change();
                                }
                                break;
                            }
                            else if(i==count) {
                                        $('#WCcomcount').val(i).change();
                                        $('#wcraidunti').val(1).change();
                                    }
                            if (troopshome[0]/(i*numbs[2])>=1.1) {
                                previ=false;
                                }
                            else {
                                previ=true;
                            }
                        }
                        } else {
                            message="Error, you need at least " + numbs[2] + " " + ttname[ttm[0]]+"!";
                            errorgo(message);
                            if (troopshome[0]/(numbs[2])<=0.90) {
                            $('#wcraidunti').val(3).change();
                        }
                        }
                        $('#rval'+ttm[1]).val(0);
                    } else {
                        //console.log(3);
                        troopshome[0]=$('#rval'+ttm[0]).val();
                        troopshome[1]=$('#rval'+ttm[1]).val();
                        var ratio=[troopshome[0]*ttloot[ttm[0]]/(troopshome[1]*ttloot[ttm[1]]+troopshome[0]*ttloot[ttm[0]]),troopshome[1]*ttloot[ttm[1]]/(troopshome[1]*ttloot[ttm[1]]+troopshome[0]*ttloot[ttm[0]])];
                        //console.log(ratio);
                        numbs[2]=Math.ceil(ratio[0]*loot[numbs[0]]*((100-numbs[1])*0.008+1)/ttloot[ttm[0]]);
                        //console.log(numbs[2]);
                        if (troopshome[0]>0) {
                            //console.log(4);
                            if(Number(troopshome[0])>=numbs[2]) {
                                var previ=false;
                                for (var i=2; i<=count;i++) {
                                    //console.log(i);
                                    //console.log(troopshome[0]);
                                    //console.log(troopshome[0]/(i*numbs[2]));
                                    if (troopshome[0]/(i*numbs[2])<=0.9) {
                                        if (i-1>1) {
                                            if (previ){
                                                $('#WCcomcount').val(i-1).change();

     $('#wcraidunti').val(1).change();
                                            }
                                            else {
                                                $('#rval'+ttm[0]).val(numbs[2]);
                                                numbs[2]=Math.ceil(ratio[1]*loot[numbs[0]]*((100-numbs[1])*0.008+1)/ttloot[ttm[1]]);

  $('#rval'+ttm[1]).val(numbs[2]);
                                                $('#WCcomcount').val(i-1);
                                                $('#wcraidunti').val(1).change();
                                            }
                                        } else {
                                            $('#rval'+ttm[0]).val(numbs[2]);
                                            numbs[2]=Math.ceil(ratio[1]*loot[numbs[0]]*((100-numbs[1])*0.008+1)/ttloot[ttm[1]]);
                                            $('#rval'+ttm[1]).val(numbs[2]);
                                            $('#WCcomcount').val(i-1);
                                            $('#wcraidunti').val(1).change();
                                        }
                                        break;
                                    }
                                    else if(i==count) {
                                        $('#WCcomcount').val(i).change();
                                        $('#wcraidunti').val(1).change();
                                    }
                                    if (troopshome[0]/(i*numbs[2])>=1.1) {
                                        previ=false;
                                    }
                                    else {
                                        previ=true;
                                    }
                                }
                            }  else {
                                message="Error, you need at least " + numbs[2] + " " + ttname[ttm[0]]+"!";
                                errorgo(message);
                                if (troopshome[0]/(numbs[2])<=0.90) {
                                    $('#wcraidunti').val(3).change();
                                }
                            }
                        }
                        else {
                            numbs[2]=Math.ceil(ratio[1]*loot[numbs[0]]*((100-numbs[1])*0.008+1)/ttloot[ttm[1]]);
                            if(Number(troopshome[1])>=numbs[2]) {
                                var previ=false;
                                for (var i=2; i<=count;i++) {
                                    if (troopshome[1]/(i*numbs[2])<=0.90) {
                                        if (i-1>1) {
                                            if (previ){
                                                $('#WCcomcount').val(i-1).change();
                                                $('#wcraidunti').val(1).change();
                                            }
                                            else {
                                                $('#rval'+ttm[1]).val(numbs[2]);
                                                $('#WCcomcount').val(i-1);
                                                $('#wcraidunti').val(1).change();
                                            }
                                        } else {
                                            $('#rval'+ttm[1]).val(numbs[2]);
                                            $('#WCcomcount').val(i-1);
                                            $('#wcraidunti').val(1).change();
                                        }
                                        break;
                                    }
                                    else if(i==count) {
                                        $('#WCcomcount').val(i).change();
                                        $('#wcraidunti').val(1).change();
                                    }
                                    if (troopshome[1]/(i*numbs[2])>=1.1) {
                                        previ=false;
                                    }
                                    else {
                                        previ=true;
                                    }
                                }
                            } else {
                                message="Error, you need at least " + numbs[2] + " " + ttname[ttm[1]]+"!";
                                errorgo(message);
                                if (troopshome[1]/(numbs[2])<=0.90) {
                                    $('#wcraidunti').val(3).change();
                                }
                            }
                        }
                    }
                }
                if (ttc==3) {
                    if (ttm[2]==14) {
                        troopshome[0]=$('#rval'+ttm[0]).val();
                        troopshome[1]=$('#rval'+ttm[1]).val();
                        var ratio=[troopshome[0]*ttloot[ttm[0]]/(troopshome[1]*ttloot[ttm[1]]+troopshome[0]*ttloot[ttm[0]]),troopshome[1]*ttloot[ttm[1]]/(troopshome[1]*ttloot[ttm[1]]+troopshome[0]*ttloot[ttm[0]])];
                        //console.log(ratio);
                        numbs[2]=Math.ceil(ratio[0]*loot[numbs[0]]*((100-numbs[1])*0.008+1)/ttloot[ttm[0]]);
                        //console.log(numbs[2]);
                        if (troopshome[0]>0) {
                            if(Number(troopshome[0])>=numbs[2]) {
                                var previ=false;
                                for (var i=2; i<=count;i++) {
                                    if (troopshome[0]/(i*numbs[2])<=0.90) {
                                        if (i-1>1) {
                                            if (previ){
                                                $('#WCcomcount').val(i-1).change();
                                                $('#wcraidunti').val(1).change();
                                            }
                                            else {
                                                $('#rval'+ttm[0]).val(numbs[2]);
                                                numbs[2]=Math.ceil(ratio[1]*loot[numbs[0]]*((100-numbs[1])*0.008+1)/ttloot[ttm[1]]);
                                                $('#rval'+ttm[1]).val(numbs[2]);
                                                $('#WCcomcount').val(i-1);
                                                $('#wcraidunti').val(1).change();
                                            }
                                        } else {
                                            $('#rval'+ttm[0]).val(numbs[2]);
                                            numbs[2]=Math.ceil(ratio[1]*loot[numbs[0]]*((100-numbs[1])*0.008+1)/ttloot[ttm[1]]);
                                            $('#rval'+ttm[1]).val(numbs[2]);
                                            $('#WCcomcount').val(i-1);
                                            $('#wcraidunti').val(1).change();
                                        }
                                        break;
                                    }
                                    else if(i==count) {
                                        $('#WCcomcount').val(i).change();
                                        $('#wcraidunti').val(1).change();
                                    }
                                    if (troopshome[0]/(i*numbs[2])>=1.1) {
                                        previ=false;
                                    }
                                    else {
                                        previ=true;
                                    }
                                }
                            }  else {
                                message="Error, you need at least " + numbs[2] + " " + ttname[ttm[0]]+"!";
                                errorgo(message);
                                if (troopshome[0]/(numbs[2])<=0.90) {
                                    $('#wcraidunti').val(3).change();
                                }
                            }
                        }
                        else {
                            numbs[2]=Math.ceil(ratio[1]*loot[numbs[0]]*((100-numbs[1])*0.008+1)/ttloot[ttm[1]]);
                            if(Number(troopshome[1])>=numbs[2]) {
                                var previ=false;
                                for (var i=2; i<=count;i++) {
                                    if (troopshome[1]/(i*numbs[2])<=0.90) {
                                        if (i-1>1) {
                                            if (previ){
                                                $('#WCcomcount').val(i-1).change();
                                                $('#wcraidunti').val(1).change();
                                            }
                                            else {
                                                $('#rval'+ttm[1]).val(numbs[2]);
                                                $('#WCcomcount').val(i-1);
                                                $('#wcraidunti').val(1).change();
                                            }
                                        } else {
                                            $('#rval'+ttm[1]).val(numbs[2]);
                                            $('#WCcomcount').val(i-1);
                                            $('#wcraidunti').val(1).change();
                                        }
                                        break;
                                    }
                                    else if(i==count) {
                                        $('#WCcomcount').val(i).change();
                                        $('#wcraidunti').val(1).change();
                                    }
                                    if (troopshome[1]/(i*numbs[2])>=1.1) {
                                        previ=false;
                                    }
                                    else {
                                        previ=true;
                                    }
                                }
                            } else {
                                message="Error, you need at least " + numbs[2] + " " + ttname[ttm[1]]+"!";
                                errorgo(message);
                                if (troopshome[1]/(numbs[2])<=0.90) {
                                    $('#wcraidunti').val(3).change();
                                }
                            }
                        }
                        $('#rval'+ttm[2]).val(0);
                    }
                    else {
                        troopshome[0]=$('#rval'+ttm[0]).val();
                        troopshome[1]=$('#rval'+ttm[1]).val();
                        troopshome[2]=$('#rval'+ttm[2]).val();
                        var ratio=[troopshome[0]*ttloot[ttm[0]]/(troopshome[2]*ttloot[ttm[2]]+troopshome[1]*ttloot[ttm[1]]+troopshome[0]*ttloot[ttm[0]])];
                        ratio[1]=troopshome[1]*ttloot[ttm[1]]/(troopshome[2]*ttloot[ttm[2]]+troopshome[1]*ttloot[ttm[1]]+troopshome[0]*ttloot[ttm[0]]);
                        ratio[2]=troopshome[2]*ttloot[ttm[2]]/(troopshome[2]*ttloot[ttm[2]]+troopshome[1]*ttloot[ttm[1]]+troopshome[0]*ttloot[ttm[0]]);
                        //console.log(ratio);
                        if (troopshome[0]>0) {
                            numbs[2]=Math.ceil(ratio[0]*loot[numbs[0]]*((100-numbs[1])*0.008+1)/ttloot[ttm[0]]);
                            //console.log(numbs[2]);
                            if(Number(troopshome[0])>=numbs[2]) {
                                var previ=false;
                                for (var i=2; i<=count;i++) {
                                    if (troopshome[0]/(i*numbs[2])<=0.90) {
                                        if (i-1>1) {
                                            if (previ){
                                                $('#WCcomcount').val(i-1).change();
                                                $('#wcraidunti').val(1).change();
                                            }
                                            else {
                                                $('#rval'+ttm[0]).val(numbs[2]);
                                                numbs[2]=Math.ceil(ratio[1]*loot[numbs[0]]*((100-numbs[1])*0.008+1)/ttloot[ttm[1]]);
                                                $('#rval'+ttm[1]).val(numbs[2]);
                                                numbs[2]=Math.ceil(ratio[2]*loot[numbs[0]]*((100-numbs[1])*0.008+1)/ttloot[ttm[2]]);
                                                $('#rval'+ttm[2]).val(numbs[2]);
                                                $('#WCcomcount').val(i-1);
                                                $('#wcraidunti').val(1).change();
                                            }
                                        } else {
                                            $('#rval'+ttm[0]).val(numbs[2]);
                                            numbs[2]=Math.ceil(ratio[1]*loot[numbs[0]]*((100-numbs[1])*0.008+1)/ttloot[ttm[1]]);
                                            $('#rval'+ttm[1]).val(numbs[2]);
                                            numbs[2]=Math.ceil(ratio[2]*loot[numbs[0]]*((100-numbs[1])*0.008+1)/ttloot[ttm[2]]);
                                                $('#rval'+ttm[2]).val(numbs[2]);
                                            $('#WCcomcount').val(i-1);
                                            $('#wcraidunti').val(1).change();
                                        }
                                        break;
                                    }
                                    else if(i==count) {
                                        $('#WCcomcount').val(i).change();
                                        $('#wcraidunti').val(1).change();
                                    }
                                    if (troopshome[0]/(i*numbs[2])>=1.1) {
                                        previ=false;
                                    }
                                    else {
                                        previ=true;
                                    }
                                }
                            } else {
                                message="Error, you need at least " + numbs[2] + " " + ttname[ttm[0]]+"!";
                                errorgo(message);
                                if (troopshome[0]/(numbs[2])<=0.90) {
                                    $('#wcraidunti').val(3).change();
                                }
                            }
                        } else if(troopshome[1]>0) {
                            numbs[2]=Math.ceil(ratio[1]*loot[numbs[0]]*((100-numbs[1])*0.008+1)/ttloot[ttm[1]]);
                            if(Number(troopshome[1])>=numbs[2]) {
                                var previ=false;
                                for (var i=2; i<=count;i++) {
                                    if (troopshome[1]/(i*numbs[2])<=0.90) {
                                        if (i-1>1) {
                                            if (previ){
                                                $('#WCcomcount').val(i-1).change();
                                                $('#wcraidunti').val(1).change();
                                            }
                                            else {
                                                $('#rval'+ttm[1]).val(numbs[2]);
                                                numbs[2]=Math.ceil(ratio[2]*loot[numbs[0]]*((100-numbs[1])*0.008+1)/ttloot[ttm[2]]);
                                                $('#rval'+ttm[2]).val(numbs[2]);
                                                $('#WCcomcount').val(i-1);
                                                $('#wcraidunti').val(1).change();
                                            }
                                        } else {
                                            $('#rval'+ttm[1]).val(numbs[2]);
                                            numbs[2]=Math.ceil(ratio[2]*loot[numbs[0]]*((100-numbs[1])*0.008+1)/ttloot[ttm[2]]);
                                                $('#rval'+ttm[2]).val(numbs[2]);
                                            $('#WCcomcount').val(i-1);
                                            $('#wcraidunti').val(1).change();
                                        }
                                        break;
                                    }
                                    else if(i==count) {
                                        $('#WCcomcount').val(i).change();
                                        $('#wcraidunti').val(1).change();
                                    }
                                    if (troopshome[1]/(i*numbs[2])>=1.1) {
                                        previ=false;
                                    }
                                    else {
                                        previ=true;
                                    }
                                }
                            } else {
                                message="Error, you need at least " + numbs[2] + " " + ttname[ttm[1]]+"!";
                                errorgo(message);
                                if (troopshome[1]/(numbs[2])<=0.90) {
                                    $('#wcraidunti').val(3).change();
                                }
                            }
                        } else {
                            numbs[2]=Math.ceil(ratio[2]*loot[numbs[0]]*((100-numbs[1])*0.008+1)/ttloot[ttm[2]]);
                            if(Number(troopshome[2])>=numbs[2]) {
                                var previ=false;
                                for (var i=2; i<=count;i++) {
                                    if (troopshome[2]/(i*numbs[2])<=0.90) {
                                        if (i-1>1) {
                                            if (previ){
                                                $('#WCcomcount').val(i-1).change();
                                                $('#wcraidunti').val(1).change();
                                            }
                                            else {
                                                $('#rval'+ttm[2]).val(numbs[2]);
                                                $('#WCcomcount').val(i-1);
                                                $('#wcraidunti').val(1).change();
                                            }
                                        } else {
                                            $('#rval'+ttm[2]).val(numbs[2]);
                                            $('#WCcomcount').val(i-1);
                                            $('#wcraidunti').val(1).change();
                                        }
                                        break;
                                    }
                                    else if(i==count) {
                                        $('#WCcomcount').val(i).change();
                                        $('#wcraidunti').val(1).change();
                                    }
                                    if (troopshome[2]/(i*numbs[2])>=1.1) {
                                        previ=false;
                                    }
                                    else {
                                        previ=true;
                                    }
                                }
                            } else {
                                message="Error, you need at least " + numbs[2] + " " + ttname[ttm[2]]+"!";
                                errorgo(message);
                                if (troopshome[2]/(numbs[2])<=0.90) {
                                    $('#wcraidunti').val(3).change();
                                }
                            }
                        }
                    }
                }
                //console.log("count:"+count);
                count=Math.floor(count);
                //setTimeout(function(){$('.splitRaid').val(count);}, 500);
        }, 300);
        });
        });
    }
    // generate buildings count for city
function makebuildcount() {
    $("#bdtable").remove();
    var currentbd={name:[],bid:[],count:[0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0]};
    var j;
    var bdtypecount=-1;
    var bdNumber=-1;

    for (var i in poll2.city.bd) {
        if (buildings.bid.indexOf(poll2.city.bd[i].bid)>-1) {
            if (currentbd.bid.indexOf(poll2.city.bd[i].bid)>-1) {
                j=currentbd.bid.indexOf(poll2.city.bd[i].bid);
                currentbd.count[j]+=1;
                bdNumber+=1;
            } else {
                bdtypecount+=1;
                j=buildings.bid.indexOf(poll2.city.bd[i].bid);
                currentbd.name[bdtypecount]=buildings.name[j];
                currentbd.bid[bdtypecount]=buildings.bid[j];
                currentbd.count[bdtypecount]+=1;
                bdNumber+=1;
            }
        }
    }
    //console.log(currentbd);
    var bdtable="<table id='bdtable'><tbody><tr>";
    for (var i in currentbd.bid) {
        if (i<9 || ((i>9 && i<19) || (i>19 && i<29))) {
            bdtable+="<td style='text-align:center; width:32px; height:32px;'><div style='background-image: url(/images/city/buildings/icons/"+currentbd.name[i]+".png); background-size:contain;background-repeat:no-repeat;width:32px; height:32px;'></div>"+Number(currentbd.count[i])+"</td>";
        }
        if (i==9 || i==19) {
            //console.log(i);
            bdtable+="</tr><tr>";
            bdtable+="<td style='text-align:center; width:32px; height:32px;'><div style='background-image: url(/images/city/buildings/icons/"+currentbd.name[i]+".png); background-size:contain;background-repeat:no-repeat;width:32px; height:32px;'></div>"+Number(currentbd.count[i])+"</td>";
        }
    }
    bdtable+="</tr></tbody></table>";
    $("#bdcountwin").html(bdtable);
    $("#numbdleft").html(bdNumber);
}

    var bdcountshow=true;
    $(document).ready(function() {
        var bdcountbox="<div id='currentBd'><div id='bdcountbar' class='queueBar' style='border-bottom-left-radius: 0px; border-bottom-right-radius: 0px;'>";
        bdcountbox+="<div id='bdcountbut' class='tradeqarr2'><div></div></div><span class='qbspan'>Current Buildings</span>";
        bdcountbox+="<div id='numbdleft' class='barRightFloat tooltipstered'>0</div></div><div id='bdcountwin' class='queueWindow' style='display: block;'></div></div>";
        $("#bqitemss").after(bdcountbox);
        $("#bdcountbar").click(function() {
            if (bdcountshow) {
                //console.log(1);
                $("#bdcountwin").hide();
                $("#bdcountbar").attr("style","border-bottom-left-radius: 10px; border-bottom-right-radius: 10px;");
                $("#bdcountbut").removeClass('tradeqarr2').addClass('tradeqarr1');
                bdcountshow=false;
            } else {
                $("#bdcountwin").show();
                $("#bdcountbar").attr("style","border-bottom-left-radius: 0px; border-bottom-right-radius: 0px;");
                $("#bdcountbut").removeClass('tradeqarr1').addClass('tradeqarr2');
                bdcountshow=true;
            }
        });
        var wood50="<td><button class='brownb' id='wood50'>Add 50%</button></td>";
        $("#woodmaxbutton").parent().after(wood50);
        $("#wood50").click(function() {
            var res=Number($("#maxwoodsend").text().replace(/,/g,""));
            if ($("#landseasendres").val()=="1") {
                var carts=Math.floor(Number($("#availcartscity").text())/2)*1000;
            } else {
                var carts=Math.floor(Number($("#availshipscity").text())/2)*10000;
            }
            if (res>carts) {
                res=carts;
            }
            $("#woodsendamt").val(res);
        });
        var stone50="<td><button class='brownb' id='stone50'>Add 50%</button></td>";
        $("#stonemaxbutton").parent().after(stone50);
        $("#stone50").click(function() {
            if ($("#landseasendres").val()=="1") {
                var carts=Math.floor(Number($("#availcartscity").text())/2)*1000;
            } else {
                var carts=Math.floor(Number($("#availshipscity").text())/2)*10000;
            }
            var res=Number($("#maxstonesend").text().replace(/,/g,""));
            if (res>carts) {
                res=carts;
            }
            $("#stonesendamt").val(res);
        });
        var iron50="<td><button class='brownb' id='iron50'>Add 50%</button></td>";
        $("#ironmaxbutton").parent().after(iron50);
        $("#iron50").click(function() {
            var res=Number($("#maxironsend").text().replace(/,/g,""));
            if ($("#landseasendres").val()=="1") {
                var carts=Math.floor(Number($("#availcartscity").text())/2)*1000;
            } else {
                var carts=Math.floor(Number($("#availshipscity").text())/2)*10000;
            }
            if (res>carts) {
                res=carts;
            }
            $("#ironsendamt").val(res);
        });
        var food50="<td><button class='brownb' id='food50'>Add 50%</button></td>";
        $("#foodmaxbutton").parent().after(food50);
        $("#food50").click(function() {
            var res=Number($("#maxfoodsend").text().replace(/,/g,""));
            if ($("#landseasendres").val()=="1") {
                var carts=Math.floor(Number($("#availcartscity").text())/2)*1000;
            } else {
                var carts=Math.floor(Number($("#availshipscity").text())/2)*10000;
            }
            if (res>carts) {
                res=carts;
            }
            $("#foodsendamt").val(res);
        });
        var shrinebut="<button class='regButton greenb' id='shrineP' style='width: 98%;margins: 1%;'>Shrine Planner</button>";
    $("#inactiveshrineInfo").before(shrinebut);
    $("#shrineP").click(function() {
        if (beentoworld) {
            shrinec=[[]];
            splayers={name:[],ally:[],cities:[]};
            var players=[];
            var coords=$("#coordstochatGo3").attr("data");
            var shrinex=parseInt(coords);
            var shriney=Number(coords.match(/\d+$/)[0]);
            var shrinecont=Number(Math.floor(shrinex/100)+10*Math.floor(shriney/100));
            for (var i in wdata.cities) {
                var tempx=Number(wdata.cities[i].substr(8,3))-100;
                var tempy=Number(wdata.cities[i].substr(5,3))-100;
                var cont=Number(Math.floor(tempx/100)+10*Math.floor(tempy/100));
                if (cont==shrinecont) {
                    var dist=Math.sqrt((tempx-shrinex)*(tempx-shrinex)+(tempy-shriney)*(tempy-shriney));
                    //console.log("dist");
                    if (dist<10) {
                        var l=Number(wdata.cities[i].substr(11,1));
                        var pid=Number(wdata.cities[i].substr(12,l));
                        var pname=pldata[pid];
                        //console.log(pname);
                        //console.log(splayers.name.indexOf(pname),pname,splayers.name);
                        var csn=[3,4,7,8];
                        if (csn.indexOf(Number(wdata.cities[i].charAt(4)))>-1) {
                            shrinec.push(["castle",pname,0,tempx,tempy,dist,"0",0,0,0]);
                        } else {
                            shrinec.push(["city",pname,0,tempx,tempy,dist,"0",0,0,0]);
                        }
                    }
                }
            }
            shrinec.sort(function(a,b) {return a[5]-b[5];});
            var planwin="<div id='shrinePopup' style='width:40%;height:50%;left: 360px; z-index: 3000;' class='popUpBox'><div class='popUpBar'><span class=\"ppspan\">Shrine Planner</span><button id='hidec' class='greenb' style='margin-left:10px;border-radius: 7px;margin-top: 2px;height: 28px;'>Hide Cities</button>";
            planwin+="<button id='addcity' class='greenb' style='margin-left:10px;border-radius: 7px;margin-top: 2px;height: 28px;'>Add City</button><button id=\"sumX\" onclick=\"$('#shrinePopup').remove();\" class=\"xbutton greenb\"><div id=\"xbuttondiv\"><div><div id=\"centxbuttondiv\"></div></div></div></button></div><div class=\"popUpWindow\" style='height:100%'>";
            planwin+="<div id='shrinediv' class='beigemenutable scroll-pane' style='background:none;border: none;padding: 0px;height:90%;'></div></div>";
            for (var i in shrinec) {
                if (i<101) {
                    var pname=shrinec[i][1];
                    if (players.indexOf(pname)==-1) {
                        players.push(pname);
                        jQuery.ajax({url: 'includes/gPi.php',type: 'POST',aysnc:false,data: {a: pname},
                                     success: function(data) {
                                         var pinfo=JSON.parse(data);
                                         splayers.name.push(pinfo.player);
                                         splayers.ally.push(pinfo.a);
                                         splayers.cities.push(pinfo.h);
                                         //console.log(pinfo.a,pinfo.h,pinfo.player);
                                     }
                                    });
                    }
                }
            }
            setTimeout(function() {
                $("#reportsViewBox").after(planwin);
                $( "#shrinePopup" ).draggable({ handle: ".popUpBar" , containment: "window", scroll: false});
                $( "#shrinePopup" ).resizable();
                if (localStorage.getItem("hidecities")) {
                    1==1;
                } else {
                    //console.log("hideciies nonexists");
                    localStorage.setItem("hidecities","0");
                }
                if (localStorage.getItem("hidecities")=="1") {
                    $("#hidec").html("Show Cities");
                }
                $("#hidec").click(function() {
                    if (localStorage.getItem("hidecities")=="0") {
                        hidecities();
                        localStorage.setItem("hidecities","1");
                        $("#hidec").html("Show Cities");
                    } else if (localStorage.getItem("hidecities")=="1") {
                        showcities();
                        localStorage.setItem("hidecities","0");
                        $("#hidec").html("Hide Cities");
                    }
                });
                updateshrine();
                var addcitypop="<div id='addcityPopup' style='width:500px;height:100px;left: 360px; z-index: 3000;' class='popUpBox'><div class='popUpBar'><span class=\"ppspan\">Add City</span>";
                addcitypop+="<button id=\"sumX\" onclick=\"$('#addcityPopup').remove();\" class=\"xbutton greenb\"><div id=\"xbuttondiv\"><div><div id=\"centxbuttondiv\"></div></div></div></button></div><div class=\"popUpWindow\" style='height:100%'>";
                addcitypop+="<div><table><td>X: <input id='addx' type='number' style='width: 35px;height: 22px;font-size: 10px;'></td><td>y: <input id='addy' type='number' style='width: 35px;height: 22px;font-size: 10px;'></td>";
                addcitypop+="<td>score: <input id='addscore' type='number' style='width: 45px;height: 22px;font-size: 10px;'></td><td>Type: <select id='addtype' class='greensel' style='font-size: 15px !important;width:55%;height:30px;'>";
                addcitypop+="<option value='city'>City</option><option value='castle'>Castle</option></select></td><td><button id='addadd' class='greenb'>Add</button></td></table></div></div>";
                $("#addcity").click(function() {
                    $("body").append(addcitypop);
                    $( "#addcityPopup" ).draggable({ handle: ".popUpBar" , containment: "window", scroll: false});
                    $("#addadd").click(function() {
                        tempx=$("#addx").val();
                        tempy=$("#addy").val();
                        dist=Math.sqrt((tempx-shrinex)*(tempx-shrinex)+(tempy-shriney)*(tempy-shriney));
                        var temp=[$("#addtype").val(),"Poseidon","Atlantis",tempx,tempy,dist,"1",$("#addscore").val(),"Hellas","1"];
                        shrinec.push(temp);
                        shrinec.sort(function(a,b) {return a[5]-b[5];});
                        updateshrine();
                        $("#addcityPopup").remove();
                    });
                });
            },2000);
        } else {
            alert("Press World Button");
        }
    });
    });

    //hiding cities in shrine planner
    function hidecities() {
        $("#shrineTab tr").each(function () {
            if($(this).attr("data")=="city") {
                $(this).hide();
            }
        });
    }
    //showing cities in shrine planner
    function showcities() {
        $("#shrineTab tr").each(function () {
            if($(this).attr("data")=="city") {
                $(this).show();
            }
        });
    }

    //updating shrine enlightment list
function updateshrine() {
    var shrinetab="<table id='shrineTab'><thead><th style='width:115px'>Change</th><th style='width:50px'>Chances</th><th>Distance</th><th>Player</th><th>City</th><th>Coords</th><th style='width:100px'>Alliance</th><th>score</th><th>Type</th></thead><tbody>";
    var ccounter=0;
    var w=[];
    var wtot=0;
    for (var i in shrinec) {
        if (i>0) {
            var k=splayers.name.indexOf(shrinec[i][1]);
            //console.log(k,splayers);
            for (var j in splayers.cities[k]) {
                if (shrinec[i][3]==splayers.cities[k][j].b && shrinec[i][4]==splayers.cities[k][j].c) {
                    shrinec[i][2]=splayers.cities[k][j].h;
                    if (shrinec[i][9]==0) {
                        shrinec[i][7]=splayers.cities[k][j].a;
                    }
                    shrinec[i][8]=splayers.ally[k];
                }
            }
            if (shrinec[i][0]=="castle") {
                ccounter++;
                if (ccounter<17) {
                    w[ccounter]=shrinec[i][7]/shrinec[i][5];
                    wtot+=shrinec[i][7]/(shrinec[i][5]);
                }
            }
        }
    }
    for (var i in w) {
        w[i]=Math.round(w[i]/wtot*100);
    }
    //console.log(shrinec);
    var ccounter=0;
    for (var i in shrinec) {
        if (i>0) {
            var cid=shrinec[i][4]*65536+Number(shrinec[i][3]);
            if (shrinec[i][0]=="castle") {
                ccounter++;
                if (ccounter<17) {
                    if (shrinec[i][6]=="0") {
                        shrinetab+="<tr style='color:purple;'><td><button data='"+i+"' class='greenb shrineremove' style='font-size: 10px;height: 20px;padding: 3px;width: 15px;border-radius: 4px;'>x</button>";
                        shrinetab+="<button id='"+i+"' data='castle' class='greenb shrinechange' style='font-size: 10px;height: 20px;padding-top: 3px;border-radius: 4px;'>City</button>";
                        shrinetab+="<button data='"+i+"' class='greenb shrine10k' style='font-size: 10px;height: 20px;padding: 3px;width: 25px;border-radius: 4px;'>10k</button>";
                        shrinetab+="<button data='"+i+"' class='greenb shrine7pt' style='font-size: 10px;height: 20px;padding: 3px;width: 25px;border-radius: 4px;'>7pt</button></td><td>"+ccounter+" - "+w[ccounter]+"% "+"</td>";
                    } else {
                        shrinetab+="<tr style='color:green;'><td><button data='"+i+"' class='greenb shrineremove' style='font-size: 10px;height: 20px;padding: 3px;width: 15px;border-radius: 4px;'>x</button>";
                        shrinetab+="<button id='"+i+"' data='castle' class='greenb shrinechange' style='font-size: 10px;height: 20px;padding-top: 3px;border-radius: 4px;'>City</button>";
                        shrinetab+="<button data='"+i+"' class='greenb shrine10k' style='font-size: 10px;height: 20px;padding: 3px;width: 25px;border-radius: 4px;'>10k</button>";
                        shrinetab+="<button data='"+i+"' class='greenb shrine7pt' style='font-size: 10px;height: 20px;padding: 3px;width: 25px;border-radius: 4px;'>7pt</button></td><td>"+ccounter+" - "+w[ccounter]+"% "+"</td>";
                    }
                } else if (ccounter>=17 && ccounter<21) {
                    shrinetab+="<tr><td><button data='"+i+"' class='greenb shrineremove' style='font-size: 10px;height: 20px;padding: 3px;width: 15px;border-radius: 4px;'>x</button>";
                    shrinetab+="<button id='"+i+"' data='castle' class='greenb shrinechange' style='font-size: 10px;height: 20px;padding-top: 3px;border-radius: 4px;'>City</button>";
                    shrinetab+="<button data='"+i+"' class='greenb shrine10k' style='font-size: 10px;height: 20px;padding: 3px;width: 25px;border-radius: 4px;'>10k</button>";
                    shrinetab+="<button data='"+i+"' class='greenb shrine7pt' style='font-size: 10px;height: 20px;padding: 3px;width: 25px;border-radius: 4px;'>7pt</button></td><td>"+ccounter+"</td>";
                }
            } else {
                if (shrinec[i][6]=="0") {
                    shrinetab+="<tr style='color:grey;' data='city'><td><button data='"+i+"' class='greenb shrineremove' style='font-size: 10px;height: 20px;padding: 3px;width: 15px;border-radius: 4px;'>x</button>";
                    shrinetab+="<button id='"+i+"' data='city' class='greenb shrinechange' style='font-size: 10px;height: 20px;padding: 3px;border-radius: 4px;width:37px;'>Castle</button>";
                    shrinetab+="<button data='"+i+"' class='greenb shrine10k' style='font-size: 10px;height: 20px;padding: 3px;width: 25px;border-radius: 4px;'>10k</button>";
                    shrinetab+="<button data='"+i+"' class='greenb shrine7pt' style='font-size: 10px;height: 20px;padding: 3px;width: 25px;border-radius: 4px;'>7pt</button></td><td></td>";
                } else {
                    shrinetab+="<tr style='color:#74A274;'><td><button data='"+i+"' class='greenb shrineremove' style='font-size: 10px;height: 20px;padding: 3px;width: 15px;border-radius: 4px;'>x</button>";
                    shrinetab+="<button id='"+i+"' data='city' class='greenb shrinechange' style='font-size: 10px;height: 20px;padding: 3px;border-radius: 4px;width:37px;'>Castle</button>";
                    shrinetab+="<button data='"+i+"' class='greenb shrine10k' style='font-size: 10px;height: 20px;padding: 3px;width: 25px;border-radius: 4px;'>10k</button>";
                    shrinetab+="<button data='"+i+"' class='greenb shrine7pt' style='font-size: 10px;height: 20px;padding: 3px;width: 25px;border-radius: 4px;'>7pt</button></td><td></td>";
                }
            }
            shrinetab+="<td>"+roundToTwo(shrinec[i][5])+"</td><td class='playerblink'>"+shrinec[i][1]+"</td><td>"+shrinec[i][2]+"</td><td class='coordblink shcitt' data='"+cid+"'>"+shrinec[i][3]+":"+shrinec[i][4]+"</td><td class='allyblink'>"+shrinec[i][8]+"</td><td>"+shrinec[i][7]+"</td><td>"+shrinec[i][0]+"</td></tr>";
            if (ccounter==20) {
                break;
            }
        }
    }
    shrinetab+="</tbody></table>";
    $("#shrinediv").html(shrinetab);
    $("#shrineTab td").css("text-align","center");
    if (localStorage.getItem("hidecities")=="1") {
        hidecities();
        //console.log("hiding");
    }
    $(".shrinechange").click(function() {
        if ($(this).attr("data")=="castle") {
            shrinec[$(this).attr("id")][0]="city";
        } else {
            shrinec[$(this).attr("id")][0]="castle";
        }
        if (shrinec[$(this).attr("id")][6]=="0") {
            shrinec[$(this).attr("id")][6]=1;
        } else {
            shrinec[$(this).attr("id")][6]=0;
        }
        updateshrine();
    });
    $(".shrineremove").click(function() {
        shrinec.splice($(this).attr("data"),1);
        updateshrine();
    });
    $(".shrine7pt").click(function() {
        if (shrinec[$(this).attr("data")][7]!=7) {
            shrinec[$(this).attr("data")][7]=7;
            shrinec[$(this).attr("data")][9]=1;
            shrinec[$(this).attr("data")][6]=1;
        } else {
            shrinec[$(this).attr("data")][9]=0;
            shrinec[$(this).attr("data")][6]=0;
        }
        updateshrine();
    });
    $(".shrine10k").click(function() {
        if (shrinec[$(this).attr("data")][7]!=10000) {
            shrinec[$(this).attr("data")][7]=10000;
            shrinec[$(this).attr("data")][9]=1;
            shrinec[$(this).attr("data")][6]=1;
        } else {
            shrinec[$(this).attr("data")][9]=0;
            shrinec[$(this).attr("data")][6]=0;
        }
        updateshrine();
    });
}
// exporting table to csv file taken from https://gist.github.com/adilapapaya/9787842
    function exportTableToCSV($table, filename) {
        var $headers = $table.find('tr:has(th)')
        ,$rows = $table.find('tr:has(td)')
        // Temporary delimiter characters unlikely to be typed by keyboard
        // This is to avoid accidentally splitting the actual contents
        ,tmpColDelim = String.fromCharCode(11) // vertical tab character
        ,tmpRowDelim = String.fromCharCode(0) // null character
        // actual delimiter characters for CSV format
        ,colDelim = '","'
        ,rowDelim = '"\r\n"';
        // Grab text from table into CSV formatted string
        var csv = '"';
        csv += formatRows($headers.map(grabRow));
        csv += rowDelim;
        csv += formatRows($rows.map(grabRow)) + '"';
        // Data URI
        var csvData = 'data:application/csv;charset=utf-8,' + encodeURIComponent(csv);
        $(this)
            .attr({
            'download': filename
            ,'href': csvData
            //,'target' : '_blank' //if you want it to open in a new window
        });
        //------------------------------------------------------------
        // Helper Functions 
        //------------------------------------------------------------
        // Format the output so it has the appropriate delimiters
        function formatRows(rows){
            return rows.get().join(tmpRowDelim)
                .split(tmpRowDelim).join(rowDelim)
                .split(tmpColDelim).join(colDelim);
        }
        // Grab and format a row from the table
        function grabRow(i,row){

            var $row = $(row);
            //for some reason $cols = $row.find('td') || $row.find('th') won't work...
            var $cols = $row.find('td'); 
            if(!$cols.length) $cols = $row.find('th');  
            return $cols.map(grabCol)
                .get().join(tmpColDelim);
        }
        // Grab and format a column from the table 
        function grabCol(j,col){
            var $col = $(col),
                $text = $col.text();
            return $text.replace('"', '""'); // escape double quotes
        }
    }
    //decoding world data for w4 and onwards
    function decwdata(data) {
        var DecData = {bosses:[],cities:[],ll:[],cavern:[],portals:[],shrines:[]},
            temp = data.split("|"),
            keys = temp[1].split("l"),
            ckey = keys[0],
            skey = keys[1],
            bkey = keys[2],
            lkey = keys[3],
            cavkey = keys[4],
            pkey = keys[5],
            cities = temp[0].split("l"),
            shrines = temp[2].split("l"),
            bosses = temp[3].split("l"),
            lawless = temp[4].split("l"),
            caverns = temp[5].split("l"),
            portals = temp[6].split("l"),
            dat = 0;
        //console.log(ckey,skey,bkey,lkey,cavkey,pkey);
        for (var i in bosses) {
            dat = (Number(bosses[i]) + Number(bkey)) + "";
            bkey=dat;
            DecData.bosses.push("1" + dat);
        }
        for (var i in cities) {
            dat = (Number(cities[i]) + Number(ckey)) + "";
            ckey=dat;
            DecData.cities.push("2" + dat);
        }
        for (var i in lawless) {
            dat = (Number(lawless[i]) + Number(lkey)) + "";
            lkey=dat;
            DecData.ll.push("3" + dat);
        }
        for (var i in caverns) {
            dat = (Number(caverns[i]) + Number(cavkey)) + "";
            cavkey=dat;
            DecData.cavern.push("7" + dat);
        }
        for (var i in portals) {
            dat = (Number(portals[i]) + Number(pkey)) + "";
            pkey=dat;
            DecData.portals.push("8" + dat);
        }
        for (var i in shrines) {
            dat = (Number(shrines[i]) + Number(skey))+ "";
            skey=dat;
            DecData.shrines.push("9" + dat);
        }
        return DecData;
    }
})();