"use strict";
// Server Part
var serverPort = 9874;

var express = require('express');
var bodyParser = require('body-parser');
var app = express();
app.use(bodyParser.json({
	limit: '200mb',
	parameterLimit: 50000
}));
app.use(bodyParser.urlencoded({
	extended: false,
	limit: '200mb',
	parameterLimit: 50000
}));
var server = app.listen(serverPort, function() {
	var host = server.address().address;
	var port = server.address().port;
	 app.get(/^(.+)$/, function(req, res){ 
		 console.log('static file request : ' + req.params);
		 res.sendfile( __dirname + req.params[0]); 
	 });
});