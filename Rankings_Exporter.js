// ==UserScript==
// @name            Rankings Exporter
// @namespace       https://bitbucket.org/
// @version         1.9.10
// @description     Try to take over the world!
// @author          Lionnel
// @match           https://w*.crownofthegods.com/
// @include         https://w*.crownofthegods.com/
// @updateURL       https://bitbucket.org/lionnel0/cotg/raw/master/Rankings_Exporter.js
// @doc             https://bitbucket.org/lionnel0/cotg_public/wiki/Rank%20exporter
// ==/UserScript==
LIO_INTEL = {};
LIO = {
    displayData: function (table, conti) {
        if (!this.cities) {
            table.html("<tr><td>Load world view</td></tr>");
        } else {
            var cities = LIO.cities.filter(function (item) {
                return item.conti === conti;
            })
            var that = this;
            var result = "";
            if (cities) {
                cities.forEach(function (city) {
                    var playerInfo = (city.player ? city.player.name : "?") + " (" + city.playerId + ")";
                    var allyInfo = (city.player ? city.player.ally : "?") + " (" + city.allyId + ")";
                    result = result + "<tr>" +
                        //<thead><th>Player</th><th>Alliance</th><th>Coord</th><th>Water</th><th>Castled</th><th>Palace lvl</th><th>City size</th></thead>\
                        "<td>" + playerInfo + "</td>" +
                        "<td>" + allyInfo + "</td>" +
                        "<td><span class='coordblink shcitt' data='" + (city.coordY * 65536 + city.coordX) + "'>" + "C" + city.conti + " " + city.coord + "</span></td>" +
                        "<td>" + city.water + "</td>" +
                        "<td>" + city.caslted + "</td>" +
                        "<td>" + city.palaceLvl + "</td>" +
                        "<td>" + city.level + "</td>" +
                        "</tr>";
                })
                table.html(result);
            }
        }
    },
    getPlayersFromId: function (processPlayersFromId) {
        if (!that.playersFromId) {
            that.playersFromId = {};
            jQuery.ajax({
                url: 'includes/gR.php',
                type: 'POST',
                aysnc: false,
                data: {
                    a: 0
                },
                success: function (data) {
                    Object.keys(playersNameFromId).forEach(function (playerId) {
                        playersIdFromName[playersNameFromId[playerId]] = playerId;
                    })

                    var playersData = JSON.parse(data)[0];
                    players = {}
                    playersData.forEach(function (player) {
                        that.playersFromId[playersIdFromName[player["1"]]] = {
                            id: playersIdFromName[player["1"]],
                            name: player["1"],
                            ally: player["4"]
                        };
                        /*
                        players[player["3"]] = {
                            name: player["1"],
                            // 2 => rank on conti
                            id: player["3"],
                            ally: player["4"]
                            // 5 => nb city
                        };*/
                    });
                    that.returnData(that.playersFromId);
                }
            });
        } else {
            processPlayersFromId(that.playersFromId)
        }
    },

    cities: [],
    // In: wdata, out: cities
    decodeWorldRowData: function (wdata) {
        var that = this;
        var worldDecodedData = {
                bosses: [],
                cities: [],
                ll: [],
                cavern: [],
                portals: [],
                shrines: []
            },
            temp = wdata.split("|"),
            keys = temp[1].split("l"),
            ckey = keys[0],
            skey = keys[1],
            bkey = keys[2],
            lkey = keys[3],
            cavkey = keys[4],
            pkey = keys[5],
            rawCities = temp[0].split("l"),
            shrines = temp[2].split("l"),
            bosses = temp[3].split("l"),
            lawless = temp[4].split("l"),
            caverns = temp[5].split("l"),
            portals = temp[6].split("l"),
            dat = 0;
        for (var i in bosses) {
            dat = (Number(bosses[i]) + Number(bkey)) + "";
            bkey = dat;
            worldDecodedData.bosses.push("1" + dat);
        }
        for (var i in rawCities) {
            dat = (Number(rawCities[i]) + Number(ckey)) + "";
            var decode = function (a2T, i2T) {
                var S2T = a2T.length;
                var y2T = i2T.length;
                if (S2T >= y2T) {
                    var P2T = a2T;
                    var Z2T = i2T;
                    var k2T = S2T;
                    var s2T = y2T;
                } else {
                    var P2T = i2T;
                    var Z2T = a2T;
                    var k2T = y2T;
                    var s2T = S2T;
                }
                var W2T = P2T.split("");
                var g2T = Z2T.split("");
                var r2T = W2T.length;
                var v2T = g2T.length;
                var w2T = 0;
                var h2T = "";
                var R2T = 1;
                var M2T;
                var p2T = 0;
                for (var Y2T = r2T - 1; Y2T >= 0; Y2T--) {
                    p2T = 0;
                    if (g2T.hasOwnProperty(v2T - R2T))
                        p2T = g2T[v2T - R2T];
                    p2T = Number(p2T) + w2T;
                    M2T = Number(W2T[Y2T]) + p2T;
                    if (M2T >= +10) {
                        w2T = Math.floor(M2T / +10);
                        M2T = M2T % (10 | 0);
                    } else
                        w2T = "0" | 0;
                    h2T = M2T + "" + h2T;
                    R2T++;
                }
                if (w2T != +"0")
                    h2T = w2T + "" + h2T;
                return h2T;
            }
            dat = decode(ckey, rawCities[i]);
            ckey = dat;
            worldDecodedData.cities.push("2" + dat);
        }
        for (var i in lawless) {
            dat = (Number(lawless[i]) + Number(lkey)) + "";
            lkey = dat;
            worldDecodedData.ll.push("3" + dat);
        }
        for (var i in caverns) {
            dat = (Number(caverns[i]) + Number(cavkey)) + "";
            cavkey = dat;
            worldDecodedData.cavern.push("7" + dat);
        }
        for (var i in portals) {
            dat = (Number(portals[i]) + Number(pkey)) + "";
            pkey = dat;
            worldDecodedData.portals.push("8" + dat);
        }
        for (var i in shrines) {
            dat = (Number(shrines[i]) + Number(skey)) + "";
            skey = dat;
            worldDecodedData.shrines.push("9" + dat);
        }

        // Filling cities list
        that.cities = [];
        worldDecodedData.cities.forEach(function (city) {
            var W7T = city.substring(8, 11); // loc
            var a7T = city.substring(5, 8); // loc
            var FST = W7T + "" + a7T;
            var palaceLevel = city.substring(2, 3); // palace level
            var wpalt = city.substring(3, 4); // ?
            var o7T = city.substring(4, 5); // ?
            var z5T = Number(city.substring(11, 12)); // nb of figure for playerId
            var m5T = city.substring(12, 12 + z5T); // Player Id
            var H7T = city.substring(12 + z5T); // Ally id
            var xloc = W7T - 100;
            var yloc = a7T - 100;
            var conti = "" + Math.floor(yloc / 100) + Math.floor(xloc / 100);
            var coord = xloc + ":" + yloc;
            var v7T, k7T, Q7T;
            if (o7T === "1") {
                v7T = 0;
                k7T = 0;
                Q7T = 1;
            } else if (o7T === "2") {
                v7T = 0;
                k7T = 1;
                Q7T = 1;
            } else if (o7T === "3") {
                v7T = 1;
                k7T = 1;
                Q7T = 1;
            } else if (o7T === "4") {
                v7T = 1;
                k7T = 0;
                Q7T = 1;
            } else if (o7T === "5") {
                v7T = 0;
                k7T = 0;
                Q7T = 8;
            } else if (o7T === "6") {
                v7T = 0;
                k7T = 1;
                Q7T = 8;
            } else if (o7T === "7") {
                v7T = 1;
                k7T = 1;
                Q7T = 8;
            } else if (o7T === "8") {
                v7T = 1; // city: 0, castle 1
                k7T = 0; // Water 1, land 0
                Q7T = 8; // low level 1 / hight level 9
            }
            that.cities.push({
                playerId: m5T,
                allyId: H7T,
                coordX: xloc,
                coordY: yloc,
                coord: coord,
                conti: conti,
                caslted: v7T === 1,
                water: k7T === 1,
                palaceLvl: palaceLevel,
                raw: city,
                level: (Q7T === 1 ? "small" : (Q7T === 8 ? "big" : "?"))
            });
        });

        that.enrichCities();
    },

    players: [],
    decodePlayers: function (data) {
        var that = this;
        that.players = [];
        Object.keys(data).forEach(function (playerId) {
            that.players.push({
                id: playerId,
                name: data[playerId]
            });
        });

        var playersFromName = that.players.reduce(function (pv, cv) {
            pv[cv.name] = cv;
            return pv;
        }, {});

        jQuery.ajax({
            url: 'includes/gR.php',
            type: 'POST',
            aysnc: false,
            data: {
                a: 0
            },
            success: function (data) {
                JSON.parse(data)[0].forEach(function (playerData) {
                    var player = playersFromName[playerData["1"]];
                    if (player) {
                        player.ally = playerData["4"];
                    }
                });
                that.enrichCities();
            }
        });
    },

    // in: cities, players, allies. out: cities with info
    enrichCities: function () {
        if (this.cities && this.players) {
            var playersFromId = this.players.reduce(function (pv, cv) {
                pv[cv.id] = cv;
                return pv;
            }, {});
            this.cities.forEach(function (city) {
                if (city.playerId && playersFromId[city.playerId]) {
                    city.player = playersFromId[city.playerId];
                }
            });
        }
        if (this.cities && this.allies) {
            var alliesFromId = this.allies.reduce(function (pv, cv) {
                pv[cv.id] = cv;
                return pv;
            }, {});
            this.cities.forEach(function (city) {
                if (city.allyId && alliesFromId[city.allyId]) {
                    city.ally = alliesFromId[city.allyId];
                }
            });
        }
    }
};

(function () {
    const VIEW_STAT_ID = "lioStatView";
    const BUTTON_UPDATE_STAT_ID = "lioStatUpdateButton";
    const BUTTON_EXPORT_STAT_ID = "lioStatExportButton";
    const BUTTON_EXPORT_RANK_ID = "lioRankExportButton";
    const BUTTON_EXPORT_TEMPLE_ID = "lioTempleExportButton";
    const INPUT_CONTI_ID = "lioStatConti";
    const TABLE_STAT_ID = "lioStatTable";
    const TAB_STAT_ID = "lioStatTab";
    var currentOffDefRequest = "";
    const OFF_HISTORY = "OFF_HISTORY";
    const DEF_HISTORY = "DEF_HISTORY";
    var intelActivated = false;

    var waitForGameLoaded = function () {
        if (($("#Sum").length > 0) && cotg.player.name()) { // CFuncky loaded
            $("#offhisttab").click(function () {
                currentOffDefRequest = OFF_HISTORY;
            });
            $("#defhisttab").click(function () {
                currentOffDefRequest = DEF_HISTORY;
            });
            if (((window.location.toString().indexOf("w12.") >= 0) && (cotg.player.alliance() === "Horizon")) ||
                (cotg.player.name() === "MikeyBonsai") || (cotg.player.name() === "TrollGralson") || (cotg.player.name() === "Lionnel0") || (cotg.player.name() === "Monnaie")) {
                intelActivated = true;

                // Rank exporter
                $("#Sum").click(function () {
                    var waitForSumLoaded = function () {
                        // Adding stat if not present
                        if ($("#" + TAB_STAT_ID).length === 0) {
                            if ($("#sumtabs").length > 0) {
                                $("#sumtabs").append(
                                    '<li id="' + TAB_STAT_ID + '"role="tab" class="ui-state-default ui-corner-top">\
                                    <a role="presentation" class="ui-tabs-anchor">Stat</a></li>'
                                );
                                $("#sumdiv").append(
                                    "<div id='" + VIEW_STAT_ID + "'>\
                                    Continent:<input id='" + INPUT_CONTI_ID + "' style='width: 30px;height: 22px;font-size: 10px;' type='number' value='0'>\
                                    <button id='" + BUTTON_UPDATE_STAT_ID + "' class='greenb' style='font-size:14px;border-radius:6px; margin:4px;'>Update</button>\
                                    <button id='" + BUTTON_EXPORT_STAT_ID + "'class='greenb' style='font-size:14px;border-radius:6px; margin:4px;'>Export</button>\
                                    <div class='beigemenutable scroll-pane' style='width:99%;height:110%;margin-left:4px;' >\
                                        <table id='" + TABLE_STAT_ID + "'>\
                                            <thead><th>Player</th><th>Alliance</th><th>Coord</th><th>Water</th><th>Castled</th><th>Palace lvl</th><th>City size</th></thead>\
                                            <tbody>\
                                        </table>\
                                    </div>\
                                </div>"
                                );

                                $("#empireRankingsDividers").append('<button id="' + BUTTON_EXPORT_RANK_ID + '" class="greenb" style="font-size: 10px;border-radius:6px;margin:4px;padding-bottom: 3px;padding-top: 3px;">Export</button>');
                                $("#" + TAB_STAT_ID).click(function () {
                                    $("#sumdiv>div").hide();
                                    $("#" + VIEW_STAT_ID).show();
                                })
                                $("#" + BUTTON_UPDATE_STAT_ID).click(function () {
                                    var conti = "" + $("#" + INPUT_CONTI_ID).val();
                                    LIO.displayData($("#" + TABLE_STAT_ID + " tbody"), conti);
                                });
                                $("#" + BUTTON_EXPORT_STAT_ID).click(function () {
                                    var win = window.open();
                                    win.document.body.innerHTML = $("#" + TABLE_STAT_ID).parent().html().replace(/,/g, '');
                                });
                                $("#" + BUTTON_EXPORT_RANK_ID).click(function () {
                                    var win = window.open();
                                    win.document.body.innerHTML = $("#" + BUTTON_EXPORT_RANK_ID).parent().parent().find("table:visible").parent().html().replace(/,/g, '');
                                });
                                $("#crownRankingsDividers").append('<button id="' + BUTTON_EXPORT_TEMPLE_ID + '" class="greenb" style="font-size: 10px;border-radius:6px;margin:4px;padding-bottom: 3px;padding-top: 3px;">Export</button>');
                                $("#" + BUTTON_EXPORT_TEMPLE_ID).click(function () {
                                    var win = window.open();
                                    win.document.body.innerHTML = $("#" + BUTTON_EXPORT_TEMPLE_ID).parent().parent().find("table:visible").parent().html();
                                });

                            }
                        } else {
                            setTimeout(waitForSumLoaded, 1000);
                        }
                    };
                    waitForSumLoaded();
                });

                // Intel on incoming & offence history
                var incomings = function () {
                    $("#iaBody tr").each(function () {
                        var location = $(':nth-child(10)', this).text();
                        if (LIO_INTEL[location]) {
                            $(this).find("td:first").html(LIO_INTEL[location]);
                        }
                    });
                };
                $("#aipxbutton").before('<button id = "LioUpdateIntel" class = "greenb tooltipstered" style = "height: 30px;width: 100px;margin-left: 47px;margin-top: 10px;border-radius:4px;font-size: 10px !important;padding: 0px;" > Update intel </button>');
                var updateIntel = function () {
                    $("script#LioIntel").remove();
                    if (window.location.toString().indexOf("w12.") >= 0) {
                        $("body").append('<script id="LioIntel" src="https://drive.google.com/uc?export=download&id=1Us0VBXZa3s8VCGXRP-de7c2LEoJyDT5i"></script>');
                        $("body").append('<script id="LioIntel" src="https://drive.google.com/uc?export=download&id=1rRewZTH6Gp1-2K9AWXzIDTaPPrQh9SaO"></script>');

                        setTimeout(function () {
                            if ("undefined" === typeof (LIO_INTEL_13)) {
                                LIO_INTEL_13 = {};
                            }
                            if ("undefined" === typeof (LIO_INTEL_15)) {
                                LIO_INTEL_15 = {};
                            }
                            if ("undefined" === typeof (LIO_INTEL_20)) {
                                LIO_INTEL_20 = {};
                            }
                            if ("undefined" === typeof (LIO_INTEL_22)) {
                                LIO_INTEL_22 = {};
                            }
                            if ("undefined" === typeof (LIO_INTEL_51)) {
                                LIO_INTEL_31 = {};
                            }
                            if ("undefined" === typeof (LIO_INTEL_51)) {
                                LIO_INTEL_51 = {};
                            }

                            LIO_INTEL = Object.assign({}, LIO_INTEL_13, LIO_INTEL_15, LIO_INTEL_20, LIO_INTEL_22, LIO_INTEL_31, LIO_INTEL_51);
                        }, 1000);
                    }

                    incomings();
                }
                $("#LioUpdateIntel").click(updateIntel);
                updateIntel();

                $("#allianceIncomings").parent().click(function () {
                    setTimeout(function () {
                        incomings();
                    }, 4000);
                });
                $("#incomingsPic").click(function () {
                    setTimeout(function () {
                        incomings();
                    }, 5000);
                });
                $("#ui-id-37").click(function () {
                    setTimeout(function () {
                        incomings();
                    }, 1000);
                });
            }
        } else {
            setTimeout(waitForGameLoaded, 3000);
        }
    };
    waitForGameLoaded();

    setTimeout(function () {
        (function (open) {
            XMLHttpRequest.prototype.open = function () {
                this.addEventListener("readystatechange", function () {
                    if (this.readyState == 4) {
                        var url = this.responseURL;
                        var cdata;
                        if (url.indexOf('gWrd.php') != -1) {
                            // Loading world
                            try {
                                var wdata = JSON.parse(this.response);
                                LIO.decodeWorldRowData(wdata.a);
                            } catch (e) {
                                console.error(e);
                            }
                        }
                        if (url.indexOf('gPlA.php') != -1) {
                            // list of players
                            LIO.decodePlayers(JSON.parse(this.response));
                        }
                        if (url.indexOf('ofdf.php') != -1) {

                            if (currentOffDefRequest === DEF_HISTORY) {
                                // Complete redraw of the content
                                var defHisto = JSON.parse(this.response);
                                setTimeout(function () {
                                    // correct defense history
                                    $('#dhBody').html("");
                                    if (defHisto.length > 0) {
                                        var result = "";
                                        var t4 = function (k3a) {
                                            return (k3a + '')["replace"](/([3-90-2])(?=([0-9]{3}){1,}$)/g, '$1,');
                                        };
                                        defHisto.forEach(function (row) {
                                            // Patching source id
                                            if (row[16] !== null && typeof (row[16]) === "object") {
                                                row[16] = Object.keys(row[16])[0];
                                            }

                                            var sourceCityId = row[16];
                                            var srcY = Math.floor(sourceCityId / 65536);
                                            var srcX = sourceCityId - srcY * 65536;
                                            var srcConti = "" + Math.floor(srcY / 100) + Math.floor(srcX / 100);
                                            var srcCity = "C" + srcConti + " (" + srcX + ":" + srcY + ")";

                                            var targerCityId = row[15];
                                            var targetY = Math.floor(targerCityId / 65536);
                                            var targetX = targerCityId - targetY * 65536;
                                            var targetConti = "" + Math.floor(targetY / 100) + Math.floor(targetX / 100);
                                            var intel = "";
                                            if (intelActivated && LIO_INTEL[srcCity]) {
                                                intel = LIO_INTEL[srcCity];
                                            }

                                            // Patching target conti
                                            if (row[13] === 0 || row[13] === "0") {
                                                row[13] = targetConti;
                                            }
                                            result = result +
                                                '<tr co=' + row[13] + ' data=' + row[12] + '>' +
                                                '<td>' + intel + '</td>' +
                                                '<td class="gFrep" id="reportlink" data="' + row[11] + '">' + row[0] + '</td>' +
                                                '<td class = "playerblink">' + row[1] + '</td>' +
                                                '<td class = "cityblink shcitt" data="' + row[15] + '">' + row[3] + '</td>' +
                                                '<td><span>' + row[4] + '</span></td>' +
                                                '<td>' + row[5] + '</td>' +
                                                '<td class = "playerblink">' + row[6] + '</td>' +
                                                '<td class = "allyblink" >' + row[2] + '</td>' +
                                                '<td class = "cityblink shcitt" data="' + row[16] + '">' + row[14] + '</td>' +
                                                '<td>' + row[7] + '</td>' +
                                                '<td></td>' +
                                                '<td>' + t4(Number(row[8])) + '</td>' +
                                                '<td>' + t4(Number(row[9])) + '</td>' +
                                                '<td>' + row[10] + '</td>' +
                                                '</tr>';

                                        });
                                        $('#dhBody').html(result);
                                    }
                                });
                            } else if (currentOffDefRequest === OFF_HISTORY) {
                                if (intelActivated) {
                                    setTimeout(function () {
                                        // offence history
                                        $("#offenseHistoryTable tbody tr").each(function () {
                                            var conti = $(this).attr("co");
                                            if ($(this).attr("co") === "0") {
                                                // Not the good conti
                                                var loc = $(':nth-child(6)', this).text();
                                                var loc2 = loc.split(":");
                                                conti = (loc2[1].length === 3 ? loc2[1][0] : "0") + (loc2[0].length === 3 ? loc2[0][0] : "0");
                                                $(this).attr("co", conti);
                                            }
                                            var location = "C" + conti + " (" + $(':nth-child(6)', this).text() + ")";
                                            if (LIO_INTEL[location]) {
                                                $(this).find("td:first").html(LIO_INTEL[location]);
                                            }
                                        });
                                    }, 4000);
                                }
                            }
                        }
                    }
                }, false);
                open.apply(this, arguments);
            };
        })(XMLHttpRequest.prototype.open);
    }, 4000);

})();