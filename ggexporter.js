// ==UserScript==
// @name         GGsheet exporter
// @namespace    http://tampermonkey.net/
// @version      0.1
// @description  try to take over the world!
// @author       You
// @match        https://*.crownofthegods.com/World00.php
// @include      https://w*.crownofthegods.com/World*
// @grant        none
// @updateURL    https://bitbucket.org/lionnel0/cotg/raw/master/ggexporter.js
// @downloadURL  https://bitbucket.org/lionnel0/cotg/raw/master/ggexporter.js
// ==/UserScript==
String.prototype.replaceAll = function(search, replacement) {
    var target = this;
    return target.replace(new RegExp(search, 'g'), replacement);
};
function exportTable(type) {
    console.log("Exporting");
    if(type==="player") {
        if ($(window.subwindow.document).find("#players").length===0) {
            var newbody='<table id="players"><thead><tr><td>Name</td><td>Score</td><td>City type</td><td>Coord</td><td>Conti</td><td>City score</td></tr></thead><tbody></tbody></table>';
            $(window.subwindow.document).find("body").html(newbody);
        }
        var data = {
            name: $("#plSpanName").text(),
            score: $("#plstSco").text().replaceAll(",",""),
            cities:[]
        };
        $("#citiesTable").find("tr").each(function(index, value){
            if(value.parentElement.tagName!="THEAD") {
                var city = {
                    type: $(value).find("#citytypePic>div>div").attr("id"),
                    coord: $(value).find("#citycoordtd").text(),
                    cont: $(value).find("#cityconttd").text(),
                    score: $(value).find("#cityscoretd").text()
                };
                $(window.subwindow.document).find('#players tr:last').after('<tr><td>'+data.name+'</td><td>'+data.score+'</td><td>'+city.type+'</td><td>\''+city.coord+'</td><td>'+city.cont+'</td><td>'+city.score+'</td></tr>');
                data.cities.push(city);
            }
        });
    }
    else if(type==="ally") {
        if ($(window.subwindow.document).find("#ally").length===0) {
            var newbody2='<table id="ally"><thead><tr><td>Ally</td><td>Player</td></tr></thead><tbody></tbody></table>';
            $(window.subwindow.document).find("body").html(newbody2);
        }
        var data2 = {
            name: $("#allyNamespan").text(),
            players:[]
        };
        $("#allyInfoMembersTable").find("tr").each(function(index, value){
            if(value.parentElement.tagName!="THEAD") {
                var player = {
                    name: $(value).find(".playerblink").text(),
                };
                $(window.subwindow.document).find('#ally tr:last').after('<tr><td>'+data2.name+'</td><td>'+player.name+'</td></tr>');
                data2.players.push(city);
            }
        });
    }
}
(function() {
    'use strict';
    console.log("gg expoter V0.1");
    $("#topplStatsDivc").append('<button id="exportPlayer" class="regButton greenb" title="Export to gg sheet!">Export</button>');
    $("#firstAMspan").append('<button id="exportAlly" class="regButton greenb" title="Export to gg sheet!">Export</button>');
    $("#exportPlayer").click(function(){
        exportTable("player");
    });
    $("#exportAlly").click(function(){
        exportTable("ally");
    });
    window.subwindow = window.open();
    var script= window.subwindow.document.createScript();
    script.src="https://code.jquery.com/jquery-3.1.1.min.js";
    window.subwindow.document.head.appendChild(script);
})();