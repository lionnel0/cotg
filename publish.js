var JavaScriptObfuscator = require('javascript-obfuscator');
var fs = require('fs');

var publish = function(err, data, fileName, name) {
	if (err) {
		console.error(err);
		return;
	}
	var obfuscationResult = JavaScriptObfuscator.obfuscate(
		data, {
			compact: false,
			controlFlowFlattening: true,
			disableConsoleOutput: false
		}
	);

	var obfuscatedCode = obfuscationResult.getObfuscatedCode();
	var versionComment = "// @version ";
	var versionCommentIndex = data.indexOf(versionComment);
	if (versionCommentIndex < 0) {
		console.error("Script Header not found");
		return;
	}
	var versionBeg = data.substr(versionCommentIndex);
	var versionEndIndex = versionBeg.indexOf("\n");
	if (versionEndIndex < 0) {
		console.error("Script Header not found");
		return;
	}
	var version = versionBeg.substr(versionComment.length, versionEndIndex - versionComment.length);

	var docComment = "// @doc ";
	var docCommentIndex = data.indexOf(docComment);
	var docBeg = data.substr(docCommentIndex);
	var docEndIndex = docBeg.indexOf("\n");
	if (docEndIndex < 0) {
		console.error("Script Header not found");
		return;
	}
	var doc = docBeg.substr(docComment.length, docEndIndex - docComment.length);

	var comment = "// ==UserScript==\n\
// @name "+name+" \n\
// @namespace https://bitbucket.org/lionnel0/cotg_public/raw/master\n\
// @version " + version + "\n\
// @description Lionnel0's CoTG Tools\n\
// @author Lionnel0\n\
// @match https://w*.crownofthegods.com/\n\
// @include https://w*.crownofthegods.com/\n\
// @grant none\n\
// @updateURL    https://bitbucket.org/lionnel0/cotg_public/raw/master/"+fileName+"\n\
// @downloadURL    https://bitbucket.org/lionnel0/cotg_public/raw/master/"+fileName+"\n\
// @doc    "+ doc +"\n\
// ==/UserScript==\n\n"

	fs.writeFile("pub/"+ fileName, comment + obfuscatedCode, function (err) {
		if (err) {
			console.error(err);
		}
		console.log("Done.")
	});
}
fs.readFile("lionnel.js", "utf-8", function (err, data) {
	publish(err, data, "lionnel.user.js", "CoTG Builder by Lionnel0");

});

fs.readFile("Rankings_Exporter.js", "utf-8", function (err, data) {
	publish(err, data, "Rankings_Exporter.user.js", "CoTG Ranking exporter by Lionnel0");
});